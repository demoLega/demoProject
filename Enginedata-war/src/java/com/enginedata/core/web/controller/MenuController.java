/*
 * Codigo producido por Engine Data
 * Todos los derechos reservados.  * 
 */
package com.enginedata.core.web.controller;

/**
 *
 * @author Edgar
 */
import com.enginedata.web.util.ServiceLocator;
import com.enginedata.web.util.ModuleEngineConstants;
import com.enginedata.core.web.util.SecurityEngineData;
import com.enginedata.web.util.FacesUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import mx.com.enginedata.bean.util.enumerator.MpProcType;
//import mx.com.enginedata.bean.util.enumerator.MpProcType;
import mx.com.enginedata.general.facade.bean.GeneralFacade;
import mx.com.enginedata.lgn.dto.LgnUserFncDTO;
import mx.com.enginedata.lgn.dto.MenuProcessDTO;
import mx.com.enginedata.lgn.dto.UserDTO;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;

/**
 * Esta clase Administrara las llamadas de la Vista Menu
 *
 * @author: Edgar Villegas
 * @version: 08/12/2015
 */
@ViewScoped
@ManagedBean(name = "menuController")
public class MenuController implements Serializable {

    private GeneralFacade generalFacade;
    private MenuModel model;
    private MenuModel modelMovil;
    private List<MenuProcessDTO> listMenuProcessDTO;
    private String uriLocal;
    List<MenuProcessDTO> menuListGestion = null;
//    MenuProcessDTO itemMenu = new MenuProcessDTO();
    private MenuModel modelGestions;

    public MenuModel getModelGestions() {
        return modelGestions;
    }

    public void setModelGestions(MenuModel modelGestions) {
        this.modelGestions = modelGestions;
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public MenuModel getModelMovil() {
        return modelMovil;
    }

    public void setModelMovil(MenuModel modelMovil) {
        this.modelMovil = modelMovil;
    }

    public MenuController() {
        try {
            generalFacade = (GeneralFacade) ServiceLocator.getInstance().getEJB(ModuleEngineConstants.JNDI_NAME_GENERAL_MODULE_BEAN);
            //serviceFacade = null;
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al procesar el Registro."));
        }
    }

    @PostConstruct
    public void init() {
        try {
            onLoadMenu();
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al procesar el Registro."));
        }
    }

    public void onLoadMenu() {
        try {
            SecurityEngineData security = SecurityEngineData.getSecurityEngineData();

            UserDTO user = new UserDTO();
            //if (security.getUserMstrDTO() != null) {
                
                //user.setUserName(security.getUserMstrDTO().getUmUserAccess());
                //user.setPerfilId(Long.parseLong(security.getUserMstrDTO().getIdProfileDTO().getIdProfile()));
                
                user.setUserName("EVILLEGAS");

                listMenuProcessDTO = generalFacade.getMenuByUser(user);
                if (!listMenuProcessDTO.isEmpty()) {
                    buildMenuMovil();
                    buildMenuGestios();
                    if (!menuListGestion.isEmpty()) {
                        buildMenuBar(menuListGestion.get(0).getMpProcId(), menuListGestion.get(0).getMpLabl());
                        String lbl = menuListGestion.get(0).getMpLabl();
                        String prefijo = "menu.page.menu.menuBar.";
                        iconCss = lbl.substring(lbl.indexOf(prefijo) + prefijo.length());
                    } else {
                        buildMenuBar(0L, "");
                    }
                }
            //}
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al procesar el Registro."));
        }
    }

    private void buildMenuMovil() {
        long ini = System.currentTimeMillis();
        double elements;
        double parent;
        elements = 0;
        parent = 0;
        modelMovil = new DefaultMenuModel();
        List<MenuProcessDTO> menuList = findParentId(0L);
        int i;
        DefaultSubMenu subMenu = null;
        for (i = 0; i < menuList.size(); i++) {
            subMenu = buildElementSubMenu(menuList.get(i));
            elements = new BigDecimal(menuList.get(i).getMpOrdr()).doubleValue();
            parent = new BigDecimal(menuList.get(i).getMpPrntId()).doubleValue();
            if (MpProcType.getEnum(menuList.get(i).getMpProcType().trim()).equals(MpProcType.MENU)) {
                //System.out.println(menuList.get(i).getMpPrntId() + "," + menuList.get(i).getMpOrdr() + ",," + menuList.get(i).getMpLabl());
                subMenu.setElements((List<MenuElement>) buildStructMenuMovil(menuList.get(i), MpProcType.MENU));
                subMenu.setStyleClass("menulink");
            }
            modelMovil.addElement(subMenu);
        }
        long fin = System.currentTimeMillis();
        long diferenciaMils = fin - ini;
//obtenemos los segundos
        long segundos = diferenciaMils / 1000;
        //obtenemos las horas
        long horas = segundos / 3600;

        //restamos las horas para continuar con minutos
        segundos -= horas * 3600;

        //igual que el paso anterior
        long minutos = segundos / 60;
        segundos -= minutos * 60;

        //ponemos los resultados en un mapa 
//        System.out.println("horas " + Long.toString(horas));
//        System.out.println("minutos " + Long.toString(minutos));
//        System.out.println("segundos " + Long.toString(segundos));
//        System.out.println("Tiempo fin create menu " + Calendar.getInstance().getTime());
    }

    public DefaultSubMenu buildElementSubMenu(MenuProcessDTO menuProcessDTO) {
        DefaultSubMenu subMenu = new DefaultSubMenu();
        subMenu.setLabel(FacesUtils.getLocaleLabel(menuProcessDTO.getMpLabl()));
        return subMenu;
    }

    public DefaultMenuItem buildElementItem(MenuProcessDTO menuProcessDTO) {
        DefaultMenuItem item = new DefaultMenuItem();
        if (menuProcessDTO.getIdFunctionDTO() == null) {
            item.setValue(FacesUtils.getLocaleLabel(menuProcessDTO.getMpLabl()));
        } else {
            item.setValue(FacesUtils.getLocaleLabel(menuProcessDTO.getIdFunctionDTO().getLfName()));
        }

        String urlItem = menuProcessDTO.getIdFunctionDTO().getLfUrl();
        String typeVersion = "";

        if (urlItem.contains("?")) {
            urlItem = urlItem + "&" + SecurityEngineData.getSecurityEngineData().getVersionApp();
            typeVersion = "&";
        } else {
            urlItem = urlItem + "?" + SecurityEngineData.getSecurityEngineData().getVersionApp();
            typeVersion = "?";
        }
        if (menuProcessDTO.getIdFunctionDTO().getLfUrl().contains("menuStruct")) {
            item.setCommand("#{menuController.openDialog('" + urlItem + "')}");
            item.setOncomplete("rmOpenDialog()");
        } else //item.setOnclick("refreshMainContext('" + urlItem + "')");
        if (menuProcessDTO.getIdFunctionDTO().getLfMultiDialog().compareTo("Y") == 0) {

            String widht = menuProcessDTO.getIdFunctionDTO().getLfWidht();
            String height = menuProcessDTO.getIdFunctionDTO().getLfHeight();

            if (widht.compareTo("N") == 0) {
                widht = "99";
            }
            if (height.compareTo("N") == 0) {
                height = "99";
            }

            String titleModalWindow = "";
            titleModalWindow = FacesUtils.getLocaleLabel(menuProcessDTO.getMpLabl());
            int positionglp = urlItem.lastIndexOf("/");
            int positionAns = urlItem.lastIndexOf(typeVersion);
            String buscarglp = urlItem.substring(positionglp + 1, positionAns);
            buscarglp = buscarglp.substring(0, buscarglp.length() - 4);

            item.setOnclick("validateVisible('" + buscarglp + "' , 'ifrMainContextModal" + buscarglp + "','" + urlItem + "')");
            LgnUserFncDTO objGeneric = new LgnUserFncDTO();
            objGeneric.setUrlFav(urlItem);
            objGeneric.setDescripcion(buscarglp);
            objGeneric.setNombre(titleModalWindow);
            objGeneric.setLfWidht(widht);
            objGeneric.setLfHeight(height);

            if (listItemsDialogs == null) {
                listItemsDialogs = new ArrayList<LgnUserFncDTO>();
            }

            if (listItemsDialogs.isEmpty()) {
                listItemsDialogs.add(objGeneric);
            } else if (!validateExistDialog(buscarglp)) {
                listItemsDialogs.add(objGeneric);
            }
        } else {
            item.setOnclick("refreshMainContext('" + urlItem + "')");
        }

        //System.out.println(urlItem);
        return item;
    }

    public boolean validateExistDialog(String name) {
        boolean existe = false;
        try {
            String nameFor = "";
            for (int i = 0; i < listItemsDialogs.size(); i++) {
                nameFor = listItemsDialogs.get(i).getDescripcion();
                if (nameFor.compareTo(name) == 0) {
                    existe = true;
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return existe;
    }

    public void openDialog(String uri) {
        try {
            uriLocal = uri.substring(0, uri.indexOf("."));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    List<LgnUserFncDTO> listItemsDialogs = null;

    public List<LgnUserFncDTO> getListItemsDialogs() {
        return listItemsDialogs;
    }

    public void setListItemsDialogs(List<LgnUserFncDTO> listItemsDialogs) {
        this.listItemsDialogs = listItemsDialogs;
    }

    public void openDialog() {
        try {
            Map<String, Object> options = new HashMap<String, Object>();
            options.put("resizable", false);
            options.put("modal", true);
            options.put("contentWidth", 470);
            options.put("contentHeight", 450);
            options.put("minimizable", true);
            options.put("draggable", true);
            RequestContext.getCurrentInstance().openDialog(uriLocal, options, null);
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error."));
        }
    }

    public boolean isGestion(long prntId) {
        boolean isGestion = false;
        if (menuListGestion != null) {
            for (MenuProcessDTO dTO : menuListGestion) {
                if (dTO.getMpProcId() == prntId) {
                    isGestion = true;
                }
            }
        }
        return isGestion;
    }

    public List<MenuElement> buildStructMenuMovil(MenuProcessDTO hijo, MpProcType tipoElemento) {
        List<MenuElement> l = new ArrayList<MenuElement>();
        DefaultSubMenu subMenu;
        DefaultMenuItem item;
        if (tipoElemento.equals(MpProcType.MENU)) {
            subMenu = buildElementSubMenu(hijo);
            List<MenuProcessDTO> subMenuList = findParentId(hijo.getMpProcId());
            if (!subMenuList.isEmpty()) {
                int i;
                for (i = 0; i < subMenuList.size(); i++) {
                    if (hijo.getMpPrntId() != 0) {
                        if (MpProcType.getEnum(subMenuList.get(i).getMpProcType().trim()).equals(MpProcType.MENU)) {
                            subMenu.setElements(buildStructMenuMovil(subMenuList.get(i), MpProcType.MENU));
                        } else {
                            subMenu.addElement(buildElementItem(subMenuList.get(i)));
                        }
                    } else if (MpProcType.getEnum(subMenuList.get(i).getMpProcType().trim()).equals(MpProcType.MENU)) {
                        l.addAll(buildStructMenuMovil(subMenuList.get(i), MpProcType.MENU));
                    } else {
                        l.add(buildElementItem(subMenuList.get(i)));
                    }
                }
                if (hijo.getMpPrntId() != 0) {
                    l.add(subMenu);
                }
            }
        } else {
            item = buildElementItem(hijo);
            if (hijo.getMpPrntId() != 0) {
                l.add(item);
            }
        }
        return l;
    }

    public List<MenuProcessDTO> findParentId(Long id) {
        List<MenuProcessDTO> listContenedor = new ArrayList<MenuProcessDTO>();
        int i;
        for (i = 0; i < listMenuProcessDTO.size(); i++) {
            if (listMenuProcessDTO.get(i).getMpPrntId().equals(id)) {
                listContenedor.add(listMenuProcessDTO.get(i));
            }
        }
        return listContenedor;
    }

    String iconCss = "";

    public String getIconCss() {
        return iconCss;
    }

    public void setIconCss(String iconCss) {
        this.iconCss = iconCss;
    }

    public void buildMenuGestios() {
        menuListGestion = findParentId(0L);

        DefaultSubMenu firstSubmenu = new DefaultSubMenu("Gestiones");
        for (int i = 0; i < menuListGestion.size(); i++) {
            modelGestions = new DefaultMenuModel();

            String lbl = menuListGestion.get(i).getMpLabl();
            String prefijo = "menu.page.menu.menuBar.";
            iconCss = lbl.substring(lbl.indexOf(prefijo) + prefijo.length());

            //System.out.println(iconCss);
            DefaultMenuItem item = new DefaultMenuItem(FacesUtils.getLocaleLabel(menuListGestion.get(i).getMpLabl()));
            item.setStyleClass("menu-icon-" + iconCss);
            item.setUpdate("menuform:menuBar , menuform:menuBarMobile , iconCss");
            item.setCommand("#{menuController.buildMenuBar('" + menuListGestion.get(i).getMpProcId() + "' , '" + menuListGestion.get(i).getMpLabl() + "')}");
            firstSubmenu.addElement(item);

            modelGestions.addElement(firstSubmenu);
        }

    }

    public void buildMenuBar(Long parent, String lbl) {
        long ini = System.currentTimeMillis();
        model = new DefaultMenuModel();
        List<MenuProcessDTO> menuList = findParentId(Long.valueOf(157));
        int i;
        DefaultSubMenu subMenu = null;
        for (i = 0; i < menuList.size(); i++) {
            //System.out.println("*****buildMenuBar " + menuList.get(i).toString());
            subMenu = buildElementSubMenu(menuList.get(i));
            if (MpProcType.getEnum(menuList.get(i).getMpProcType().trim()).equals(MpProcType.MENU)) {
                subMenu.setElements((List<MenuElement>) buildStructMenuBar(menuList.get(i), MpProcType.MENU));
            }
            model.addElement(subMenu);
        }
        String prefijo = "menu.page.menu.menuBar.";
        iconCss = lbl.substring(lbl.indexOf(prefijo) + prefijo.length());
        long fin = System.currentTimeMillis();
        long diferenciaMils = fin - ini;
        long segundos = diferenciaMils / 1000;
        long horas = segundos / 3600;

        segundos -= horas * 3600;

        //igual que el paso anterior
        long minutos = segundos / 60;
        segundos -= minutos * 60;

    }

    public List<MenuElement> buildStructMenuBar(MenuProcessDTO hijo, MpProcType tipoElemento) {
        List<MenuElement> l = new ArrayList<MenuElement>();
        DefaultSubMenu subMenu;
        DefaultMenuItem item;
        if (tipoElemento.equals(MpProcType.MENU)) {
            subMenu = buildElementSubMenu(hijo);
            List<MenuProcessDTO> subMenuList = findParentId(hijo.getMpProcId());
            if (!subMenuList.isEmpty()) {
                int i;
                for (i = 0; i < subMenuList.size(); i++) {
                    if (hijo.getMpPrntId() != 0 && !isGestion(hijo.getMpPrntId())) {
                        if (MpProcType.getEnum(subMenuList.get(i).getMpProcType().trim()).equals(MpProcType.MENU)) {
                            List<MenuElement> elements = buildStructMenuBar(subMenuList.get(i), MpProcType.MENU);
                            for (int j = 0; j < elements.size(); j++) {
                                subMenu.addElement(elements.get(j));
                            }

                        } else {
                            subMenu.addElement(buildElementItem(subMenuList.get(i)));
                        }
                    } else if (MpProcType.getEnum(subMenuList.get(i).getMpProcType().trim()).equals(MpProcType.MENU)) {
                        l.addAll(buildStructMenuBar(subMenuList.get(i), MpProcType.MENU));
                    } else {
                        l.add(buildElementItem(subMenuList.get(i)));
                    }
                }
                if (hijo.getMpPrntId() != 0 && !isGestion(hijo.getMpPrntId())) {
                    l.add(subMenu);
                }
            }
        } else {
            item = buildElementItem(hijo);
            if (hijo.getMpPrntId() != 0 && !isGestion(hijo.getMpPrntId())) {
                l.add(item);
            }
        }
        return l;
    }

    private List<MenuProcessDTO> selectedThemes;

    public List<MenuProcessDTO> completeTheme(String query) {

        List<MenuProcessDTO> allThemes = new ArrayList<MenuProcessDTO>();
        allThemes.addAll(listMenuProcessDTO);
        List<MenuProcessDTO> filteredThemes = new ArrayList<MenuProcessDTO>();

        if (query.compareTo(" ") != 0) {
            for (int i = 0; i < allThemes.size(); i++) {
                MenuProcessDTO skin = allThemes.get(i);
                String buscar = FacesUtils.getLocaleLabel(skin.getMpLabl());
                skin.setMpLabl(buscar);

                buscar = buscar.replaceAll("á", "a");
                buscar = buscar.replaceAll("é", "e");
                buscar = buscar.replaceAll("í", "i");
                buscar = buscar.replaceAll("ó", "o");
                buscar = buscar.replaceAll("ú", "u");

                query = query.replaceAll("á", "a");
                query = query.replaceAll("é", "e");
                query = query.replaceAll("í", "i");
                query = query.replaceAll("ó", "o");
                query = query.replaceAll("ú", "u");

                if (buscar.toLowerCase().contains(query.toLowerCase())) {
                    if (MpProcType.getEnum(skin.getMpProcType().trim()).equals(MpProcType.ITEM)) {
                        filteredThemes.add(skin);
                    }
                }
            }
        }
        return filteredThemes;
    }
    MenuProcessDTO selectedMenu = new MenuProcessDTO();

    public void onItemSelect(SelectEvent event) {
        RequestContext.getCurrentInstance().execute("refreshMainContext('" + event.getObject() + "');PF('overlayMenu').hide();");
        RequestContext.getCurrentInstance().execute("document.getElementById('autocompleteFunctions_input').value=''");

    }

    public List<MenuProcessDTO> getSelectedThemes() {
        return selectedThemes;
    }

    public void setSelectedThemes(List<MenuProcessDTO> selectedThemes) {
        this.selectedThemes = selectedThemes;
    }

    public MenuProcessDTO getSelectedMenu() {
        return selectedMenu;
    }

    public void setSelectedMenu(MenuProcessDTO selectedMenu) {
        this.selectedMenu = selectedMenu;
    }
}
