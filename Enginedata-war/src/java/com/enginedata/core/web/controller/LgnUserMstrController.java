/*
 * Codigo producido por Engine Data
 * Todos los derechos reservados.  * 
 */
package com.enginedata.core.web.controller;

/**
 *
 * @author Edgar
 */
import com.enginedata.web.util.ModuleEngineConstants;
import com.enginedata.web.util.ServiceLocator;
import java.io.Serializable;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import mx.com.enginedata.general.facade.bean.GeneralFacade;
import mx.com.enginedata.lgn.dto.*;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.data.FilterEvent;
import org.primefaces.model.LazyDataModel;

@ViewScoped
@ManagedBean(name = "lgnUserMstrController")
public class LgnUserMstrController extends MasterController implements Serializable {

    
    private GeneralFacade serviceFacade;
    private LgnUserMstrDTO selectedUserMstr;
    /*
    private EmailFacade emailFacade;
    private LgnUserMstrDTO selectedUserMstr;
    private SysClntMstrDTO selectedClntMstr;
    private LgnProfilesDTO player;
    private SysClntMstrDTO newClntMstr;
    private SysAddrMstrDTO newAddrMstr;
    private FilterPageDTO filters;
    private List<LgnUserMstrDTO> LgnUserMstrDTOS;
    private LazyDataModel<LgnUserMstrDTO> ldm;
    private LazyLgnUserMstrDataModel ldmGeneric;
    private List<LgnUserMstrDTO> filteredLgnUserMstrDTO;
    private List<SysClntMstrDTO> filteredSysClntMstrDTO;
    private List<LgnProfilesDTO> listLgnProfilesDTO;
    private List<SysClntMstrDTO> listSysClntMstrDTO;

    TypeUser typeUser;
    private HtmlCommandButton btnClientNew;
    private DataTable dtLazyDataTable;
    private boolean globalFilter2Active = true;
    private boolean activeBtnEliminar = true;
    private boolean activeUsuario = true;
    private boolean activeChbEAD = false;
    private boolean activeChbRAD = true;
    private boolean activeChbFXC = false;
    private Boolean activeChbStatusUserMstr = false;
    private boolean disabledChbStatusUserMstr = false;
    private boolean disabledBtnEmailSend = false;
    private boolean requiredCustomer = true;
    private boolean requiredEmployee = false;
    public String tipoCliente;
    private int posicionSelectClient;
    private ModoOperacion modoOperacion;
    private String titleDialogOperation;
    private String maskRFC = "aaa-999999-***";
    private String claveOLD = "";
    private String controlFocus;
    */
    
    public LgnUserMstrController() {
        try {
            serviceFacade = (GeneralFacade) ServiceLocator.getInstance().getEJB(ModuleEngineConstants.JNDI_NAME_GENERAL_MODULE_BEAN);
            //emailFacade = (EmailFacade) ServiceLocator.getInstance().getEJB(ModuleEngineConstants.JNDI_NAME_UTILERIA_EMAIL_BEAN);
        } catch (Exception ex) {
            ex.printStackTrace();
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }

        if (selectedUserMstr == null) {
            selectedUserMstr = new LgnUserMstrDTO();
        }
//        if (filters == null) {
//            filters = new FilterPageDTO();
//        }
    }

    @PostConstruct
    public void init() {
        try {
            /*
            security = SecurityEngineData.getSecurityEngineData();
            listLgnProfilesDTO = serviceFacade.getProfilesActiveAll();
            listSysClntMstrDTO = new ArrayList<SysClntMstrDTO>();
            */
        } catch (Exception ex) {
            ex.printStackTrace();
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }
    }

    /*
    public List<LgnProfilesDTO> queryByName(String name) {
        // Assumed search using the startsWith
        List<LgnProfilesDTO> queried = new ArrayList<LgnProfilesDTO>();
        for (LgnProfilesDTO player : listLgnProfilesDTO) {
            if (player.getLpName().toUpperCase().startsWith(name.toUpperCase())) {
                queried.add(player);
            }
        }
        return queried;
    }

    public void execOperationUserMstr() {
        if (getModoOperacion().equals(ModoOperacion.NUEVO)) {
            saveUserMstr();
        } else {
            updateUserMstr();
        }
    }

    private void saveUserMstr() {
        try {
            if (selectedUserMstr != null) {
//                DataSecurityDTO data = SecurityEngineData.getSecurityEngineData().getDataSecurity();
                String userLogin = security.getUserLogin();
                selectedUserMstr.setUmUserType(typeUser.getType());
                if (typeUser.equals(TypeUser.INTERNO)) {
                    selectedUserMstr.setUmUserStus("ACTI");
                } else {
                    selectedUserMstr.setUmUserStus("PREACTI");
                }
                selectedUserMstr.setUmDeleteFlag("0");
                Date time = security.getTimeServer();
                selectedUserMstr.setCrtdBy(userLogin);
                //selectedUserMstr.setCrtdOn(time);
                selectedUserMstr.setMdfdBy(userLogin);
                //selectedUserMstr.setMdfdOn(time);
                selectedUserMstr = serviceFacade.saveLgnUserMstr(selectedUserMstr);
                ldmGeneric.getDatasource().add(selectedUserMstr);
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('updateDialog').hide();");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, FacesUtils.getLocaleLabel("enginedate.page.general.label.aviso"), FacesUtils.getLocaleLabel("enginedate.page.general.info.label.procesoCorrecto")));
                if (!typeUser.equals(TypeUser.INTERNO)) {
                    envioEmail();
                }
            }
        } catch (mx.com.enginedate.comun.SmartGeneralException ex) {
            if (ex.getCodeError().equalsIgnoreCase("9998")) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.catalogs.lstUser.msjErrorUsuarioExistente")));
                selectedUserMstr.setUmUserAccess("");
//                RequestContext.getCurrentInstance().execute("updateForm:usuario.focus();");
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if (ex.getMessage().contains("ELIMINADO")) {
                RequestContext.getCurrentInstance().execute("PF('widgetReactivarUsuario').show();");
            } else if ((ex.getCause().getCause().getCause() instanceof SQLIntegrityConstraintViolationException) || (ex.getCause().getCause() instanceof SQLIntegrityConstraintViolationException)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.catalogs.lstUser.msjErrorUsuarioExistente")));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
            }
        } finally {

        }
    }

    public void saveClntMstr() {
        try {
            if (newClntMstr != null) {
                newClntMstr.setBsnsSegId("0099");//CM_BSNS_SEG_ID-SEGMENTO GIRO COMERCIAL DEFAULT 0099
                newClntMstr.setRegdClntFlag("N");//CM_REGD_CLNT_FLAG-DEFAULT N
                newClntMstr.setRadFlag(activeChbRAD ? FlagGeneric.ACTIVOCORTO.getType() : FlagGeneric.INACTIVOCORTO.getType());//CM_RAD_FLAG
                newClntMstr.setDestPaidFlag(activeChbFXC ? FlagGeneric.ACTIVOCORTO.getType() : FlagGeneric.INACTIVOCORTO.getType());//CM_DEST_PAID_FLAG-FXC
                newClntMstr.setEadFlag(activeChbEAD ? FlagGeneric.ACTIVOCORTO.getType() : FlagGeneric.INACTIVOCORTO.getType());//CM_EAD_FLAG
                newClntMstr.setBsnsLineId("9999");//CM_BSNS_LINE_ID-DEFAULT 9999
                newClntMstr.setCredStusId("DIS");//CM_CRED_STUS_ID-DEFAULT DIS
                newClntMstr.setSupdCheqFlag("N");//CM_SUPD_CHEQ_FLAG-DEFAULT N
                newClntMstr.setActvFlag("A");//CM_SUPD_CHEQ_FLAG-DEFAULT N
                newAddrMstr.setAmAddrStyp("SHIP");//AM_ADDR_STYP,
                newAddrMstr.setAmEntyDesc(newClntMstr.getClntName());//AM_ENTY_DESC-NOMBRE DEL CLIENTE
                newAddrMstr.setAmAddrType("CLNT");//AM_ADDR_TYPE-default CLNT
                newAddrMstr.setAmAddrId("1");//AM_ADDR_ID,
                newAddrMstr.setAmDefaFlag("Y");//AM_DEFA_FLAG-DEFAULT Y
                newAddrMstr.setAmAddrDefnType("Y");//AM_ADDR_DEFN_TYPE-SERIA UN Y SI ES UNICO DIRECCION
                newAddrMstr.setAmGetyCode(newClntMstr.getDwCoberturaViewDTO().getColoId());
                newAddrMstr.setAmGetyLevl(newClntMstr.getDwCoberturaViewDTO().getColLevel().toString());
                newAddrMstr.setAmGetyType(newClntMstr.getDwCoberturaViewDTO().getColType().toString());
                newClntMstr = serviceFacade.createClntComplete(security.getUserMstrDTO().getUmUserAccess(), security.getBranchOwner(), newClntMstr, newAddrMstr);
                onCustomerChosen(newClntMstr);
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('clienteDialog').hide();");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        } finally {

        }
    }

    private void updateUserMstr() {
        if (selectedUserMstr != null) {
            String userLogin = security.getUserLogin();
            Date time = security.getTimeServer();
            selectedUserMstr.setMdfdBy(userLogin);
            selectedUserMstr.setMdfdOn(time);
            try {
                if (typeUser.equals(TypeUser.INTERNO)) {
                    if (activeChbStatusUserMstr) {
                        selectedUserMstr.setUmUserStus("ACTI");
                    } else {
                        selectedUserMstr.setUmUserStus("INAC");
                    }
                }
                serviceFacade.updateLgnUserMstr(selectedUserMstr);
                ldmGeneric.getDatasource().set(posicionSelectClient, selectedUserMstr);
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('updateDialog').hide();");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, FacesUtils.getLocaleLabel("enginedate.page.general.label.aviso"), FacesUtils.getLocaleLabel("enginedate.page.general.info.label.procesoCorrecto")));
            } catch (Exception ex) {
                ex.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
            }
        }
    }

    public void deleteUserMstr() {
        if (selectedUserMstr != null) {
            String userLogin = security.getUserLogin();
            Date time = security.getTimeServer();
            selectedUserMstr.setMdfdBy(userLogin);
            selectedUserMstr.setMdfdOn(time);
            try {
                serviceFacade.deleteLgnUserMstr(selectedUserMstr);
                ldmGeneric.getDatasource().remove(posicionSelectClient);
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('updateDialog').hide();");
            } catch (Exception ex) {
                ex.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
            }
        }
    }

    public void userMstrSelectionAll() {
        try {
            posicionSelectClient = ldmGeneric.getDatasource().lastIndexOf(selectedUserMstr);
            selectedUserMstr = serviceFacade.getUserMstrForUserID(selectedUserMstr.getUmUserId());
            if (!getTypeUser().equals(TypeUser.INTERNO)) {
                if (selectedUserMstr.getIdClntMstrDTO() == null) {
                    selectedUserMstr.setIdClntMstrDTO(new SysClntMstrDTO());
                }
            }
            player = selectedUserMstr.getIdProfileDTO();
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }
    }

    public void onRowDblClckSelect(final SelectEvent event) {
        if (event.getObject() instanceof LgnUserMstrDTO) {
            configurarDialogOperacion("2");
        }
    }

    public void configurarDialogOperacion(String modo) {
        try {
            claveOLD = "";
            if (modo.equalsIgnoreCase("1")) {
                setModoOperacion(ModoOperacion.NUEVO);
                titleDialogOperation = "Nuevo";
                activeBtnEliminar = true;
                activeUsuario = false;
                selectedUserMstr = null;
                selectedUserMstr = new LgnUserMstrDTO();
                if (getTypeUser().equals(TypeUser.CLIENTE) || getTypeUser().equals(TypeUser.PROVEEDOR)) {
                    activeChbStatusUserMstr = false;
                    disabledChbStatusUserMstr = true;
                    requiredCustomer = true;
                    requiredEmployee = false;
                    selectedUserMstr.setIdClntMstrDTO(new SysClntMstrDTO());
                } else {
                    activeChbStatusUserMstr = true;
                    selectedUserMstr.setUmUserStus("ACTI");
                    disabledChbStatusUserMstr = false;
                    requiredCustomer = false;
                    requiredEmployee = true;
                }
                disabledBtnEmailSend = true;
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('updateDialog').show();");
            } else if (selectedUserMstr == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.aviso"), "Favor de seleccionar un usuario"));
            } else {
                setModoOperacion(ModoOperacion.MODIFICAR);
                titleDialogOperation = "Modificar";
                activeBtnEliminar = false;
                activeUsuario = true;
                if (getTypeUser().equals(TypeUser.CLIENTE) || getTypeUser().equals(TypeUser.PROVEEDOR)) {
                    requiredCustomer = true;
                    requiredEmployee = false;
                    if (selectedUserMstr.getUmUserStus().equalsIgnoreCase("PREACTI")) {
                        disabledChbStatusUserMstr = true;
                        activeChbStatusUserMstr = false;
                    } else {
                        disabledChbStatusUserMstr = false;
                        activeChbStatusUserMstr = true;
                    }
                    disabledBtnEmailSend = false;
                } else {
                    requiredCustomer = false;
                    requiredEmployee = true;
                    disabledBtnEmailSend = true;
                    activeChbStatusUserMstr = selectedUserMstr.getUmUserStus().equalsIgnoreCase("ACTI");
                }
                if (selectedUserMstr.getIdClntMstrDTO() != null && selectedUserMstr.getIdClntMstrDTO().getClntId() != null) {
                    claveOLD = selectedUserMstr.getIdClntMstrDTO().getClntId();
                }

                userMstrSelectionAll();
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('updateDialog').show();");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }
    }

//    public void onkeypressCliente() {
//        System.out.println("onChange");
//    }
    // Metodo para buscar un cliente por codigo
    public void findClientByCode() throws Exception {

        //System.out.println(" typeClient " + typeClient);
        String codeClient = "";
        try {
            codeClient = selectedUserMstr.getIdClntMstrDTO().getClntId();
            selectedUserMstr.getIdClntMstrDTO().setClntId("");
            selectedUserMstr.getIdClntMstrDTO().setClntName("");
            if (codeClient.compareTo("") != 0) {
                WhereCondition whereCondition = new WhereCondition();
                whereCondition.getWhere().add(new ColumnWhere("CLNTID", TypeColumn.STRING, LogicalOperator.EQUAL, codeClient, ""));
                //whereCondition.getWhere().add(new ColumnWhere("AMADDRDEFNTYPE", TypeColumn.STRING, LogicalOperator.EQUAL, "Y", ""));

                //DwCoberturaViewDTO coberturaDTO = (DwCoberturaViewDTO) generalFacade.getOnlyOneItemByFilters("query_getAddrComplete", whereCondition, new DwCoberturaViewDTO(), TypePropertiesConstants.PRINCIPAL);
                List<SysClntMstrDTO> listSysClntMstrDTO = serviceFacade.getListCliente(whereCondition, security.getTypeUser(), FacesUtils.cantidadMaximaRegistos, false);
                SysClntMstrDTO sysClntMstrDTO = null;
                if (!listSysClntMstrDTO.isEmpty()) {
                    if (listSysClntMstrDTO.size() == 1) {
                        sysClntMstrDTO = listSysClntMstrDTO.get(0);
                    } else {
                        showSearchCustomerDialog("2", listSysClntMstrDTO);
                        RequestContext.getCurrentInstance().execute("recorreFocus(6)");
                        return;
                    }
                }
                
                if (sysClntMstrDTO != null) {
                    onCustomerChosen(sysClntMstrDTO);
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), "No se encontro Cliente."));
                }
            } else {
                RequestContext.getCurrentInstance().execute("recorreFocus(6)");
            }
            
        } catch (Exception ex) {
            
        }
    }
    
    private void showSearchCustomerDialog(String modeButton, List<SysClntMstrDTO> listCliente) throws Exception {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", true);
        options.put("modal", true);
        options.put("contentWidth", 1300);
        options.put("contentHeight", 450);
        Map<String, List<String>> paramMap = new HashMap<String, List<String>>();
        List<String> listModeButton = new ArrayList<String>(), listTitleDialog = new ArrayList<String>();
        listModeButton.add(modeButton);
        //modeButton es para indicar que botones se mostraran: 1= Si o no, 2=Seleccionar.
        paramMap.put("modeButton", listModeButton);
//        options.put("closable", false);
        if (modeButton.equalsIgnoreCase("1")) {
            listTitleDialog.add(FacesUtils.getLocaleLabel("enginedate.page.utileria.busqueda.searchCustomerNotFilter.panel.title"));
        } else {
            listTitleDialog.add(FacesUtils.getLocaleLabel("enginedate.page.utileria.busqueda.searchCustomerNotFilter.panel.titleManyAddress"));
        }
        paramMap.put("titleDialog", listTitleDialog);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("listCustomerSimilarity", listCliente);
        RequestContext.getCurrentInstance().openDialog("/mx/com/enginedate/utileria/busqueda/searchCustomerNotFilter", options, paramMap);
    }
    
    public void chooseCustomer() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        options.put("contentWidth", 1000);
//        options.put("header", "BUSQUEDA CLIENTE");
        try {
            RequestContext.getCurrentInstance().openDialog("/mx/com/enginedate/utileria/busqueda/busquedaCliente", options, null);
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }
    }

    public void onCustomerChosen(SelectEvent event) {
        if (event.getObject() instanceof SysClntMstrDTO) {
            onCustomerChosen((SysClntMstrDTO) event.getObject());
        } else if (event.getObject() instanceof Boolean) {
            if (((Boolean) event.getObject())) {
                RequestContext.getCurrentInstance().execute("document.getElementById('updateForm:btnClientNew').click();");
            }
        }
    }

    public void chooseEmployee() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        options.put("contentWidth", 1000);
//        options.put("header", "BUSQUEDA CLIENTE");
        try {
            RequestContext.getCurrentInstance().openDialog("/mx/com/enginedate/utileria/busqueda/searchEmployee", options, null);
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }
    }

    public void onEmployeeChosen(SelectEvent event) {

    }

    private void onCustomerChosen(SysClntMstrDTO dTO) {
        if (dTO != null) {
            selectedUserMstr.setIdClntMstrDTO(dTO);
        }
    }

    public void chooseZipCode() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", true);
        options.put("modal", true);
        options.put("contentWidth", 800);
//        options.put("header", "BUSQUEDA CLIENTE");
        try {
            RequestContext.getCurrentInstance().openDialog("/mx/com/enginedate/utileria/busqueda/busquedaCodigoPostal", options, null);
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }
    }

    public void onZipCodeChosen(SelectEvent event) {
        DwCoberturaViewDTO car = (DwCoberturaViewDTO) event.getObject();
        if (car != null) {
            newClntMstr.setDwCoberturaViewDTO(car);
        }
    }

    public void changeCbxTipoCliente(ValueChangeEvent event) {
        if ("I".equalsIgnoreCase(event.getNewValue().toString())) {
            maskRFC = "aaaa-999999-***";
        } else {
            maskRFC = "aaa-999999-***";
        }
    }

    public void configurarDialogCliente() {
        try {
            newClntMstr = null;
            newClntMstr = new SysClntMstrDTO();
            newClntMstr.setDwCoberturaViewDTO(new DwCoberturaViewDTO());
            newAddrMstr = null;
            newAddrMstr = new SysAddrMstrDTO();
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('clienteDialog').show();");
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }
    }

    public void findAddressByZipCodeEnter() throws Exception {
        List<DwCoberturaViewDTO> cob = new ArrayList<DwCoberturaViewDTO>();
        List<String> fieldSet = new ArrayList<String>();
        SelectEntity select = new SelectEntity();
        WhereCondition where = new WhereCondition();
        List<ColumnWhere> listColumnsWhere = new ArrayList<ColumnWhere>();
        ColumnWhere columnWhere;
        try {
            if (!newClntMstr.getDwCoberturaViewDTO().getColoZipCode().isEmpty()) {
//            cp = new InputText();
//            uiComponentId = (event != null)? event.getComponent().getId() : "";
//             if (uiComponentId.equals("btnFindByCdZipCode")) {
//                cp = (InputText) context.getViewRoot().findComponent("frmTable:txtCdZipCode");
//            } else if (uiComponentId.equals("btnFindByDdZipCode")) {
//                cp = (InputText) context.getViewRoot().findComponent("frmTable:txtDdZipCode");
//            }
                columnWhere = new ColumnWhere("COLO_ZIPCODE", TypeColumn.STRING, "=", newClntMstr.getDwCoberturaViewDTO().getColoZipCode(), "");
//        fieldSet.add("*");
                fieldSet.add("COLO_ZIPCODE COLOZIPCODE");
                fieldSet.add("COLO_PLAZA plaza");
                fieldSet.add("COLO_SUCURSAL sucursal");
                fieldSet.add("COLO_ID COLOID");
                fieldSet.add("COLO_DES COLODES");
                fieldSet.add("COL_LEVEL COLLEVEL");
                fieldSet.add("COL_TYPE COLTYPE");
                fieldSet.add("CIUDAD");
                fieldSet.add("ID_CIUD codCiud");
                fieldSet.add("CIU_LEVEL CIULEVEL");
                fieldSet.add("CIU_TYPE CIUTYPE");
                fieldSet.add("DELMUN");
                fieldSet.add("COD_DELE CODDELE");
                fieldSet.add("DEL_LEVEL DELLEVEL");
                fieldSet.add("DELETYPE DELETYPE");
                fieldSet.add("ESTADO");
                fieldSet.add("COD_ESTA CODESTA");
                fieldSet.add("EST_LEVEL ESTLEVEL");
                fieldSet.add("EST_TYPE ESTTYPE");
                fieldSet.add("ZONA");
                fieldSet.add("COD_ZONA CODZONA");
                fieldSet.add("ZONA_LEVL ZONALEVL");
                fieldSet.add("ZONA_TYPE ZONATYPE");
                fieldSet.add("PAIS");
                fieldSet.add("COD_PAIS CODPAIS");
                fieldSet.add("PAIS_LEVEL PAISLEVEL");
                fieldSet.add("PAIS_TYPE PAISTYPE");
                fieldSet.add("BC_ZONE BCZONE");
                fieldSet.add("COLO_OL COLOOL");
                fieldSet.add("BC_RUTA BCRUTA");
                fieldSet.add("BD_DSTN_HRS BDDSTNHRS");

                select.setEntity("DW_COBERTURA_VIEW");
                select.setFieldSelect(fieldSet);
                listColumnsWhere.add(columnWhere);
                where.setWhere(listColumnsWhere);

                cob = serviceFacade.getAllItemsByFilters(select, where, new DwCoberturaViewDTO());
                if (cob.isEmpty()) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), "No se encontró Código Postal."));
                } else {
                    newClntMstr.setDwCoberturaViewDTO(cob.get(0));
                }
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), "No se encontró Código Postal."));
            throw e;
        }
    }

    public void clntMstrSelectionAll() {
        try {
            if (!selectedUserMstr.getIdClntMstrDTO().getClntId().isEmpty() && !selectedUserMstr.getIdClntMstrDTO().getClntId().equalsIgnoreCase(claveOLD)) {
                listSysClntMstrDTO = null;
                WhereCondition whereCondition = new WhereCondition();
                whereCondition.getWhere().add(new ColumnWhere("CLNTID", TypeColumn.STRING, LogicalOperator.EQUAL, selectedUserMstr.getIdClntMstrDTO().getClntId(), LogicalUnion.AND));
                whereCondition.getWhere().add(new ColumnWhere("AMADDRDEFNTYPE", TypeColumn.STRING, LogicalOperator.EQUAL, "Y", ""));

                listSysClntMstrDTO = serviceFacade.getListCliente(whereCondition, security.getTypeUser(), FacesUtils.cantidadMaximaRegistos, false);
                if (listSysClntMstrDTO == null) {
                    listSysClntMstrDTO = new ArrayList<SysClntMstrDTO>();
                }
                if (listSysClntMstrDTO.isEmpty()) {
                    selectedUserMstr.getIdClntMstrDTO().setClntId("");
                    selectedUserMstr.getIdClntMstrDTO().setClntName("");
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), "No existe el cliente"));
                } else {
                    selectedUserMstr.setIdClntMstrDTO(listSysClntMstrDTO.get(0));
                    claveOLD = selectedUserMstr.getIdClntMstrDTO().getClntId();
                    controlFocus = ".boton";
                }
            } else {
                controlFocus = ".boton";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }
    }

    public void posicionFocus() {
        if (controlFocus != null) {
//            RequestContext context = RequestContext.getCurrentInstance();
//            context.execute("controlFocus('" + controlFocus + "')");
        }
    }

    public void envioEmail() {
        try {
            if (selectedUserMstr.getUmUserId() != null && !selectedUserMstr.getUmUserEmail().isEmpty()) {
                asyncServiceMethod();
                disabledBtnEmailSend = true;
                RequestContext.getCurrentInstance().update(":updateForm:btnEmailSend");
            } else if (selectedUserMstr.getUmUserEmail().isEmpty()) {
                RequestContext.getCurrentInstance().execute("recorreFocus(3)");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.aviso"), "Favor de ingresar el email."));
            } else {
                RequestContext.getCurrentInstance().execute("recorreFocus(11)");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.aviso"), "Favor de guardar primero la información"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            disabledBtnEmailSend = false;
            RequestContext.getCurrentInstance().update(":updateForm:btnEmailSend");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
        }
    }

    public void asyncServiceMethod() {
        Runnable task = new Runnable() {
            public void run() {
                try {
                    emailFacade.envioEmailCustomerActivate("Activación del cliente", selectedUserMstr.getUmUserEmail(), selectedUserMstr, false);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        new Thread(task, "ServiceThread").start();
    }

    private void statusEmailSend(String titulo, String mensaje, FacesMessage.Severity severity) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, titulo, mensaje));
        RequestContext.getCurrentInstance().update(":updateForm:btnEmailSend");
    }

    public void reactivarUsuario(String modo) {
        if (modo.equalsIgnoreCase("1")) {//si
            try {
                LgnUserMstrDTO lgnUserMstrDTO = serviceFacade.reactivarUsuario(typeUser, selectedUserMstr.getUmUserAccess(), security.getUserMstrDTO().getUmUserAccess());
                if (lgnUserMstrDTO != null && !lgnUserMstrDTO.getUmUserId().isEmpty()) {
                    RequestContext.getCurrentInstance().execute("PF('widgetReactivarUsuario').hide();");
                    RequestContext.getCurrentInstance().execute("PF('updateDialog').hide();");
                    ldmGeneric.getDatasource().add(lgnUserMstrDTO);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, FacesUtils.getLocaleLabel("enginedate.page.general.label.aviso"), FacesUtils.getLocaleLabel("enginedate.page.general.catalogs.lstUser.usuarioReactivado")));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.catalogs.lstUser.usuarioNoReactivado")));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, FacesUtils.getLocaleLabel("enginedate.page.general.label.error"), FacesUtils.getLocaleLabel("enginedate.page.general.error.label.procesar")));
            }
        } else {//NO
            selectedUserMstr.setUmUserAccess("");
        }
    }

    //Seccion de utilerias de JSF
    public void cancelPopUp() {
//        newUserMstr = new LgnUserMstrDTO();
    }

    public LgnUserMstrDTO getSelectedUserMstr() {
        return selectedUserMstr;
    }

    public void setSelectedUserMstr(LgnUserMstrDTO selectedUserMstr) {
        //System.out.println("setSelectedUserMstr" + selectedUserMstr);
        this.selectedUserMstr = selectedUserMstr;
    }

    public GeneralFacade getServiceFacade() {
        return serviceFacade;
    }

    public FilterPageDTO getFilters() {
        return filters;
    }

    public void setFilters(FilterPageDTO filters) {
        this.filters = filters;
    }

    public List<LgnUserMstrDTO> getFilteredLgnUserMstrDTO() {
        return filteredLgnUserMstrDTO;
    }

    public void setFilteredLgnUserMstrDTO(List<LgnUserMstrDTO> filteredLgnUserMstrDTO) {
        this.filteredLgnUserMstrDTO = filteredLgnUserMstrDTO;
    }

    public LazyDataModel<LgnUserMstrDTO> getLdm() {
        return ldm;
    }

    public void setLdm(LazyDataModel<LgnUserMstrDTO> ldm) {
        this.ldm = ldm;
    }

    public TypeUser getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(TypeUser typeUser) {
        this.typeUser = typeUser;
        try {
            List<LgnUserMstrDTO> list = serviceFacade.getUserMstrAll(typeUser);
            if (ldmGeneric != null) {
                ldmGeneric.setDatasource(list);
            } else {
                ldmGeneric = new LazyLgnUserMstrDataModel(list);
            }
            ldm = ldmGeneric;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void filterListener(FilterEvent filterEvent) {
        ldmGeneric.setTable((DataTable) filterEvent.getSource());
    }

    public List<LgnProfilesDTO> getListLgnProfilesDTO() {
        return listLgnProfilesDTO;
    }

    public void setListLgnProfilesDTO(List<LgnProfilesDTO> listLgnProfilesDTO) {
        this.listLgnProfilesDTO = listLgnProfilesDTO;
    }

    public LgnProfilesDTO getPlayer() {
        return player;
    }

    public void setPlayer(LgnProfilesDTO player) {
        this.player = player;
    }

    public HtmlCommandButton getBtnClientNew() {
        return btnClientNew;
    }

    public void setBtnClientNew(HtmlCommandButton btnClientNew) {
        this.btnClientNew = btnClientNew;
    }

    public DataTable getDtLazyDataTable() {
        return dtLazyDataTable;
    }

    public void setDtLazyDataTable(DataTable dtLazyDataTable) {
        this.dtLazyDataTable = dtLazyDataTable;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public SysClntMstrDTO getSelectedClntMstr() {
        return selectedClntMstr;
    }

    public void setSelectedClntMstr(SysClntMstrDTO selectedClntMstr) {
        this.selectedClntMstr = selectedClntMstr;
    }

    public List<SysClntMstrDTO> getFilteredSysClntMstrDTO() {
        return filteredSysClntMstrDTO;
    }

    public void setFilteredSysClntMstrDTO(List<SysClntMstrDTO> filteredSysClntMstrDTO) {
        this.filteredSysClntMstrDTO = filteredSysClntMstrDTO;
    }

    public List<SysClntMstrDTO> getListSysClntMstrDTO() {
        return listSysClntMstrDTO;
    }

    public void setListSysClntMstrDTO(List<SysClntMstrDTO> listSysClntMstrDTO) {
        this.listSysClntMstrDTO = listSysClntMstrDTO;
    }

    public boolean isGlobalFilter2Active() {
        return globalFilter2Active;
    }

    public void setGlobalFilter2Active(boolean globalFilter2Active) {
        this.globalFilter2Active = globalFilter2Active;

    }

    public enum ModoOperacion {

        NUEVO(0), MODIFICAR(1);
        private final int modoOperacion;

        ModoOperacion(int clasificacion) {
            this.modoOperacion = clasificacion;
        }

        public int getModoOperacion() {
            return modoOperacion;
        }
    }

    public ModoOperacion getModoOperacion() {
        return modoOperacion;
    }

    public void setModoOperacion(ModoOperacion modoOperacion) {
        this.modoOperacion = modoOperacion;
    }

    public String getTitleDialogOperation() {
        return titleDialogOperation;
    }

    public void setTitleDialogOperation(String titleDialogOperation) {
        this.titleDialogOperation = titleDialogOperation;
    }

    public boolean isActiveBtnEliminar() {
        return activeBtnEliminar;
    }

    public void setActiveBtnEliminar(boolean activeBtnEliminar) {
        this.activeBtnEliminar = activeBtnEliminar;
    }

    public boolean isActiveUsuario() {
        return activeUsuario;
    }

    public void setActiveUsuario(boolean activeUsuario) {
        this.activeUsuario = activeUsuario;
    }

    public SysClntMstrDTO getNewClntMstr() {
        return newClntMstr;
    }

    public void setNewClntMstr(SysClntMstrDTO newClntMstr) {
        this.newClntMstr = newClntMstr;
    }

    public boolean isActiveChbEAD() {
        return activeChbEAD;
    }

    public void setActiveChbEAD(boolean activeChbEAD) {
        this.activeChbEAD = activeChbEAD;
    }

    public boolean isActiveChbRAD() {
        return activeChbRAD;
    }

    public void setActiveChbRAD(boolean activeChbRAD) {
        this.activeChbRAD = activeChbRAD;
    }

    public boolean isActiveChbFXC() {
        return activeChbFXC;
    }

    public void setActiveChbFXC(boolean activeChbFXC) {
        this.activeChbFXC = activeChbFXC;
    }

    public SysAddrMstrDTO getNewAddrMstr() {
        return newAddrMstr;
    }

    public void setNewAddrMstr(SysAddrMstrDTO newAddrMstr) {
        this.newAddrMstr = newAddrMstr;
    }

    public boolean isActiveChbStatusUserMstr() {
        return activeChbStatusUserMstr;
    }

    public void setActiveChbStatusUserMstr(boolean activeChbStatusUserMstr) {
        this.activeChbStatusUserMstr = activeChbStatusUserMstr;
    }

    public boolean isDisabledChbStatusUserMstr() {
        return disabledChbStatusUserMstr;
    }

    public void setDisabledChbStatusUserMstr(boolean disabledChbStatusUserMstr) {
        this.disabledChbStatusUserMstr = disabledChbStatusUserMstr;
    }

    public String getMaskRFC() {
        return maskRFC;
    }

    public void setMaskRFC(String maskRFC) {
        this.maskRFC = maskRFC;
    }

    public boolean isRequiredCustomer() {
        return requiredCustomer;
    }

    public void setRequiredCustomer(boolean requiredCustomer) {
        this.requiredCustomer = requiredCustomer;
    }

    public boolean isRequiredEmployee() {
        return requiredEmployee;
    }

    public void setRequiredEmployee(boolean requiredEmployee) {
        this.requiredEmployee = requiredEmployee;
    }

    public boolean isDisabledBtnEmailSend() {
        return disabledBtnEmailSend;
    }

    public void setDisabledBtnEmailSend(boolean disabledBtnEmailSend) {
        this.disabledBtnEmailSend = disabledBtnEmailSend;
    }
    */

}
