/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enginedata.core.web.controller;

import com.enginedata.web.util.ServiceLocator;
import com.enginedata.web.util.ModuleEngineConstants;
import com.enginedata.core.web.util.SecurityEngineData;
import java.io.Serializable;
import java.util.Map;
import javax.naming.NamingException;
import mx.com.enginedata.general.facade.bean.GeneralFacade;
//import mx.com.enginedata.properties.TypePropertiesConstants;
//import mx.com.enginedata.rad.facade.RadFacade;
import org.primefaces.model.LazyDataModel;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
//import com.enginedata.core.web.dto.FilterPageDTO;
import org.primefaces.event.SelectEvent;
import mx.com.enginedata.bean.util.enumerator.TypeUser;
import java.util.HashMap;
import org.primefaces.context.RequestContext;
import com.enginedata.web.util.FacesUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants;
import mx.com.enginedata.general.model.dto.LgnUserMstrDTO;
import mx.com.enginedata.lgn.dto.LgnUserFncDTO;
import mx.com.enginedata.lgn.dto.SysBrncMstrDTO;
import mx.com.enginedata.lgn.dto.SysSiteMstrDTO;
import org.primefaces.model.DualListModel;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author jruiz
 */
@ViewScoped
@ManagedBean(name = "favoriteController")
public class FavoriteController implements Serializable {

    //RadFacade serviceFacade;
//    RadGuiaHeadDTO selectedItem;
//    RadGuiaHeadDTO newItem;
//    FilterPageDTO filters;
    SecurityEngineData securityUser;
    GeneralFacade generalFacade;
    //RadGuiaAddrDTO selectedAddr;
    LgnUserFncDTO favoritesUsrDTO;
    FacesUtils facesUtils;

    public LgnUserMstrDTO player;
    public String guiaSeleccionada;

    public LgnUserMstrDTO getPlayer() {
        return player;
    }

    public void setPlayer(LgnUserMstrDTO player) {
        this.player = player;
    }

    public String getGuiaSeleccionada() {
        return guiaSeleccionada;
    }

    public void setGuiaSeleccionada(String guiaSeleccionada) {
        this.guiaSeleccionada = guiaSeleccionada;
    }

    public LgnUserMstrDTO selectedUserMstr;

    public LgnUserMstrDTO getSelectedUserMstr() {
        return selectedUserMstr;
    }

    public void onRowUnSelect(SelectEvent event) {
        //System.out.println("onRowSelect " + selectRadHead.getGuiaNo());
    }

    //Rad guia head se crean el get
    //FIN
    //Combo sucursales get de los dtos y del lazy
    //INICIO   
    SysBrncMstrDTO selectedBrnc;

    public SysBrncMstrDTO getSelectedBrnc() {
        return selectedBrnc;
    }

    private LazyDataModel<SysBrncMstrDTO> lazyModelSysBrncMstr;

    public LazyDataModel<SysBrncMstrDTO> getLazyModelSysBrncMstr() {
        return lazyModelSysBrncMstr;
    }

    //FIN
//***************************************    
//  LAZY USER MASTER inicio
//***************************************
    private LazyDataModel<LgnUserMstrDTO> LazyLgnUserMstr;

    public LazyDataModel<LgnUserMstrDTO> getLazyLgnUserMstr() {
        return LazyLgnUserMstr;
    }

    public void setLazyLgnUserMstr(LazyDataModel<LgnUserMstrDTO> LazyLgnUserMstr) {
        this.LazyLgnUserMstr = LazyLgnUserMstr;
    }

    public FavoriteController() {
        try {
            //serviceFacade = (RadFacade) ServiceLocator.getInstance().getEJB(ModuleEngineConstants.JNDI_NAME_RAD_MODULE_BEAN);
            generalFacade = (GeneralFacade) ServiceLocator.getInstance().getEJB(ModuleEngineConstants.JNDI_NAME_GENERAL_MODULE_BEAN);

        } catch (NamingException ex) {
            ex.printStackTrace();
        }

//        if (filters == null) {
//            filters = new FilterPageDTO();
//
//        }
    }

    public List<LgnUserFncDTO> listUsrFunction = new ArrayList<LgnUserFncDTO>();

    public List<LgnUserFncDTO> getListUsrFunction() {
        return listUsrFunction;
    }

    public void setListUsrFunction(List<LgnUserFncDTO> listUsrFunction) {
        this.listUsrFunction = listUsrFunction;
    }

    public List<LgnUserFncDTO> listFunctionFav = new ArrayList<LgnUserFncDTO>();

    public void setListFunctionFav(List<LgnUserFncDTO> listFunctionFav) {
        this.listFunctionFav = listFunctionFav;
    }

//     private ThemeService service;
    private DualListModel<LgnUserFncDTO> Listas;

    public DualListModel<LgnUserFncDTO> getListas() {
        return Listas;
    }

    public void setListas(DualListModel<LgnUserFncDTO> Listas) {
        this.Listas = Listas;
    }

    public List<LgnUserFncDTO> ListFav;

    public MenuModel modelFavoritos;

    //@PostConstruct
    public void onLoadPersonalizable() {
        try {
            if (!FacesContext.getCurrentInstance().isPostback()) {
                Listas = new DualListModel<LgnUserFncDTO>();
                loadLinksFavoritos();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onClickFavorites() {
        try {
            securityUser = SecurityEngineData.getSecurityEngineData();
            tipoUsuario = securityUser.getTypeUser();
            String userId = securityUser.getUserMstrDTO().getUmUserId();

            //SecurityEngineData security = SecurityEngineData.getSecurityEngineData();

            String profileId = "5";//security.getUserMstrDTO().getIdProfileDTO().getIdProfile();
            String[] parametros = {profileId, userId};
            String[] parametros2 = {userId};

            //Llenado de dos listas
            
            List<LgnUserFncDTO> ListFunct = null;//generalFacade.getAllItemsByFilters("query_modUser", parametros, new LgnUserFncDTO(), TypePropertiesConstants.PRINCIPAL);
            //ListFav = generalFacade.getAllItemsByFilters("query_funcUser", parametros2, new LgnUserFncDTO(), TypePropertiesConstants.PRINCIPAL);

            //Se juntan las dos listas y se insertan ene l dual list que es el que lee el picklist
            Listas = new DualListModel<LgnUserFncDTO>(ListFunct, ListFav);

            loadLinksFavoritos();

            if (!tipoUsuario.equals(securityUser.getTypeUser().INTERNO)) {
                getShortCutClients();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void loadLinksFavoritos() {
        try {
            securityUser = SecurityEngineData.getSecurityEngineData();
            tipoUsuario = securityUser.getTypeUser();
            securityUser = SecurityEngineData.getSecurityEngineData();
            tipoUsuario = securityUser.getTypeUser();
            if (securityUser.getUserMstrDTO() != null) {

                String userId = securityUser.getUserMstrDTO().getUmUserId();
                SecurityEngineData security = SecurityEngineData.getSecurityEngineData();
                String[] parametros2 = {userId};
                modelFavoritos = new DefaultMenuModel();
                if (Listas.getTarget().isEmpty()) {
                    //ListFav = generalFacade.getAllItemsByFilters("query_funcUser", parametros2, new LgnUserFncDTO(), TypePropertiesConstants.PRINCIPAL);
                    Listas.setTarget(ListFav);
                }
                List<LgnUserFncDTO> targetDTOs = Listas.getTarget();
                listFavorites = new ArrayList<LgnUserFncDTO>();
                LgnUserFncDTO objGeneric = new LgnUserFncDTO();
                String title = facesUtils.getLocaleLabel("enginedata.page.general.label.coberturas");
                objGeneric = new LgnUserFncDTO();
                objGeneric.setUrlFav("mx/com/enginedata/utileria/busqueda/busquedaCodigoPostal.jsf");
                objGeneric.setDescripcion(title);
                objGeneric.setNombre(title);
                objGeneric.setLfIcon("fa fa-globe Fs18 Fleft MarRight10");
                listFavorites.add(objGeneric);
                DefaultMenuItem itemA = new DefaultMenuItem(title);
                itemA.setOncomplete("rmOpenDialogBusq()");
                itemA.setStyleClass("fa fa-globe Fs18 Fleft MarRight10");
                itemA.setTitle(title);
                modelFavoritos.addElement(itemA);

                for (LgnUserFncDTO object : targetDTOs) {
                    String multiDialog = "";
                    String txt = "";
                    String titleModalWindow = "";
                    titleModalWindow = facesUtils.getLocaleLabel(object.getNombreFav());

                    if (facesUtils.getLocaleLabel(object.getNombreFav()).length() < 25) {
                        txt = facesUtils.getLocaleLabel(object.getNombreFav());
                    } else {
                        txt = facesUtils.getLocaleLabel(object.getNombreFav()).substring(0, 25).concat("...");
                    }

                    multiDialog = object.getLfMultiDialog();
                    DefaultMenuItem item = new DefaultMenuItem(txt);

                    String urlItem = object.getUrlFav();
                    String typeVersion = "";
                    if (urlItem.contains("?")) {
                        urlItem = urlItem + "&" + SecurityEngineData.getSecurityEngineData().getVersionApp();
                        typeVersion = "&";
                    } else {
                        urlItem = urlItem + "?" + SecurityEngineData.getSecurityEngineData().getVersionApp();
                        typeVersion = "?";
                    }

                    int positionglp = urlItem.lastIndexOf("/");
                    int positionAns = urlItem.lastIndexOf(typeVersion);
                    String buscarglp = urlItem.substring(positionglp + 1, positionAns);
                    buscarglp = buscarglp.substring(0, buscarglp.length() - 4);

                    //item.setOnclick("PF('" + buscarglp + "').show();validateVisible('" + buscarglp + "');refreshIframe('ifrMainContextModal" + buscarglp + "','" + urlItem + "');");
                    //item.setOnclick("refreshMainContext('" + urlItem + "')");
                    if (multiDialog == null) {
                        multiDialog = "N";
                    }

                    if (multiDialog.compareTo("Y") != 0) {
                        item.setOnclick("refreshMainContext('" + urlItem + "')");
                    } else {
                        item.setOnclick("validateVisible('" + buscarglp + "','ifrMainContextModal" + buscarglp + "','" + urlItem + "')");
                    }

                    item.setStyleClass(object.getLfIcon());
                    item.setTitle(facesUtils.getLocaleLabel(object.getNombreFav()));
                    modelFavoritos.addElement(item);

                    objGeneric = new LgnUserFncDTO();
                    objGeneric.setUrlFav(urlItem);
                    objGeneric.setDescripcion(buscarglp);
                    objGeneric.setNombre(titleModalWindow);
                    objGeneric.setLfIcon(object.getLfIcon());
                    listFavorites.add(objGeneric);

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    List<LgnUserFncDTO> listFavorites = null;

    public List<LgnUserFncDTO> getListFavorites() {
        return listFavorites;
    }

    public void setListFavorites(List<LgnUserFncDTO> listFavorites) {
        this.listFavorites = listFavorites;
    }

    public void listen() {
        FacesContext context = null;
        context = FacesContext.getCurrentInstance();
        Map<String, String> params = context.getExternalContext().getRequestParameterMap();
        //System.out.println(params.get("url"));
        //String url = params.get("url");
        String url = (String) params.get("url");//Active dialog
        String link = "";
        Map<String, Object> options = new HashMap<String, Object>();
        options.clear();
        options.put("resizable", false);
        options.put("draggable", true);
        options.put("modal", false);
        //options.put("maximizable", true);
        options.put("minimizable", true);
        //options.put("contentHeight", 300);
        options.put("contentWidth", 1000);

        link = url.substring(0, url.length() - 4);
        //System.err.println(("url "+link));
        RequestContext.getCurrentInstance().openDialog(link, options, null);
        //options.clear();
    }

    public void getModal(String url) {

        String link = "";
        Map<String, Object> options = new HashMap<String, Object>();
//        options.put("resizable", false);
//        options.put("draggable", false);
//        options.put("modal", false);
//        options.put("contentWidth", 1000);
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        options.put("contentWidth", 1000);
        options.put("minimizable", true);
        link = url.substring(0, url.length() - 4);
        //System.err.println(link);
        RequestContext.getCurrentInstance().openDialog("/mx/com/enginedata/rad/radMonitoreo.jsf", options, null);
        //RequestContext.getCurrentInstance().openDialog(link, options, null);    
        //linkCoberturas();
    }

    public MenuModel getModel() {
        return modelFavoritos;
    }

    public void errorMessage(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
        RequestContext.getCurrentInstance().update("formModel");
        reloadPage("5000");
    }

    public void saveMessage(String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(facesUtils.getLocaleLabel("enginedata.page.general.label.mensajeinformacion"), mensaje));
        RequestContext.getCurrentInstance().update("formModel");
        reloadPage("5000");
    }

    public void reloadPage(String timeMS) {
        RequestContext.getCurrentInstance().execute("setTimeout(\"parent.location.reload()\"," + timeMS + ")");
    }

    public void saveListFavorites() {
        securityUser = SecurityEngineData.getSecurityEngineData();
        tipoUsuario = securityUser.getTypeUser();
        String userId = securityUser.getUserMstrDTO().getUmUserId();
        //String typeUser = securityUser.getUserMstrDTO().umUserType;
        //Pregunta si eres usuario INTERNO
        if (tipoUsuario.equals(securityUser.getTypeUser().INTERNO)) {

            if (Listas.getTarget().size() < 6) {
                try {
                    List<LgnUserFncDTO> targetDTOs = Listas.getTarget();
                    String response = "";//generalFacade.savePickListFavorites(targetDTOs, userId);
                    if (response.compareTo("1") == 0) {
                        //List<LgnUserFncDTO> sourceDTOs = Listas.getSource();
                        loadLinksFavoritos();
                        saveMessage(facesUtils.getLocaleLabel("enginedata.page.general.label.guardado"));
                    } else {
                        saveMessage(facesUtils.getLocaleLabel("enginedata.page.general.error.label.procesar"));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            } else {
                errorMessage(facesUtils.getLocaleLabel("enginedata.page.general.label.nivelMaximoFavoritos"));
            }
        } else {
            errorMessage(facesUtils.getLocaleLabel("enginedata.page.general.label.sinPermiso"));
        }

    }

//    Link para abrir la ventana modal de coberturas
    public void linkCoberturas() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", false);
        options.put("contentWidth", 1000);
        options.put("minimizable", true);
        options.put("draggable", true);
        RequestContext.getCurrentInstance().openDialog("/mx/com/enginedata/utileria/busqueda/busquedaCodigoPostal", options, null);
    }

    public TypeUser tipoUsuario;

    public void validaTypeUser() {

    }

    public void getShortCutClients() throws Exception {
        //String idEmployee = securityUser.getUserMstrDTO().idEmployee; 
        String[] parametros = {"2"};

        //Llenado de dos listas
        List<LgnUserFncDTO> listAtajosClnt = generalFacade.getAllItemsByFilters("query_getShortCutClnt", parametros, new LgnUserFncDTO(), TypePropertiesConstants.PRINCIPAL);
        modelFavoritos = new DefaultMenuModel();
        for (LgnUserFncDTO object : listAtajosClnt) {
            String txt = "";

            if (object.getUrlFav().compareTo("mx/com/enginedata/general/catalogs/lstArtefactType.jsf") != 0) {

                if (facesUtils.getLocaleLabel(object.getNombreFav()).length() < 25) {
                    txt = facesUtils.getLocaleLabel(object.getNombreFav());
                } else {
                    txt = facesUtils.getLocaleLabel(object.getNombreFav()).substring(0, 25).concat("...");
                }

                DefaultMenuItem item = new DefaultMenuItem(txt);
                item.setOnclick("refreshMainContext('" + object.getUrlFav() + "')");
                item.setStyleClass(object.getLfIcon());
                item.setTitle(facesUtils.getLocaleLabel(object.getNombreFav()));
                modelFavoritos.addElement(item);

            }

        }

    }

    SysSiteMstrDTO selectSysSiteMstrDTO = new SysSiteMstrDTO();
    List<SysSiteMstrDTO> listSysSiteMstrDTO = new ArrayList<SysSiteMstrDTO>();
    SysBrncMstrDTO selectBrncMstr = new SysBrncMstrDTO();
    List<SysBrncMstrDTO> listBrncMstr = new ArrayList<SysBrncMstrDTO>();

    public void getAllSiteAndBrcn() {
        try {
            String[] args = {securityUser.getUserMstrDTO().getUmUserAccess(), ""};
            listSysSiteMstrDTO = generalFacade.getAllItemsByFilters("query_getSitesByUser", args, new SysSiteMstrDTO(), TypePropertiesConstants.PRINCIPAL);

            if (securityUser.getBranchOwner() != null) {
                String siteId = securityUser.getBranchOwner().substring(0, 3);
                int index = 0;
                for (SysSiteMstrDTO dto : listSysSiteMstrDTO) {
                    if (dto.getSiteId().equalsIgnoreCase(siteId)) {
                        selectSysSiteMstrDTO = (SysSiteMstrDTO) listSysSiteMstrDTO.get(index);
                        getAllBrcnBySite(selectSysSiteMstrDTO, "load");
                        break;
                    }
                    index++;
                }
            } else if (!listSysSiteMstrDTO.isEmpty()) {
                selectSysSiteMstrDTO = (SysSiteMstrDTO) listSysSiteMstrDTO.get(0);
                getAllBrcnBySite(selectSysSiteMstrDTO, "load");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onChangeCbxSite(ValueChangeEvent event) {
        try {
            if (event != null) {
                //selectSysSiteMstrDTO = (SysSiteMstrDTO) event.getNewValue();
            }
            if (selectSysSiteMstrDTO != null) {
                getAllBrcnBySite(selectSysSiteMstrDTO, "change");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void getAllBrcnBySite(SysSiteMstrDTO selectSysSiteMstrDTO, String type) {
        try {
            String[] args = {securityUser.getUserMstrDTO().umUserAccess, selectSysSiteMstrDTO.getSiteId(), ""};
            listBrncMstr = generalFacade.getAllItemsByFilters("query_getBrncByUser", args, new SysBrncMstrDTO(), TypePropertiesConstants.PRINCIPAL);
            if (type.compareTo("load") != 0) {
                int index = 0;
                for (SysBrncMstrDTO dto : listBrncMstr) {
                    if (dto.getBmBrncID().contains("01")) {
                        selectBrncMstr = (SysBrncMstrDTO) listBrncMstr.get(index);
                        break;
                    }
                    index++;
                }
            } else {
                String brncId = securityUser.getBranchOwner();
                int index = 0;
                for (SysBrncMstrDTO dto : listBrncMstr) {
                    if (dto.getBmBrncID().equalsIgnoreCase(brncId)) {
                        selectBrncMstr = (SysBrncMstrDTO) listBrncMstr.get(index);
                        break;
                    }
                    index++;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    String ipWebSocket = "";

    public String getPropertyByName(String name) throws IOException {
        FileInputStream is = null;
        String response = "";
        try {
            String ruta = "";
            Properties properties;

            ruta = System.getProperty("user.dir") + File.separator + "enginedataConfig" + File.separator + "configSipWeb" + File.separator + "configApplication.properties";
            is = new FileInputStream(ruta);
            properties = new Properties();
            properties.load(is);

            response = properties.getProperty(name);

        } catch (Exception e) {
            e.printStackTrace();
            //throw new SmartGeneralException("No se encontro el Archivo " + name + ".properties");
        } finally {
            if (is != null) {
                is.close();
                is = null;
            }
        }
        return response;
    }

    public void getUrlWebSocket() {
        try {
            String url = "";
            //url = getPropertyByName("urlWebSocket");
            //System.out.println(url); 
            ipWebSocket = url;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void changeBrnc() {
        try {
            //securityUser.setUserMstrDTO(player);
            securityUser.setBranchOwner(selectBrncMstr.getBmBrncID().toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public SysBrncMstrDTO getSelectBrncMstr() {
        return selectBrncMstr;
    }

    public void setSelectBrncMstr(SysBrncMstrDTO selectBrncMstr) {
        this.selectBrncMstr = selectBrncMstr;
    }

    public List<SysBrncMstrDTO> getListBrncMstr() {
        return listBrncMstr;
    }

    public void setListBrncMstr(List<SysBrncMstrDTO> listBrncMstr) {
        this.listBrncMstr = listBrncMstr;
    }

    public SysSiteMstrDTO getSelectSysSiteMstrDTO() {
        return selectSysSiteMstrDTO;
    }

    public void setSelectSysSiteMstrDTO(SysSiteMstrDTO selectSysSiteMstrDTO) {
        this.selectSysSiteMstrDTO = selectSysSiteMstrDTO;
    }

    public List<SysSiteMstrDTO> getListSysSiteMstrDTO() {
        return listSysSiteMstrDTO;
    }

    public void setListSysSiteMstrDTO(List<SysSiteMstrDTO> listSysSiteMstrDTO) {
        this.listSysSiteMstrDTO = listSysSiteMstrDTO;
    }

    private boolean skip;

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String getIpWebSocket() {
        return ipWebSocket;
    }

    public void setIpWebSocket(String ipWebSocket) {
        this.ipWebSocket = ipWebSocket;
    }

}
