/*
 * Codigo producido por Engine Data
 * Todos los derechos reservados.  * 
 */
package com.enginedata.core.web.controller;

/**
 *
 * @author Edgar
 */
import com.enginedata.core.web.util.SecurityEngineData;
import com.enginedata.web.util.ModuleEngineConstants;
import com.enginedata.web.util.ServiceLocator;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import mx.com.enginedata.general.facade.bean.GeneralFacade;
import mx.com.enginedata.lgn.dto.LgnActionDTO;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Edgar
 */
@ViewScoped
public class MasterController implements Serializable {

    protected List<LgnActionDTO> listActionExcepcion;
    private GeneralFacade generalFacade;
    private int numRowDefault = 17;
    private int numRowPaginator = numRowDefault;

    public MasterController() {
        try {
            //System.out.println("MasterController-Constructor");
            generalFacade = (GeneralFacade) ServiceLocator.getInstance().getEJB(ModuleEngineConstants.JNDI_NAME_GENERAL_MODULE_BEAN);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onLoad() {
        //System.out.println("MasterController-onLoad");
        numRowPaginator = numRowDefault;
        String relativoUri = ((HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest()).getServletPath();
        try {
            relativoUri = relativoUri.substring(1, relativoUri.length());
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("radGuiaHeadDTOMonitor")) {
                if (SecurityEngineData.getSecurityEngineData().getUserMstrDTO() != null) {
                    //listActionExcepcion = generalFacade.getExcepcionActionByProfile(
                }
            } else if (SecurityEngineData.getSecurityEngineData().getUserMstrDTO() != null) {
                //listActionExcepcion = generalFacade.getExcepcionActionByProfile(relativoUri, SecurityEngineData.getSecurityEngineData().getUserMstrDTO().getIdProfileDTO().getIdProfile());
            }
            onLoadPersonalizable();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void onLoadPersonalizable() {

    }

    public boolean actionPermitido(String action) {
        boolean permitido = false;
        if (listActionExcepcion != null && !listActionExcepcion.isEmpty()) {
            for (LgnActionDTO dto : listActionExcepcion) {
                if (dto.getName().contains(action)) {
                    permitido = true;
                    break;
                }
            }
        }
        return permitido;
    }

    public String getArrayRowPaginator() {
        //System.out.println("MasterController-getArrayRowPaginator");
        int factor = 1;
        int cadena = 0;
        String response = "";
        while (factor <= 4) {
            cadena = numRowDefault * factor;
            if (factor <= 3) {
                response = response + Integer.toString(cadena) + ",";
            } else {
                response = response + Integer.toString(cadena);
            }
            factor++;
        }
        return response;
    }
    
    public void showGuiaDetail(String searchedBokGuiaHead, String formNo, String selectedRef){
        try {
            Map<String, Object> options = new HashMap<String, Object>();
            options.put("resizable", false);
            options.put("draggable", true);
            options.put("modal", true);
            options.put("contentWidth", 1000);
            options.put("contentHeight", 540);
            
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("external", true);
            map.put("action", "EDIT");
            map.put("guiaSelected", searchedBokGuiaHead);
            map.put("formNo", formNo);
            map.put("selectedRef", selectedRef);
           
//            Map<String, String> guiaParams = new HashMap<String, String>();
//            guiaParams.put("guiaNo", searchedBokGuiaHead);
//            guiaParams.put("formNo", formNo);
            
//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("guiaParams", guiaParams);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("guiaDetailData", map);
            
            //RequestContext.getCurrentInstance().openDialog("

        } catch (Exception ex) {
            //Logger.getLogger(RadController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void findHistoryByGuiaNoOrRastreo(String forma, String rastreo){
         try{ 
//             update=":form:tableRastreo"  oncomplete="PF('DialogHistory').show();"
//                 rastreo = rastreo; 
//                if (formNo.startsWith("RA")){
//                    historyDTO = generalFacade.getTraceabilityByGuia(rastreo);
//                } else {
//                    historyDTO = generalFacade.getTraceabilityByGuia_no_Rad(rastreo);
//                }
             
             
//             RequestContext.getCurrentInstance().update(":form:tableRastreo");
//             RequestContext.getCurrentInstance().execute("PF('DialogHistory').show();");
             
             //historyDTO = generalFacade.getHistory(rastreo);
        
                Map<String, Object> options = new HashMap<String, Object>();
                options.put("resizable", false);
                options.put("draggable", true);
                options.put("modal", true);
                options.put("contentWidth", 860);
                options.put("contentHeight", 450);
                Map<String, String> guiaParams = new HashMap<String, String>();
                guiaParams.put("guiaNo", rastreo);
                guiaParams.put("formNo", forma.substring(5));
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("guiaParams", guiaParams);
                //RequestContext.getCurrentInstance().openDialog("

          }catch (Exception ex) {
            //Logger.getLogger(RadController.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    public int getNumRowPaginator() {
        return numRowPaginator;
    }

    public void setNumRowPaginator(int numRowPaginator) {
        this.numRowPaginator = numRowPaginator;
    }

}

