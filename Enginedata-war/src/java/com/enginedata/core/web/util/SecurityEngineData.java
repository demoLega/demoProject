/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enginedata.core.web.util;

//import com.enginedata.Encryp;
//import com.enginedata.core.ejb.util.ServiceLocator;
//import com.enginedata.core.general.ModuleEngineConstants;
//import mx.com.enginedata.bean.util.DataSecurityDTO;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import mx.com.enginedata.bean.util.enumerator.TypeUser;
import mx.com.enginedata.general.model.dto.LgnUserMstrDTO;
//import mx.com.enginedata.bean.util.ConfiguracionServidorDTO;
//import mx.com.enginedata.bean.util.enumerator.TypeUser;
//import mx.com.enginedata.comun.SmartGeneralException;
//import mx.com.enginedata.general.facade.bean.GeneralFacade;
//import mx.com.enginedata.general.model.dto.LgnUserMstrDTO;
//import mx.com.enginedata.utileria.facade.bean.EmailFacade;
import org.primefaces.context.RequestContext;

/**
 *
 * @author jruiz
 */
@SessionScoped
@ManagedBean(name = "securityEngineData")
public class SecurityEngineData {

    private Locale locale;
//    GeneralFacade generalFacade;
//    EmailFacade emailFacade;
    LgnUserMstrDTO userMstrDTO;
    private boolean disabledBtnEmailSend = false;
    private TypeUser typeUser = TypeUser.INTERNO;
    private String userEmail;
    private boolean isLocateMX = true;
    private String userAccess;
    private String userPwd;
    private String usuario;
    private String correo;
    private HtmlInputText txtCorreo;
    private String branchOwner;
    private String captcha;
    private String versionApp = "1", ambiente;

    private String userAccessSession;
    private String userPwdSession;
    private String typePrint;
    private String callbackConvert;
    private List<String> guiaNoToPrint;

    public TypeUser getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(TypeUser type) {
        this.typeUser = type;
    }
    
    public SecurityEngineData() {
//        try {
//            generalFacade = (GeneralFacade) ServiceLocator.getInstance().getEJB(ModuleEngineConstants.JNDI_NAME_GENERAL_MODULE_BEAN);
//            emailFacade = (EmailFacade) ServiceLocator.getInstance().getEJB(ModuleEngineConstants.JNDI_NAME_UTILERIA_EMAIL_BEAN);
//        } catch (NamingException ex) {
//            ex.printStackTrace();
//        }
    }

    @PostConstruct
    public void init() {

        if (locale == null) {
            locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        }
        isLocateMX = locale.getLanguage().equalsIgnoreCase("es");
    }

    public String authenticaSIPWeb() {
        try {
//            if (typeUser == null) {
//                typeUser = TypeUser.INTERNO;
//            }
//            DataSecurityDTO data = null;
            userAccess = userAccess.toUpperCase();
//            List<Object> listObj = generalFacade.authentication(typeUser, userAccess, new Encryp().encodeEncrypt(userPwd));
//            Boolean isSign = false;
//            if (listObj == null ? false : !listObj.isEmpty()) {
//                data = (DataSecurityDTO) listObj.get(0);
//                isSign = (Boolean) listObj.get(1);
//                branchOwner = (String) listObj.get(2);
//            }

            Boolean isSign = true;
            if (isSign) {
                return "admin";
                //userMstrDTO = generalFacade.getUserMstrForUserAccess(userAccess, typeUser.getType());
//                userMstrDTO = new LgnUserMstrDTO();
//                if (userMstrDTO == null) {
//                    FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=3" + buildParameterTypeUser());
//                } else {
//
////                    ambiente = generalFacade.checkPropertiesFilesQuerys();
////                    showVersionApp();
//                    HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
//                            .getExternalContext().getSession(false);
//                    session.setAttribute("username", "evillegas");
//                    
//                    return "index";
//                    //FacesContext.getCurrentInstance().getExternalContext().redirect("j_security_check?j_username=" + data.getUserBridge() + "&j_password=" + data.getPasswordBridge());
//                }
            } else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=1" + buildParameterTypeUser());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } /*catch (SmartGeneralException ex) {
            try {
                ex.printStackTrace();
                if(ex.getCodeError().equalsIgnoreCase("1")){
                    FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=4" + buildParameterTypeUser());
                }else{
                    FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=2" + buildParameterTypeUser());                    
                }
            } catch (IOException ex1) {
                ex.printStackTrace();
            }
        } */ catch (Exception ex) {
            try {
                ex.printStackTrace();
                FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=3" + buildParameterTypeUser());
            } catch (IOException ex1) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private void showVersionApp() throws Exception {
        try {
            String earDir = SecurityEngineData.class.getProtectionDomain().getCodeSource().toString();
            String pathEAR = earDir.replace("\\", "/");
            //System.out.println("URL: " + pathEAR);
            String[] pathEars = pathEAR.split("/");
            String eData = "";
            for (String pathEar : pathEars) {
                eData = pathEar;               
            }
            versionApp = eData;

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public void enviarSolicitud() {
        try {
            disabledBtnEmailSend = true;
            if (txtCorreo.getSubmittedValue() != null) {
                userEmail = (txtCorreo.getSubmittedValue().toString());
            }
            RequestContext.getCurrentInstance().update(":frmLogin:sendChangePsw");
            asyncServiceMethod();
            disabledBtnEmailSend = false;
            RequestContext.getCurrentInstance().update(":frmLogin:sendChangePsw");
        } catch (Exception ex) {
            ex.printStackTrace();
            disabledBtnEmailSend = false;
            RequestContext.getCurrentInstance().update(":frmLogin:btnEnvio");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al procesar el envio."));
        }
    }

    public void asyncServiceMethod() {
        Runnable task = new Runnable() {
            public void run() {
                try {
//                    if (userEmail != null) {
//                        LgnUserMstrDTO userMstrDTO = generalFacade.getUserMstrForEmail(userEmail, TypeUser.CLIENTE.getType());
//                        if (userMstrDTO != null) {
//                            emailFacade.envioEmailCustomerActivate("Cambio de contraseña", userEmail, userMstrDTO, true);
//                        } else {
//                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No existe el correo"));
//                        }
//                    } else {
//                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        new Thread(task, "ServiceThread").start();
    }

    private String buildParameterTypeUser() {
//        switch (getTypeUser()) {
//            case CLIENTE:
//                return "&type=" + getTypeUser().getType();
//            case PROVEEDOR:
//                return "&type=" + getTypeUser().getType();
//        }
        return "";
    }

    public String changeLanguage(String language, String country) {
        locale = new Locale(language, country);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
        isLocateMX = locale.getLanguage().equalsIgnoreCase("es");
        return "index.jsf?faces-redirect=true" + buildParameterTypeUser();
    }

    public String getUserLogin() {
        String username;
        username = this.getUser();
        if (username == null) {
            username = "";
        }
        return username;
    }

    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        session.invalidate();
        return "login.jsf?faces-redirect=true" + buildParameterTypeUser();
    }

    private String getUser() {
        return userAccess;
    }

    public static SecurityEngineData getSecurityEngineData() {
        HttpSession session = (HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return (SecurityEngineData) session.getAttribute("securityData");
    }

    public int getTimeout() {
        HttpSession session = (HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return session.getMaxInactiveInterval();
    }

    public String getIdGA() throws Exception {
        String idGA = "";
        try {
            //ConfiguracionServidorDTO configuracionServidorDTO = generalFacade.getConfiguracionServidor();
            //idGA = configuracionServidorDTO.getCookieGa();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
        }
        return idGA;
    }

    public void authenticaGeneratingCredentials() {
        try {
            String userAcessOld = getUserAccess();
            String userAcessNew = getUserAccessSession();
            setUserAccess(userAccessSession);
            setUserPwd(userPwdSession);
            //            if (typeUser == null) {
            //                typeUser = TypeUser.INTERNO;
            //            }
            //            DataSecurityDTO data = null;
            //            userAccess = userAccess.toUpperCase();
            //            List<Object> listObj = generalFacade.authentication(typeUser, userAccess, new Encryp().encodeEncrypt(userPwd));
            //            Boolean isSign = false;
            //            if (listObj == null ? false : !listObj.isEmpty()) {
            //                data = (DataSecurityDTO) listObj.get(0);
            //                isSign = (Boolean) listObj.get(1);
            //                branchOwner = (String) listObj.get(2);
            //            }
            //
            //            if (isSign) {
            //
            //                userMstrDTO = generalFacade.getUserMstrForUserAccess(userAccess, typeUser.getType());
            //                if (userMstrDTO == null) {
            //                    //FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=3" + buildParameterTypeUser());
            //                } else {
            //
            //                    ambiente = generalFacade.checkPropertiesFilesQuerys();
            //                    showVersionApp();
            //                    //FacesContext.getCurrentInstance().getExternalContext().redirect("j_security_check?j_username=" + data.getUserBridge() + "&j_password=" + data.getPasswordBridge());
            //                }
            //                //Con este
            //                //RequestContext.getCurrentInstance().execute("parent.location.reload()");
            //                if (userAcessOld.compareTo(userAcessNew) != 0) {
            //                    FacesContext.getCurrentInstance().getExternalContext().redirect("j_security_check?j_username=" + data.getUserBridge() + "&j_password=" + data.getPasswordBridge());
            //                }
            //                RequestContext.getCurrentInstance().execute("PF('alertNewLoginModal').hide()");
            //            } else {
            //                FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=1" + buildParameterTypeUser());
            //            }
            //        } catch (IOException ex) {
            //            ex.printStackTrace();
            //        } catch (SmartGeneralException ex) {
            //            try {
            //                ex.printStackTrace();
            //                if (ex.getCodeError().equalsIgnoreCase("1")) {
            //                    FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=4" + buildParameterTypeUser());
            //                } else {
            //                    FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=2" + buildParameterTypeUser());
            //                }
            //            } catch (IOException ex1) {
            //                ex.printStackTrace();
            //            }
            //        } 
        }catch (Exception ex) {
            try {
                ex.printStackTrace();
                FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf?e=3" + buildParameterTypeUser());
            } catch (IOException ex1) {
                ex.printStackTrace();
            }
        }
        }

    

    public String getBranchOwner() {
        return branchOwner;
    }

    public void setBranchOwner(String branchOwner) {
        this.branchOwner = branchOwner;
    }

    public Date getTimeServer() {
        java.util.Date fecha = new Date();
        return fecha;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getLanguage() {
        return locale.getLanguage();
    }

    public LgnUserMstrDTO getUserMstrDTO() {
        return userMstrDTO;
    }

    public void setUserMstrDTO(LgnUserMstrDTO userMstrDTO) {
        this.userMstrDTO = userMstrDTO;
    }
    
    public boolean isDisabledBtnEmailSend() {
        return disabledBtnEmailSend;
    }

    public void setDisabledBtnEmailSend(boolean disabledBtnEmailSend) {
        this.disabledBtnEmailSend = disabledBtnEmailSend;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public boolean isIsLocateMX() {
        return isLocateMX;
    }

    public void setIsLocateMX(boolean isLocateMX) {
        this.isLocateMX = isLocateMX;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public HtmlInputText getTxtCorreo() {
        return txtCorreo;
    }

    public void setTxtCorreo(HtmlInputText txtCorreo) {
        this.txtCorreo = txtCorreo;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserAccess() {
        return userAccess;
    }

    public void setUserAccess(String userAccess) {
        this.userAccess = userAccess;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public String getUserAccessSession() {
        return userAccessSession;
    }

    public void setUserAccessSession(String userAccessSession) {
        this.userAccessSession = userAccessSession;
    }

    public String getUserPwdSession() {
        return userPwdSession;
    }

    public void setUserPwdSession(String userPwdSession) {
        this.userPwdSession = userPwdSession;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getTypePrint() {
        return typePrint;
    }

    public void setTypePrint(String typePrint) {
        this.typePrint = typePrint;
    }

    public List<String> getGuiaNoToPrint() {
        return guiaNoToPrint;
    }

    public void setGuiaNoToPrint(List<String> guiaNoToPrint) {
        this.guiaNoToPrint = guiaNoToPrint;
    }

    public String getCallbackConvert() {
        return callbackConvert;
    }

    public void setCallbackConvert(String callbackConvert) {
        this.callbackConvert = callbackConvert;
    }

}
