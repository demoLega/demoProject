/*
 * Codigo producido por Engine Data
 * Todos los derechos reservados.  * 
 */
package com.enginedata.web.util;

/**
 *
 * @author Edgar
 */
import java.util.ResourceBundle;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;

public class FacesUtils {

    public static int cantidadMinimaCaracteres = 4;
    public static int cantidadMaximaRegistos = 1000;

    @SuppressWarnings("unchecked")
    public static <T> T get(String elExpression, Class<T> objClass) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ELContext elctx = fctx.getELContext();
        Application jsfApp = fctx.getApplication();
        ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        ValueExpression ve = null;

        ve = exprFactory.createValueExpression(
                elctx,
                elExpression,
                objClass);

        return (T) ve.getValue(elctx);
    }

    public static void set(String elExpression, Object value) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ELContext elctx = fctx.getELContext();
        Application jsfApp = fctx.getApplication();
        ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        ValueExpression ve = null;

        ve = exprFactory.createValueExpression(
                elctx,
                elExpression,
                Object.class);

        ve.setValue(elctx, value);
    }

    public static String getLocaleLabel(String propertyName) {
        //System.out.println("propertyName" + propertyName);
        FacesContext facesContext = FacesContext.getCurrentInstance();

        ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "msgs");

        if (!bundle.containsKey(propertyName)) {
            return propertyName;
        }
        return bundle.getString(propertyName);
    }

    public static String getLocaleLabel(String propertyName, ResourceBundle bundle) {
        if (!bundle.containsKey(propertyName)) {
            return propertyName;
        }
        return bundle.getString(propertyName);
    }

    public static ResourceBundle getResourceBundle(FacesContext facesContext) {
        return facesContext.getApplication().getResourceBundle(facesContext, "msgs");
    }
}
