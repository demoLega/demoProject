/*
 * Codigo producido por Engine Data
 * Todos los derechos reservados.  * 
 */
package com.enginedata.web.util;

/**
 *
 * @author Edgar
 */
/**
 *
 * @author jruiz
 */
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ServiceLocator {
  private static final ServiceLocator INSTANCE = new ServiceLocator();

  public ServiceLocator() {
    super();
  }

  public static ServiceLocator getInstance() {
    return ServiceLocator.INSTANCE;
  }
  private static Context context;

  public Object getEJB(String ejbServiceName) throws NamingException {
    if (context == null) {
      System.out.print("Entra context");
      context = new InitialContext();
    }
    return context.lookup(ejbServiceName);
  }

}

