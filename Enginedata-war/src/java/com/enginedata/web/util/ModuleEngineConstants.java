/*
 * Codigo producido por Engine Data
 * Todos los derechos reservados.  * 
 */
package com.enginedata.web.util;

/**
 *
 * @author Edgar
 */
public class ModuleEngineConstants {
     public static final String JNDI_NAME_GOBERNANCE_ARTEFACTTYPE_BEAN = "PQEXPRESS.GOBERNANCE.CATALOGS.GobernanceFacade#mx.com.enginedata.gobernance.facade.bean.GobernanceFacade";
    public static final String JNDI_NAME_GENERAL_MODULE_BEAN = "PQEXPRESS.GENERAL.CATALOGS.GeneralFacade#mx.com.enginedata.general.facade.bean.GeneralFacade";
    public static final String JNDI_NAME_RAD_MODULE_BEAN = "PQEXPRESS.RAD.RadFacade#mx.com.enginedata.rad.facade.RadFacade";
    public static final String JNDI_NAME_UTILERIA_EMAIL_BEAN = "PQEXPRESS.UTILERIA.EMAIL.EmailFacade#mx.com.enginedata.utileria.facade.bean.EmailFacade";
    public static final String JNDI_NAME_LOGISTIC_MODULE_BEAN = "PQEXPRESS.LOGISTIC.CATALOGS.LogisticFacade#mx.com.enginedata.logistic.facade.bean.LogisticFacade";
    public static final String JNDI_NAME_GENERIC_BEAN = "PQEXPRESS.GENERIC.GenericoFacade#mx.com.enginedata.api.generico.facade.GenericoFacade";
    public static final String JNDI_NAME_ANMO_MODULE_BEAN = "PQEXPRESS.ANOMALIAS.CONSULTAS.AnmoFacade#mx.com.enginedata.anmo.facade.bean.AnmoFacade";
    public static final String JNDI_NAME_EAD_MODULE_BEAN = "PQEXPRESS.EAD.CONSULTAS.EadFacade#mx.com.enginedata.ead.facade.bean.EadFacade";
    public static final String JNDI_NAME_CENTINEL_MODULE_BEAN = "PQEXPRESS.CENTINEL.CATALOGS.CentinelFacade#mx.com.enginedata.centinel.facade.bean.CentinelFacade";
    public static final String JNDI_NAME_COMMERCIAL_MODULE_BEAN = "PQEXPRESS.COMMERCIAL.CommercialFacade#mx.com.enginedata.logistic.facade.CommercialFacade";
}

