/*
 * Codigo producido por Engine Data
 * Todos los derechos reservados.  * 
 */
package mx.com.enginedata.bean.util.enumerator;

/**
 *
 * @author Edgar
 */
import java.io.Serializable;

/**
 *
 * @author jasanchez
 */
public enum MpProcType  implements Serializable{

    MENU("MENU"), ITEM("ITEM");
    private final String type;

    MpProcType(String clasificacion) {
        this.type = clasificacion;
    }

    public String getType() {
        return type;
    }

    public static MpProcType getEnum(String clasificador) {
        if (ITEM.getType().equalsIgnoreCase(clasificador)) {
            return ITEM;
        } else {
            return MENU;
        }
    }
}
