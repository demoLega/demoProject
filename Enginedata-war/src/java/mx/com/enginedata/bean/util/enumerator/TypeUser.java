/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.bean.util.enumerator;

import java.io.Serializable;

/**
 *
 * @author Edgar
 */
public enum TypeUser implements Serializable {

    CLIENTE("CUS"), PROVEEDOR("SUP"), INTERNO("INT");
    private final String type;

    TypeUser(String clasificacion) {
        this.type = clasificacion;
    }

    public String getType() {
        return type;
    }

    public static TypeUser getEnum(String clasificador) {
        if (CLIENTE.getType().equalsIgnoreCase(clasificador)) {
            return CLIENTE;
        } else if (PROVEEDOR.getType().equalsIgnoreCase(clasificador)) {
            return PROVEEDOR;
        } else {
            return INTERNO;
        }

    }
}
