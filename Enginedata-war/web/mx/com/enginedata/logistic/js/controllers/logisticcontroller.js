
app.controller('logisticController',['$scope','$http',
    function($scope, $http) {
     
     $(document).keyup(function(e) {     
    if(e.keyCode== 27) {
       // alert('Esc key is press');  
     //   PF('fichatecnica').hide();
    } 
            });
     
  $scope.selectedUdto = {};
    $scope.handleComplete = function (xhr, status, args){
   $scope.selectedUdto  = JSON.parse(args.selectedUdto) ;
   
             console.log(   $scope.selectedUdto);
             
           function setmarker(){
         gMap = new google.maps.Map(document.getElementById('formGMapDialog:gmapD')); 
          gMap.setZoom(15);      // This will trigger a zoom_changed on the map
          gMap.setCenter(new google.maps.LatLng(args.lat , args.lng));
          //Pintar las sucursales
          $scope.imagenes = {
              sucursalDestino : "../../../../../resources/images/logistic/sucursalDestino.png",
               sucursalOrigen : "../../../../../resources/images/logistic/sucursalOrigen.png",
               truckGreen : '../../../../../resources/images/logistic/truckGreen.png',
                truckBlue : '../../../../../resources/images/logistic/truck.png'
          };
         
          
          if ( parseFloat($scope.selectedUdto.posicion.speed) > 0){
              //Verde
                 var marker = new google.maps.Marker({
            position: new google.maps.LatLng(args.lat,args.lng),
             map: gMap,
               icon:$scope.imagenes.truckGreen,
             title:$scope.selectedUdto.segmentsEdit.segmentsDTO.tsTruckNo
        });
          }
          else if ( parseFloat($scope.selectedUdto.posicion.speed) == 0){
           //Azul
              var marker = new google.maps.Marker({
            position: new google.maps.LatLng(args.lat,args.lng),
             map: gMap,
               icon: $scope.imagenes.truckBlue,
             title:$scope.selectedUdto.segmentsEdit.segmentsDTO.tsTruckNo
        });
                   
          }
          var tsStrtBrnc = new google.maps.Marker({
            position: new google.maps.LatLng($scope.selectedUdto.segmentsEdit.segmentsDTO.bmLocaLat_origen,$scope.selectedUdto.segmentsEdit.segmentsDTO.bmLocaLon_origen),
             map: gMap,
               icon:  $scope.imagenes.sucursalOrigen,
             title:$scope.selectedUdto.segmentsEdit.segmentsDTO.tsStrtBrnc
        });
        var tsDestBrnc = new google.maps.Marker({
            position: new google.maps.LatLng($scope.selectedUdto.segmentsEdit.tsDestBrnc.bmLocaLat,$scope.selectedUdto.segmentsEdit.tsDestBrnc.bmLocaLon),
             map: gMap,
               icon:  $scope.imagenes.sucursalDestino,
             title:$scope.selectedUdto.segmentsEdit.bmBrncId
        });
         
}
setTimeout(setmarker, 500);
};


                }
]);
