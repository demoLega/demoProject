function cerrarAyuda() {
    $(".overlayHelp").attr("class");
    $(".overlayHelp").removeClass("overlayHelpActive"), $(".overlayHelpAll").addClass("displayNone");
    var b = validaParametroGet(), c;
    if (b != "null") {
        c = getUrlIframe("glp/", b);
        $(" iframe[src*='" + c + "'] ").contents().find("body .overlayClose , body .overlayHelp2 , .overlayHelpAll").addClass("displayNone"), $(" iframe[src*='" + c + "'] ").contents().find("body .overlayHelp2 ").removeClass("overlayHelpActive"), document.getElementById("helpPanelBtn").click()
    }
}
function activaAyuda() {
    var a = $("#ifrMainContext").get(0), b = a.src, c = validaParametroGet(), d = validaIsInicio(b, c), e = getUrlIframe("glp/", c), f = $(" iframe[src*='" + e + "'] ").contents().height();
    if ("inicio" !== d) {
        if ($(" iframe[src*='" + e + "'] ").contents().find("body .overlayHelpLista-" + d).html()) {
            var g = $(" iframe[src*='" + e + "'] ").contents().find("body .overlayHelpLista-" + d).attr("class"), h = "displayNone";
            g.indexOf(h) !== -1 ? ($(" iframe[src*='" + e + "'] ").contents().find("body").prepend("<div class='overlayHelp2 overlayHelpAll overlayHelpActive'><div class='overlayClose displayNone overlayHelpAll' title='Cerrar' onclick='cerrarAyuda()'></div></div>"), $(" iframe[src*='" + e + "'] ").contents().find("body .overlayHelp2").addClass("overlayHelpActive"), $(" iframe[src*='" + e + "'] ").contents().find("body .overlayHelp2").css({height: f}), $(" iframe[src*='" + e + "'] ").contents().find("body .overlayHelpLista-" + d).removeClass("displayNone"), $(" iframe[src*='" + e + "'] ").contents().find("body .overlayClose").removeClass("displayNone")) : cerrarAyuda()
        }
    } else {
        var i = ".overlayHelpLista-" + d;
        $(i).html() && ($(".overlayHelp").removeClass("displayNone"), $(".overlayHelp").addClass("overlayHelpActive"), $(".overlayHelpLista-" + d).removeClass("displayNone"), $(".overlayClose").removeClass("displayNone"))
    }
}
function getUrlIframe(a, b) {
    if ("null" !== b) {
        var c = $("#ifrMainContext").get(0), d = c.src, e = d.lastIndexOf(a), f = d.lastIndexOf(b), g = d.substring(e + 4, f);
        return g
    }
    return"inicio"
}
function buildAnnotationsHelp(a) {
    var b = "?", c = validaIsInicio(a, b), d = getUrlIframe("glp/", b), e = $(" iframe[src*='" + d + "'] ").contents().height();
    if ("inicio" !== c) {
        if ($(" iframe[src*='" + d + "'] ").contents().find("body .overlayHelpLista-" + c).html()) {
            var f = $(" iframe[src*='" + d + "'] ").contents().find("body .overlayHelpLista-" + c).attr("class"), g = "displayNone";
            f.indexOf(g) !== -1 ? ($(" iframe[src*='" + d + "'] ").contents().find("body").prepend("<div class='overlayHelp2 overlayHelpAll overlayHelpActive'><div class='overlayClose displayNone overlayHelpAll' title='Cerrar' onclick='cerrarAyuda()'></div></div>"), $(" iframe[src*='" + d + "'] ").contents().find("body .overlayHelp2").addClass("overlayHelpActive"), $(" iframe[src*='" + d + "'] ").contents().find("body .overlayHelp2").css({height: e}), $(" iframe[src*='" + d + "'] ").contents().find("body .overlayHelpLista-" + c).removeClass("displayNone"), $(" iframe[src*='" + d + "'] ").contents().find("body .overlayClose").removeClass("displayNone")) : cerrarAyuda()
        }
    } else {
        var h = ".overlayHelpLista-" + c;
        $(h).html() && ($(".overlayHelp").removeClass("displayNone"), $(".overlayHelp").addClass("overlayHelpActive"), $(".overlayHelpLista-" + c).removeClass("displayNone"), $(".overlayClose").removeClass("displayNone"))
    }
}
function getUrlIframe(a, b) {
    if ("null" !== b) {
        var c = $("#ifrMainContext").get(0), d = c.src, e = d.lastIndexOf(a), f = d.lastIndexOf(b), g = d.substring(e + 4, f);
        return g
    }
    return"inicio"
}
function validaIsInicio(a, b) {
    var c = a.lastIndexOf("/"), d = "";
    if ("null" !== b) {
        var e = a.lastIndexOf(b);
        d = a.substring(c + 1, e), d = d.substring(0, d.length - 4)
    } else
        d = "inicio";
    return d
}
function validaParametroGet() {
    var a = $("#ifrMainContext").get(0), b, c = "";
    if (a == undefined) {
        c = "null";
    } else {
        b = a.src;
        c = b.indexOf("?") > -1 ? "?" : (b.indexOf("&") > -1 ? "&" : "null")
    }
    return c;
}
function validatesFirst() {
    /(^|;)\s*visited=/.test(document.cookie) || screen.width > 960 && (document.cookie = "visited=true;", activaAyuda())
}
function readAppendAnnotations() {
    $("#result").load("/enginedata/mx/com/enginedata/utileria/help/helpAnotations.jsf",
            function (a) {
                $("#anotations").append(a)
            })
}
$(document).on("keydown", "body",
        function (a) {
            27 === a.which && cerrarAyuda()
        }),
        $(window).bind("load",
        function () {
            validatesFirst(), readAppendAnnotations()
        });