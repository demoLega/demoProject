   function verificaTecla(boton,tecla,widgetBar){
        if (tecla === 38) {
            //alert(boton+" - "+tecla);
            //event.preventDefault();
            up(widgetBar);
        }
        if (tecla === 40) {
            //event.preventDefault();
            down(widgetBar);
        }
        if (tecla === 13) {
            document.getElementById(boton).click(); 
        }  
   }

 
var down = function(widgetBar) {
    //If no rows selected, select first row
    if(PF(widgetBar).selection.length === 0){
        PF(widgetBar).selectRow(0);
        return;
    }
    //get index of selected row, if no row is selected return 0
    var index = PF(widgetBar).originRowIndex;
    //get amount of rows in the table
    var rows = PF(widgetBar).tbody[0].childElementCount;
    //increase row index
    index++;
    //if new index equals number of rows, set index to first row
    if (index === rows) {
        index = 0;
    }
    //deselect all selected rows
    PF(widgetBar).unselectAllRows();
    //select new row 
    PF(widgetBar).selectRow(index);
    //change originRowIndex value
    PF(widgetBar).originRowIndex = index;
};

var up = function(widgetBar) {
    //get amount of rows in the table
//    alert(widgetBar);
    var rows = PF(widgetBar).tbody[0].childElementCount;
    //get index of selected row, if no row is selected return 0
    var index = PF(widgetBar).originRowIndex;
    //if this is first row, set index to last row
    if (index === 0) {
        index = rows - 1;
    } else {
        index--;
    }
    //unselect all rows
    PF(widgetBar).unselectAllRows();
    //select previous row 
    PF(widgetBar).selectRow(index);
    //set index to current selected row
    PF(widgetBar).originRowIndex = index;
}
