
    var variable = null;    
    var userAccess = null;
 
    function initLoadInput(){
        /*variable = document.getElementById('frmFavoritos:inputIpWS').value;
        userAccess = window.top.$('#uData').data().secUID;
        console.log(variable + "pqtx/notifications?param=" + userAccess);
        initialize();*/
    }
    
//
//-------------   SECCION WEBSOCKET --------------------------------------------
    var socket = null;
    function initialize() {
//        socket = new WebSocket(variable + "pqtx/notifications?param=" + userAccess);
//        //socket = new WebSocket("ws:/http://192.168.10.197:8001/Websocket/pqtx/notifications?param="+userAccess);
//        socket.onmessage = onSocketMessage;
//        this.socket.onclose = function () {
//
//        };
    }
var cont = 0;
function onSocketMessage(event) {    
   if (event.data) {
       var device = JSON.parse(event.data);
       if (device.action === "add") {
           printDeviceElement(device); 
       }
       if (device.action === "remove") {
           document.getElementById(device.id).remove();
       }
       if (device.action === "toggle") {
           var node = document.getElementById(device.id);
           var statusText = node.children[2];
           if (device.status === "On") {
               statusText.innerHTML = "Status: " + device.status + " (<a href=\"#\" OnClick=toggleDevice(" + device.id + ")>Turn off</a>)";
           } else if (device.status === "Off") {
               statusText.innerHTML = "Status: " + device.status + " (<a href=\"#\" OnClick=toggleDevice(" + device.id + ")>Turn on</a>)";
           }
       }
        countDivs();         
   }
}

function addDevice(name, type, description) {
   var DeviceAction = {
       action: "add",
       name: name,
       type: type,
       description: description
   };
   socket.send(JSON.stringify(DeviceAction));
}

function removeDevice(element) {
 
   var id = element;
   var DeviceAction = {
       action: "remove",
       idUser : userAccess,
       name : "",
       mssg : "",
       type : "",
       id: id
   };

   document.getElementById("content").innerHTML = "";
   socket.send(JSON.stringify(DeviceAction));

}

function getDateNow() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; 
    var yyyy = today.getFullYear();
    
    var hh = today.getHours();
    var min = today.getMinutes();
    var seg = today.getSeconds();
 
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
//
    if (hh < 10) {
        hh = '0' + hh;
    }

    if (min < 10) {
        min = '0' + min;
    }
    
    if (seg < 10) {
        seg = '0' + seg;
    }
    today = yyyy+'/'+mm + '/' + dd + '/' + hh+':'+min+':'+seg ; 
    return  today;
}

function showDiff(timeEvent) {
    
    var date2 = new Date(getDateNow());
            //getDateNow();
    var date1 = new Date(timeEvent);
   
    var diff = (date2 - date1) / 1000;
    var diff = Math.abs(Math.floor(diff));

    var days = Math.floor(diff / (24 * 60 * 60));
    var leftSec = diff - days * 24 * 60 * 60;

    var hrs = Math.floor(leftSec / (60 * 60));
    var leftSec = leftSec - hrs * 60 * 60;

    var min = Math.floor(leftSec / (60));
    var leftSec = leftSec - min * 60;
 
    var t = "";
    if (hrs < 1) {
        if (min >= 1) {
            t = min + " minutos";
        } else {
            t = leftSec + " segundos";
        }
    } else {
        if (hrs > 1) {
            if (hrs > 23) {
                t = days + " días";
            }else{
                t = hrs + " hora";
            }
        } else {
            t = hrs + " hora";
        }
    }    
    
    return t;
}

function printDeviceElement(device) {
   var fecha = "";
   if (device.fecha!==""){
     fecha = showDiff(device.fecha);
   }else{
       fecha = "1 segundo";
   }
    
   var descr = "";   
   if (device.mssg.length>50){
       descr = device.mssg.substring(0, 50); 
       descr = descr+"...";
   }else{
       descr = device.mssg;
   }
    
   var content = document.getElementById("content");
   
   var deviceDiv = document.createElement("div");
   deviceDiv.setAttribute("id", device.id);
   deviceDiv.setAttribute("class", "device " + device.type);
   content.appendChild(deviceDiv);

    var deviceType = document.createElement("div");
    deviceType.setAttribute("class", " ui-grid-col-1 typeNoti " + device.type);
    var icono = "";
    if (device.type === "INFO") {
        icono = "<i class='fa fa-info-circle' ></i>";
    } else {
        if (device.type === "ERRROR") {
            icono = "<i class='fa fa-exclamation-circle' ></i>";
        } else {
            if (device.type === "WARNING") {
                icono = "<i class='fa fa-exclamation-triangle' ></i>";
            } else {
                if (device.type === "OTHER") {
                    icono = "<i class='fa fa-adjust' ></i>";
                }
            }
        }
    }
    
   deviceType.innerHTML = icono;
   deviceDiv.appendChild(deviceType);
 
   var deviceName = document.createElement("div");
   deviceName.setAttribute("class", "divText ui-grid-col-11");
   deviceName.innerHTML = "<div> <span class='deviceName'>"+device.name+"</span>" + "<span class='deviceDescr'>"+descr+"</span> </div> <div> <div class='timeDiv'> "+fecha+" </div> </div>";
   deviceDiv.appendChild(deviceName);   
   
//   var timeDiv = document.createElement("span");
//   timeDiv.setAttribute("class", "timeDiv");
//   timeDiv.innerHTML = "";
//   deviceDiv.appendChild(timeDiv);    

   var deviceStatus = document.createElement("span");
   if (device.status === "On") {
       deviceStatus.innerHTML = "<b>Status:</b> " + device.status + " (<a href=\"#\" OnClick=toggleDevice(" + device.id + ")>Turn off</a>)";
   } else if (device.status === "Off") {
       deviceStatus.innerHTML = "<b>Status:</b> " + device.status + " (<a href=\"#\" OnClick=toggleDevice(" + device.id + ")>Turn on</a>)";
       //deviceDiv.setAttribute("class", "device off");
   }
   deviceDiv.appendChild(deviceStatus);

//   var deviceDescription = document.createElement("span");
//   deviceDescription.setAttribute("class", "deviceDescr");
//   deviceDescription.innerHTML = "" + device.mssg;
//   deviceDiv.appendChild(deviceDescription);

   var removeDevice = document.createElement("span");
   removeDevice.setAttribute("class", "removeDevice");
   removeDevice.innerHTML = "<a href=\"#\" OnClick=removeDevice(" + device.id + ");countDivs();><i class='fa fa-check-circle' title='Leer' ></i></a>";
   deviceDiv.appendChild(removeDevice);
   
}

function removeClassDiv(){
    $("#frmFavoritos\\:notificationsButton").removeClass("shake");
}

function countDivs() {
    var contTab = "";
    cont = $("div.device ").length;    
    if (cont===0){
        cont = "";
        contTab = "";
    }else{
        contTab = "("+cont+")";
    }    
    document.getElementById('cantNotf').innerHTML = cont;
    $("#frmFavoritos\\:notificationsButton").addClass("shake");
    setTimeout(removeClassDiv, 1000);
    
    document.title = contTab+" EngineData - Paquetexpress";
}

function showDialogNotifications(event){
    var clase = $('.notificationsBar').attr("class");
    if (clase.indexOf("displayNone") > -1) {
        $('.notificationsBar').removeClass('displayNone');
    }else{
        $('.notificationsBar').addClass('displayNone');
    }
}

function getArrayIdsNotify(size){
    var array = "";
     for (var i = 1 ; i<=size ; i++) {          
         array = array+"'"+$(".device:nth-child("+i+")").attr("id")+"',";
     }
     calculateDates([{name: 'ids', value: array}]);    
}
//initQuery();
function initQuery(){
    var size = $("div.device ").length;
    setTimeout(getArrayIdsNotify(size), 1000);
}

function completeTimeNotify(xhr, status, args){
    var list = eval('(' + args.list + ')');
    for (var i = 0 ; i<list.length ; i++) { 
        $("#"+list[i].idUser+" .timeDiv ").html("   "+list[i].modulo);
    }    
}