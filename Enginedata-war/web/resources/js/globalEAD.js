
function validateSearchPollEAD(){
    actionMoveMobiles();
}
//<![CDATA[
function handleCompleteEAD(xhr, status, args) {
    try {

        var gmap = PF('wvGmapEAD').getMap();
        var newMarkers = eval('(' + args.newMarkers + ')');
        if (typeof (newMarkers) !== "undefined") {
            for (var i in gmap.markers)
            {
                var oldMarker = gmap.markers[i];
                var newMarker = newMarkers[i];
                if (newMarker != null) {
                    oldMarker.setPosition(newMarker.latlng);
                    oldMarker.title = newMarker.title;
                    oldMarker.setMap(gmap);
                    oldMarker.id = newMarker.id;
                    oldMarker.clickable = newMarker.clickable;
                    oldMarker.optimized = false;
                    oldMarker.icon = newMarker.icon;
                    oldMarker.zindex = newMarker.zindex;
//                    var flag = $("#form\\:tabFilters\\:flagLayerIdMobil").val();
                     //if (flag == "true"){
                     
//                        var infowindow = new google.maps.InfoWindow({
//                            content: newMarker.title
//                        });
//
//                        infowindow.open(gmap, oldMarker);    
                        
//                    }
                     
                    
                } else {
                    oldMarker.setMap(null);
                }
            }

        }
        if (typeof (oldMarkersLength) !== "undefined") {

            var oldMarkersLength = gmap.markers.length;
            var newMarkersLength = newMarkers.length;
            // alert(oldMarkersLength+",,"+newMarkersLength);
            for (var i = oldMarkersLength; i < newMarkersLength; i++) {
                var newMarker = newMarkers[i];
                var marker = new google.maps.Marker({
                    position: newMarker.latlng,
                    title: newMarker.title,
                    clickable: newMarker.clickable,
                    icon: newMarker.icon,
                    optimized: false,
                    id: newMarker.id,
                    zindex: newMarker.zindex
                }); 
                gmap.markers[i] = marker;
            }
            PF('wvGmapEAD').addOverlays(gmap.markers);
        }
        //clusterMarkers();
    } catch (err) {
        console.log(err.message);
    }

}
// ]]>
function insertStyle(src,color) {
    //alert(src,"..",color);
    var style = document.createElement('style');
    style.markers = [];
    document.getElementsByTagName('head')[0].appendChild(style);
    var selector = ['.GeolocationGrid img[src="', src, '"]'].join('');
    //if (style.markers.indexOf(selector) < 0) {
        style.markers.push(selector);
        style.sheet.insertRule([selector, '{background-color:', color, ';}'].join(''), 0);
    //}
    //alert(selector,"...",color);
}

function optimizedGMap(){
    var gmap = PF('wvGmapEAD').getMap();
    for (var i in gmap.markers){
//        var oldMarker = gmap.markers[i];
//            oldMarker.setMap(gmap);
//            oldMarker.optimized = false;
//            oldMarker.icon = 'http://maps.gstatic.com/mapfiles/markers2/drag_cross_67_16.png#red';


        var style = document.createElement('style');
        style.markers = [];
        document.getElementsByTagName('head')[0].appendChild(style);
        
        var newMarker = gmap.markers[i];
        var marker = new google.maps.Marker({
            position: newMarker.latlng,
            title: newMarker.title,
            clickable: newMarker.clickable,
            icon: newMarker.icon,
            optimized: false,
            id: newMarker.id
        });      
        
        gmap.markers[i] = marker;
        PF('wvGmapEAD').addOverlays(gmap.markers[i]);          
       
        var selector = ['img[src="', marker.getIcon(), '"]'].join('');
        if (style.markers.indexOf(selector) < 0) {
            style.markers.push(selector);
            var color = "#ccc";
            style.sheet.insertRule([selector, '{background-color: tomato;}'].join(''), 0);
        }
    
    }
    
    
}



function handleSelectedMovilEAD(xhr, status, args) {
    try {
        var gmap = PF('wvGmapEAD').getMap();
        for (var i in gmap.markers) {
            var rowMarker = gmap.markers[i].position;
            var newMarker = eval("args.marker");
            var newPosition = eval("args.position");
            var newMovil = eval("args.idMovil");
            var flagZoom = eval("args.flagZoom");
            
            var newLatitude = eval("args.latitude");
            var newLongitude = eval("args.longitude");
            newMovil = newMovil.replace('"', "");
            newMovil = newMovil.replace('"', "");
            //gmap.markers[i].setIcon("/enginedata/resources/images/icons/gps/truck24.png");
            var oldMovil = gmap.markers[i].title;
            if (newMovil === oldMovil) {
                var oldMarker = gmap.markers[i];
                //oldMarker.setPosition(newPosition);
                oldMarker.setAnimation(google.maps.Animation.DROP);

                newLatitude = newLatitude.replace('"', "");
                newLatitude = newLatitude.replace('"', "");
                newLongitude = newLongitude.replace('"', "");
                newLongitude = newLongitude.replace('"', "");
 
                gmap.setCenter(new google.maps.LatLng(newLatitude, newLongitude));

                flagZoom = flagZoom.replace('"', "");
                flagZoom = flagZoom.replace('"', "");
                                
                if (flagZoom=="Y"){
                    gmap.setZoom(18);
                }
                

            }


        }


    } catch (err) {
        console.log(err.message);
    }

}


//function initMap() {
//  var gmap = PF('wvGmapEAD').getMap();
//  var directionsDisplay = new google.maps.DirectionsRenderer;
//  var directionsService = new google.maps.DirectionsService;
//  var map = new google.maps.Map(gmap, {
//    zoom: 14,
//    center: {lat: 37.77, lng: -122.447}
//  });
//  directionsDisplay.setMap(map);
//
//  calculateAndDisplayRoute(directionsService, directionsDisplay);
//  document.getElementById('mode').addEventListener('change', function() {
//    calculateAndDisplayRoute(directionsService, directionsDisplay);
//  });
//}
//
//function calculateAndDisplayRoute(directionsService, directionsDisplay) {
//  var selectedMode = document.getElementById('mode').value;
//  directionsService.route({   
//    origin: {lat: 25.78525847, lng: -108.9802813},  // Haight.
//    destination: {lat: 25.79168295, lng: -108.99192795},  // Ocean Beach.
//    // Note that Javascript allows us to access the constant
//    // using square brackets and a string value as its
//    // "property."
//    travelMode: google.maps.TravelMode[selectedMode]
//  }, function(response, status) {
//    if (status == google.maps.DirectionsStatus.OK) {
//      directionsDisplay.setDirections(response);
//    } else {
//      window.alert('Directions request failed due to ' + status);
//    }
//  });
//}


function clusterGmap() {
    
//    var map = PF('wvGmapEAD').getMap();
//
//    var markers = [];
//    for (var i = 0; i < 100; i++) {
//        var latLng = new google.maps.LatLng(data.photos[i].latitude,
//                data.photos[i].longitude);
//        var marker = new google.maps.Marker({'position': latLng});
//        markers.push(marker);
//    }
//    var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'images/m'});
//    PF('wvGmapEAD').addOverlays(markerCluster);  
}

function clusterMarkers() {
    var mcOptions = {gridSize: 50, maxZoom: 35 , imagePath: '/enginedata/resources/images/icons/gps/ead/clients/2Dlvy.png' };
    var mc = new MarkerClusterer(PF('wvGmapEAD').map, PF('wvGmapEAD').map.markers, mcOptions);
    PF('wvGmapEAD').map.fitBounds = google.maps.Map.prototype.fitBounds;
}


function calcRoute(end,start) {
    //alert(end+",,,"+start)
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var directionsService = new google.maps.DirectionsService();
    var pMap = PF('wvGmapEAD').getMap();
     directionsDisplay.setMap(pMap);
   
//    var end = "25.78525847, -108.9802813";
//    var start = "25.79168295,-108.99192795";

    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING

    };
 
    directionsService.route(request, function (response, status) {
        //alert(response);
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        } else {
            if (status == 'ZERO_RESULTS') {

                alert('No route could be found between the origin and destination.');

            } else if (status == 'UNKNOWN_ERROR') {

                alert('A directions request could not be processed due to a server error. The request may succeed if you try again.');

            } else if (status == 'REQUEST_DENIED') {

                alert('This webpage is not allowed to use the directions service.');

            } else if (status == 'OVER_QUERY_LIMIT') {

                alert('The webpage has gone over the requests limit in too short a period of time.');

            } else if (status == 'NOT_FOUND') {

                alert('At least one of the origin, destination, or waypoints could not be geocoded.');

            } else if (status == 'INVALID_REQUEST') {

                alert('The DirectionsRequest provided was invalid.');

            } else {

                alert("There was an unknown error in your request. Requeststatus: \n\n" + status);

            }

        }

    });



}

//<![CDATA[
function handleCompleteRoutesEAD(xhr, status, args) {
    try {

        var gmap = PF('wvGmapEAD').getMap();
        var newMarkers = eval('(' + args.newMarkersSecuence + ')');
        if (typeof (newMarkers) !== "undefined") {
            for (var i in gmap.markers)
            {
                var oldMarker = gmap.markers[i];
                var newMarker = newMarkers[i];
                if (newMarker != null) {
                    oldMarker.setPosition(newMarker.latlng);
                    oldMarker.title = newMarker.title;
                    oldMarker.setMap(gmap);
                    oldMarker.id = newMarker.id;
                    oldMarker.clickable = newMarker.clickable;
                    oldMarker.optimized = false;
                    oldMarker.icon = newMarker.icon;
 
                    var infowindow = new google.maps.InfoWindow({
                        content: newMarker.data.rowNum+"°"
                    });

                    infowindow.open(gmap, oldMarker);    
    
                    
                } else {
                    oldMarker.setMap(null);
                }
            }

        }
        //if (typeof (oldMarkersLength) !== "undefined") {

            var oldMarkersLength = gmap.markers.length;
            var newMarkersLength = newMarkers.length;
            // alert(oldMarkersLength+",,"+newMarkersLength);
            for (var i = oldMarkersLength; i < newMarkersLength; i++) {
                var newMarker = newMarkers[i];
                var marker = new google.maps.Marker({
                    position: newMarker.latlng,
                    title: newMarker.title,
                    clickable: newMarker.clickable,
                    icon: newMarker.icon,
                    optimized: false,
                    id: newMarker.id
                });
                //marker.setAnimation(google.maps.Animation.BOUNCE);
             
                gmap.markers[i] = marker;
            }
            PF('wvGmapEAD').addOverlays(gmap.markers);
 

        //}
        //clusterMarkers();
    } catch (err) {
        console.log(err.message);
    }

}
// ]]>



//<![CDATA[
function handleCompleteEADSecuence(xhr, status, args) {
    try {

        var gmap = PF('wvGmapEAD').getMap();
        var newMarkers = eval('(' + args.newMarkers + ')');
        if (typeof (newMarkers) !== "undefined") {
            for (var i in gmap.markers)
            {
                var oldMarker = gmap.markers[i];
                var newMarker = newMarkers[i];
                if (newMarker != null) {
                    oldMarker.setPosition(newMarker.latlng);
                    oldMarker.title = newMarker.title;
                    oldMarker.setMap(gmap);
                    oldMarker.id = newMarker.id;
                    oldMarker.clickable = newMarker.clickable;
                    oldMarker.optimized = false;
                    oldMarker.icon = newMarker.icon;
                     
                } else {
                    oldMarker.setMap(null);
                }
            }

        }
 
            var oldMarkersLength = gmap.markers.length;
            var newMarkersLength = newMarkers.length;
            // alert(oldMarkersLength+",,"+newMarkersLength);
            for (var i = 0; i < newMarkersLength; i++) {
                var newMarker = newMarkers[i];
                var marker = new google.maps.Marker({
                    position: newMarker.latlng,
                    title: newMarker.title,
                    clickable: newMarker.clickable,
                    icon: newMarker.icon,
                    optimized: false,
                    id: newMarker.id,
                    labelContent: "Martin",
                    labelAnchor: new google.maps.Point(22, 0),
                    labelClass: "labels" // the CSS class for the label                    
                });
              
                gmap.markers[i] = marker;
            }
            PF('wvGmapEAD').addOverlays(gmap.markers);
      } catch (err) {
        console.log(err.message);
    }

}
// ]]>

function floatingPanel() {
    //panel
    $('.floatbuttonFilters').removeClass('displayNone'); 
    $('.tabFiltersFloat').addClass('displayBlock');
    
    //boton
    $('.floatbuttonFilters').addClass('displayNone');    
    $('.floatbuttonFilters').removeClass('displayBlock'); 
}
function mouseLeaveFilters(){
    //panel
    $('.tabFiltersFloat').addClass('displayNone');    
    $('.tabFiltersFloat').removeClass('displayBlock');    

    //boton
    $('.floatbuttonFilters').removeClass('displayNone');    
    $('.floatbuttonFilters').addClass('displayBlock');        
}
 
 
 $(document).ready(function () { 
    $(document).on('click', '.panelDetail', function (e) {
        //mouseLeaveFilters();
     });
 });



$(function () {
    
    $('.busquedaGralRapidaInpuut').keyup(function () {
        //remoteGenericSearch();
        var val = $(this).val().toLowerCase();
        
        val = deleteVowels(val);        
        var dom = "#form\\:tabPanelDT\\:dtbClntGeo > .ui-datatable-scrollable-body > table > tbody > tr ";
        $(dom).each(function () {
            var text = $(this).text().toLowerCase();
            text = deleteVowels(text);
            $(this).css('display', 'none');
            if (text.indexOf(val) !== -1) {
                $(this).removeAttr("style");
            }
        });
    });
    
});

function deleteVowels(s){
    if (s.normalize != undefined) {
        s = s.normalize ("NFKD");
    }
    return s.replace (/[\u0300-\u036F]/g, "");
}