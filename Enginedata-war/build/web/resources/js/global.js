//function to reload page in the inframe

var msgRequired = "Los siguientes campos son requeridos:";
var msgNumericData = "El valor introducido '$str$' no es un n�mero";
var userId = "";

function refreshMainContext(urlPage) {
    var iframe = $("#ifrMainContext").get(0);
    iframe.src = urlPage;
}
function refreshIframeInicio(urlPage) {
    $('#typeFilterDiv', parent.document).html($("#typeSeachTxt").html());    
    $('#urlDiv', parent.document).html(urlPage);
    $('#clickiframe', parent.document).click();
}

function refreshIframeGeneric(){
    var urlPage = $("#urlDiv").html();
    refreshMainContext(urlPage);
}
function closeDialogo(dialog) {
    PF(dialog).hide();

}

//function to validate numeric input from the user 
function validateNumber(ctl)
{
    //element=document.getElementById(ctl);
    numero = ctl.value;

    if (!/^([0-9])*$/.test(numero))
    {
        alert(msgNumericData.replace("$str$", numero));
        //alert("El valor introducido '" + numero + "' no es un n�mero");
        ctl.focus();
        ctl.select();
        return false;
    }

    return true;
}
//Solo numeros no llevan punto.
function onlyNumbers(e)
{
    var keynum = window.event ? window.event.keyCode : e.which;
    //Tecla de retroceso (8) para borrar, siempre la permite
    if (keynum == 8)
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

//Solo estilo moneda, es decir entero.decimal
function onlyCurrency(txt, evt)
{
    var keynum = window.event ? window.event.keyCode : evt.which;
    if (keynum == 46) {
        //Check if the text already contains the . character
        if (txt.value.indexOf('.') === -1) {
            return true;
        } else {
            return false;
        }
    } else {
        if (keynum > 31
             && (keynum < 48 || keynum > 57))
            return false;
    }
    return true;
}

//function to validate the dataType for assign to specific validation
function validateValue(ctl, dataType) {
    switch (dataType) {
        case 'dateType':
        {
            return validateDate(ctl);
            break;
        }//date        
        case 'intType':
        {
            return validateNumber(ctl);
            break;
        }//int          
        case 'intTypeOnlyNum':
        {
            return validateNumberOnly(ctl);
            break;
        }//intTypeOnlyNum          
        case 'strType':
        {
            return validateString(ctl);
            break;
        }//str 
        case 'mulType':
        {
            return validateString(ctl);
            break;
        }//mul
        case 'hourType':
        {
            return validateHour(ctl);
            break;
        }//mul
        case 'minuteType':
        {
            return validateMinute(ctl);
            break;
        }//mul 
        case 'timeType':
        {
            return validateTime(ctl);
            break;
        }//mul 
        default:
        {
            break;
        }
    }
}

//function to validate required data from the user
function validateRequired(pFields, labels) {

    arrayLabels = labels;
    arrayIds = pFields;
    var strFinal = "";
    contador = 0;
    var focusId = null;

    for (var i = 0; i < arrayIds.length; i++)
    {
        id = arrayIds[i];
        posLabel = i;
        valueId = document.getElementById(id).value;
        if (valueId == "") {
            if (focusId == null) {
                focusId = i;
            }
            strFinal = strFinal + " - " + labels[posLabel] + '\n';
            contador = contador + 1;
        }
    }

    if (contador >= 1)
    {
        alert(msgRequired + '\n' + strFinal);
        document.getElementById(pFields[focusId]).focus();
        return false;
    }

}

function checkDateRange(start, end) {


    initDate = document.getElementById(start).value;
    dateEnd = document.getElementById(end).value;
    alert("initDate: " + initDate);
    alert("dateEnd: " + dateEnd);
    // Parse the entries
    var startDate = Date.parse(initDate);
    var endDate = Date.parse(dateEnd);
    // Make sure they are valid
    alert("startDate: " + startDate);
    alert("endDate: " + endDate);
    if (endDate == null && startDate == null) {

    }

    // Check the date range, 86400000 is the number of milliseconds in one day
    var difference = (endDate - startDate) / (86400000 * 7);
    if (difference < 0) {
        alert("La fecha de Inicio debe ser antes de la fecha fin.");
        return false;
    }
    if (difference <= 1) {
        alert("La fecha de inicio debe ser diferente a la fecha fin ");
        return false;
    }
    return true;
}


function validateRangeDate(start, end) {


    initDate = document.getElementById(start).value;
    dateEnd = document.getElementById(end).value;

    if (initDate != null && dateEnd != null) {

        // la fecha debe tener el formato siguiente dd/mm/yyyy
        //var fechaInicio = document.getElementById(initDate);
        //var fechaFin = document.getElementById(dateEnd);

        var anio = parseInt(initDate.substring(6, 10));
        var mes = initDate.substring(3, 5);
        var dia = initDate.substring(0, 2);
        var c_anio = parseInt(dateEnd.substring(6, 10));
        var c_mes = dateEnd.substring(3, 5);
        var c_dia = dateEnd.substring(0, 2);
        if (c_anio > anio) {
            return(true);

        } else {

            if (c_anio == anio) {

                if (c_mes > mes) {
                    return(true);
                } else {
                    if (c_mes == mes) {
                        if (c_dia >= dia) {
                            return(true);
                        } else {
                            alert("El rango de fechas ingrasado es incorrecto");
                            return (false);
                        }
                    } else {
                        alert("El rango de fechas ingresado es incorrecto");
                        return(false);

                    }
                }
            } else {
                alert("El rango de fechas ingresado es incorrecto");
                return(false);
            }
        }
    } else {
        return(true);
    }
}

function validaIE() {
    if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i)) {
        return true;
    } else {
        return false;
    }
}

function simulateClickActiveImp() {
    // alert('Simulate');
    //setTimeout('startConnection()',2000);
    // setTimeout('findDefaultPrinter(true)',3000);

//            alert('111');

//            var fso = new ActiveXObject("Scripting.FileSystemObject");
//            var a = fso.CreateTextFile("\glp\resources\etiqueta\printZPL.txt", true);
//            a.WriteLine("This is a test.");
//            a.Close();

    /// write to file
//        var file = new File("\glp\resources\etiqueta\printZPL.txt");
//        var str = "My string of text";
//
//        file.open("w"); // open file with write access
//        file.writeln("First line of text");
//        file.writeln("Second line of text " + str);
//        file.write(str);
//        file.close();        
//        
    // alert('dsd');

    //$('#divImprimir').addClass("displayNone");
}

$(document).ready(function () {

    var tipo = $(".typeUser").text();
    if (tipo != "INTERNO") {
        $("#filtroSucursal *").attr("disabled", true);
        $("#filtroCliente *").attr("disabled", true);
        $("#filtroCliente *").addClass("disabledCliente");
        $("#filtroSucursal *").addClass("disabledCliente");
    }

    $(document).on('click', '.ui-menuitem:not(.ui-menu-parent)', function (e) {
        $('#layout-menu-cover').removeClass("active-menu");
    });

    $(document).on('mouseenter', '#menu-button', function (e) {
        window.top.$("body .overlayMenuClick").css({"visibility": "hidden"});
    });
 
    $(document).on('click', '.bodyContent', function (e) {
        window.top.$('body *').removeClass("active-menu");
        window.top.$("body #menu-button").removeClass('active-menu');
        window.top.$("body .overlayMenuClick").css({"visibility": "hidden"});
    });

//  $(document).on('mouseenter','.bodyContent', function (e) {   
//        window.top.$('body *').removeClass("active-menu");
//        window.top.$("body #menu-button").removeClass('active-menu');      
//        window.top.$("body .overlayMenuClick").css({"visibility": "hidden" });
//   });    

    $(document).on('mouseleave', '.megaMenuDesktop', function (e) {
        $('*').removeClass("active-menu");
        $("#menu-button").removeClass('active-menu');
    });

    $(document).on('mouseenter', '.pointHelp-idioma , .pointHelp-idioma *', function (e) {
        $(".pointHelp-idioma").addClass('active-hover');
    });

    $(document).on('mouseleave', '.pointHelp-idioma ', function (e) {
        setInterval("$('.pointHelp-idioma').removeClass('active-hover')", 3000);
    });


    //Se abre el panel de clientes
    $(document).on('click', '#form\\:pnlClnt.ui-panel-collapsed-h #form\\:pnlClnt_toggler', function (e) {
        //alert("ui-panel-collapsed-h");
        $("#form\\:gmap").addClass('GeoLocation-Map70Height');
        $("#form\\:gmap").removeClass('GeoLocation-Map100');
        //rmCloseDialogSolicitud();
    });

    //Se cierra el panel
    $(document).on('click', '#form\\:pnlClnt:not(.ui-panel-collapsed-h) #form\\:pnlClnt_toggler', function (e) {
//        alert("NOT:::ui-panel-collapsed-h");
        $("#form\\:gmap").removeClass('GeoLocation-Map70Height');
        $("#form\\:gmap").addClass('GeoLocation-Map100');
        //rmCloseDialogSolicitud();
    });


    $(document).on('keyup', '.enterSubmitRad', function (e) {
        var code = e.which;
        if (code === 13) {
            $(".filtroButtonSearch button").click();
        }
    });




//  $(document).on('click','.ui-columntoggler-items .ui-columntoggler-item *', function (e) { 
//     alert('columntoggler');
//  });    
//  $(document).on('click','th.ui-sortable-column', function (e) { 
//     alert('ui-sortable-column');
//  });      


    window.onload = function () {
        $('body').addClass("bodyContent2");
    };

});


function cargarOnClickIframe() {
//     var oFrame = window.top.document.getElementById("ifrMainContext");
//    oFrame.contentWindow.document.onclick = function () {
//        window.top.$('*').removeClass("active-menu");
//        window.top.$("#menu-button").removeClass('active-menu');
//    };
//
//    oFrame.contentWindow.document.body.click(function(){
//        window.top.$('*').removeClass("active-menu");
//    }); 
}

$(window).bind("load", function () {
     var ancho = screen.width;
    if (screen.width <= 767) {
        InsertReflow();
    }

    cargarOnClickIframe();

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', '__gaTracker');

    userId = window.top.$('#uData').data().secTyp + "_" + window.top.$('#uData').data().secUID;
    cookiIdGA = window.top.$('#uData').data().cookieGA;

    console.log('userId = ' + userId);
    __gaTracker('create', cookiIdGA, {'cookieDomain': 'auto'});
    if (userId !== null) {
        __gaTracker('set', 'userId', userId);
        __gaTracker('set', 'dimension1', userId);
        __gaTracker('set', 'dimension2', userId);
    }
    __gaTracker('send', 'pageview');


});


function InsertReflow() {
    // 
    if (document.documentElement.clientWidth <= 767) {
        var Columnas = $(".dt-reflow table > thead > tr > th").size();
        var Renglones = $(".dt-reflow table > tbody > tr > td").size();
        var contRenglones = 1;
        var reng = Renglones / Columnas;
        var concol = 1;
        var aumenta = 1;
        var fila = 0;
        //alert('Ok..'+Columnas + ",,," +Renglones);
        if (Columnas != "") {
            if (Renglones != "0") {

                for (var e = 0; e <= Renglones; e++) {

                    for (var e1 = 0; e1 <= Columnas; e1++) {

                        var header = $(".dt-reflow table > thead > tr:nth-child(1) > th:nth-child(" + e1 + ") > span").html();
                        $(".dt-reflow table > tbody > tr:nth-child(" + e + ") > td:nth-child(" + e1 + ")").prepend("<span class='titleReflow'>" + header + "<span>");

                    }

                }

            }
        }
    }
}

function InsertToList() {

    var Columnas = $(".dt-tolist table > tbody > tr:nth-child(1) > td").size();
    var Renglones = $(".dt-tolist table > tbody > tr > td").size();
    var contRenglones = 1;
    var reng = Renglones / Columnas;
    var concol = 1;
    var aumenta = 1;
    var fila = 0;
    if (Columnas != "") {

        for (var e = 0; e <= Renglones; e++) {

            for (var e1 = 0; e1 <= Columnas; e1++) {

                var header = $(".dt-tolist table > thead > tr:nth-child(1) > th:nth-child(" + e1 + ") > span").html();
                $(".dt-tolist table > tbody > tr:nth-child(" + e + ") > td:nth-child(" + e1 + ")").prepend("<span class='titleReflow'>" + header + "<span>");

            }

        }

    }
}


function checkSubmitEnter() {
    return event.keyCode !== 13;
}

$(document).on('keyup', '.ContDest', function (e) {
    var code = e.which;
    if (code == 13) {
        focusStep2();
    }
});


$(document).on('keyup', 'body', function (e) {
    var code = e.which;
    if (code == 121) {
        focusStep2();
    }
});

document.addEventListener("keyup", function(event) {
  
   if (event.keyCode===121){
     document.getElementById('frmTable:btnSaveRADReq').click();
  }
  if (event.keyCode===112){
     activaAyuda();
  }  
  if (event.keyCode===113){
    PF('dlgVersion').show();
  }    
  if (event.keyCode===115){     
     document.getElementById('imgMenuBtn').click();
     $("#autocompleteFunctions_input").focus();
  }     
//    if (event.keyCode === 13) {
//        $(".row2v2 .ui-grid-col-2 .ui-selectonemenu.ui-state-focus").each(function () {
//            //$("#frmTable:txtCdContact")
//            var clase = $(this).attr("class");
//            var id = $(this).attr("id");
////            if (clase.indexOf("ui-state-focus") > -1) {
////                alert(clase);
////            }
//        });
//    }
  

    
});

//$(".ui-selectonemenu").on('keypress', function(event) {
//    var keycode = event.keyCode ? event.keyCode : event.which;
//    alert(keycode);
//    if(keycode == '13')
//        $("input[id$='editbutton']").get(0).click();    
//});

//CUANDO PRESIONAS ENTER EN ATENCION A
$(document).on('keyup', '#frmTable\\:txtCdContact', function (e) {
    var code = e.which;
    var id = $(this).attr('id');
    console.log(id);
    if (code === 13) { //SOLO CUANDO PRESIONAS LEFT 
        focoById('frmTable:txtDdClientID');
    }  
});

//CUANDO PRESIONAS ENTER EN cmdStateChangeOcurre
$(document).on('keyup', '#frmTable\\:cmdStateChangeOcurre', function (e) {
    var code = e.which;
    var id = $(this).attr('id');
    console.log(id);
    if (code === 13) { //SOLO CUANDO PRESIONAS LEFT 
        $('#frmTable\\:cmbChangeSiteOcurre input').focus();
    }  
});

//CUANDO PRESIONAS ENTER EN cmbChangeSiteOcurre
$(document).on('keyup', '#frmTable\\:cmbChangeSiteOcurre', function (e) {
    var code = e.which;
    var id = $(this).attr('id');
    console.log(id);
    if (code === 13) { //SOLO CUANDO PRESIONAS LEFT 
        $('#frmTable\\:cmbChangeBrncOcurre input').focus();
    }  
});

//CUANDO PRESIONAS ENTER EN cmbChangeBrncOcurre
$(document).on('keyup', '#frmTable\\:cmbChangeBrncOcurre', function (e) {
    var code = e.which;
    var id = $(this).attr('id');
    console.log(id);
    if (code === 13) { //SOLO CUANDO PRESIONAS LEFT 
        $('#frmTable\\:btnChangeBranch').focus();
    }  
});

//CUANDO PRESIONAS LEFT O RIGHT EN LAS FLECHAS
$(document).on('keyup', 'form button , button ', function (e) {
    var code = e.which;
    var valor = $(this).val();
    var typeElement = $(this).attr('role');
    var clase = $(this).attr("class");
    var tipo = $(this).attr('role');

    if (code == 37) { //SOLO CUANDO PRESIONAS LEFT 
        if (tipo == "button") {
            var tabindex = $(this).attr('tabindex');
            var resultado = (parseInt(tabindex) - 1);
            var focodis = null;
            var contFoco = null;
            var status = $("form").find("[tabindex='" + resultado + "']").attr('disabled');
            if (status == "disabled") {
                componenteHTMLFoco(resultado);
            } else {
                $("form").find("[tabindex='" + resultado + "']").select();
                $("[tabindex='" + resultado + "']:not(.ui-autocomplete-dropdown)").focus();
            }
        }

    } else {
        if (code == 39) { //SOLO CUANDO PRESIONAS RIGHT                  
            if (tipo == "button") {
                var tabindex = $(this).attr('tabindex');
                var resultado = (parseInt(tabindex) + 1);
                var focodis = null;
                var contFoco = null;
                var status = $("form").find("[tabindex='" + resultado + "']").attr('disabled');
                if (status == "disabled") {
                    componenteHTMLFoco(resultado);
                } else {
                    $("form").find("[tabindex='" + resultado + "']").select();
                    $("[tabindex='" + resultado + "']:not(.ui-autocomplete-dropdown)").focus();
                }

            }
        }
    }

});
function focoById(id) {
    document.getElementById(id).focus();
    //return false;
}
function focoDesc() {
    //alert("valida");
    var valida = validaIE();
    if (valida) {
        focoById('frmTable:srvcItemCont');
        return false;
    }
}

function isNumberKey(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var value = obj.value;
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains) {
        if (charCode == 46){
            return false;
        }
    }
//     var points = counterPoints(value , ".");
//     if (points===1){
//        return false; 
//     }
    // console.log(points);
    if (charCode == 46)
        return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function counterPoints(str , coincidencia) {
    var regex = new RegExp("[^" + coincidencia + "]", "g");
    str = str.replace(regex, "").length;
    return str;
}

function focoCalendar(){
    focoById();
}
//$(document).on('keydown', '.ui-selectonemenu , .ui-selectonemenu-trigger ', function (e) {
//    var code = e.which;
//     alert("ddd");
//    if (code == 13) {
//        alert("ddd");
//    }
//});
//#frmTable\\:srvcItemDesc_input     :not(#frmTable\\:txtCdMail)
$(document).on('keydown', 'form input:not(#frmTable\\:srvcItemDesc_input) ,form button , button , .ui-selectonemenu , .ui-selectonemenu-trigger ', function (e) {
    var code = e.which;
    var valor = $(this).val();
    var typeElement = $(this).attr('role');
    var clase = $(this).attr("class");
    var tipo = $(this).attr('role');
    if (code == 13) {
        var ide = $(this).attr('id');
        var buscar = "AtencionCntact";
        if (clase.indexOf(buscar) > -1) {
            $("form").find(".ui-wizard-nav-next").focus();
        }

        var tipo = $(this).attr('role');

        if (tipo == "button") {
            var idClic = $(this).attr('id');
            document.getElementById(idClic).click();
        }
        var tabindex = $(this).attr('tabindex');
        //var clase = $(this).attr('class');
        var resultado = (parseInt(tabindex) + 1);
//        var focodis = null;
//        var contFoco = null;
        var status = $("form").find("[tabindex='" + resultado + "']").attr('disabled');

        if (status == "disabled") {
            var role = $("form").find("[tabindex='" + resultado + "']").attr('id');
            if (role != "frmTable:srvcItemLgth") {
                componenteHTMLFoco(resultado);
            } else {
                $("form").find(".btnAdd").focus();
            }
        } else {
//            alert(ide)
//            if (ide !== "frmTable:srvcItemDesc_input"  ){
            if (ide === "frmTable:btnYesOtroArticulo") {
                document.getElementById("frmTable:srvcItemQty").focus();
                return false;
            } else {
                if (ide === "frmTable:txtCdClientReqID" || ide === "frmTable:txtCdClientID" || ide === "frmTable:txtDdClientID") {
                    if (valor !== "") {
                        return false;
                    }
                }
            }

            componenteHTMLFoco(resultado);

        }

    } else {


    }


});


(function () {
    var componenteHTMLFoco1 = function (resultado) {
        var status2 = $("form").find("[tabindex='" + resultado + "']").attr('disabled');
        var val = $("form").find("[tabindex='" + resultado + "']").val();
        if ((status2 != "disabled" && (val == "" || val == "0")) || (status2 == undefined && val == undefined)) {
            $("form").find("[tabindex='" + resultado + "']:not(.ui-autocomplete-dropdown,.ui-selectonemenu-trigger)").select();
            $("form").find("[tabindex='" + resultado + "']:not(.ui-autocomplete-dropdown,.ui-selectonemenu-trigger)").focus();
        } else if ($(this).attr('role') == 'button' && $(this).attr('id') == 'frmTable:btnConfirmar') {
            $(this).focus();
            return false;
        } else {
            componenteHTMLFoco1(parseInt(resultado) + 1);
        }
    };

    componenteHTMLFoco = function (n) {
        return componenteHTMLFoco1(n);
    };
})();

jQuery(document).ready(function ($) {
    $().maxlength();
    $().boldRequired();
})
function boldRequerido() {
    $().boldRequired();

}
;
(function ($) {

    $.fn.maxlength = function () {

        $("input[maxlength]").keypress(function (event) {
            var key = event.which;

            if (key >= 33 || key == 13 || key == 32) {
                var maxLength = $(this).attr("maxlength");
                var length = this.value.length;
                if (length >= maxLength) {
                    event.preventDefault();
                }
            }
        });
    }

    $.fn.boldRequired = function () {

        $("input[aria-required]").each(function () {
            var id = $(this).attr("id");
            $("form").find("[for='" + id + "']").addClass("fontWeightRequired");
        });

    }

})(jQuery);


function recorreFocus(elem) {
    $(".ui-inputfield[tabindex=" + elem + "]").focus();
}

//function focusIdCliente(CLIENTE) {
//    alert(".." + CLIENTE);
//    $("form").find(".ui-inputfield[tabindex='2']").focus();
//}


function nextStep() {

}

function nextStep2() {

}

function focusTop() {
    $("#pr2").click();
}


function focusEmpty(tipo) {

    var grid = "";
    var tabIndex = null;
    var id = "";
    // alert(tipo);
    if (tipo == "ORIGIN") {
        grid = ".gridClntInputFor";
        tabIndex = 11;
        id = "frmTable:txtCdClientID";
    } else {
        if (tipo == "DESTINATION") {
            grid = ".gridClntTabIndexDest";
            tabIndex = 30;
        } else {
            if (tipo == "ORIGINIDCLNT") {
                $(".ui-inputfield[tabindex='8']").focus();
            } else {
                if (tipo == "DESTIDCLNT") {
                    $(".ui-inputfield[tabindex='27']").focus();

                }
            }
        }
    }
    if (grid != "") {
        $("" + grid + " .ui-inputfield").each(function () {
            var thisTab = $(this).attr('tabindex');
            if (typeof (thisTab) !== "undefined") {
                if (thisTab > tabIndex) {
                    if ($(this).val().length == "" && $(this).attr('disabled') != 'disabled') {
                        $(this).focus();
                        return false;
                    }
                }
            }

        });
    }

}
  
$(document).on('focus', '.cantInput', function (e) {
    focusStep2();
});

function focusStep2() {
    $("#pr").click();
} 

function focusStepDown() {
    $("#plnTotalesBtn").click();
}



function moneyFormat(languaje) {
    $('.currency').formatCurrency();
}


$(document).on('click', '#menuMobile .ui-menuitem-link', function (e) {
    $("#layout-menu-cover").addClass('active-menu');

});

function onloadHelp(origin) {

}


$(document).on('keydown', 'form input', function (e) {
    if (($(this).attr("class") == undefined ? false : ($(this).attr("class").indexOf("ui-password") == -1) && ($(this).attr("class") == undefined ? false : $(this).attr("class").indexOf("ui-inputmask") == -1) && ($(this).attr("class") == undefined ? false : $(this).attr("class").indexOf("omitirChangeUppercase") == -1))) {
        if ($(this).css("text-transform").indexOf("uppercase") == -1) {
            $(this).css("text-transform", "uppercase");
        }

    }
});

$(document).on('blur', 'form input', function (e) {
    javascript:{
        if (($(this).attr("class") == undefined ? false : ($(this).attr("class").indexOf("ui-password") == -1) && ($(this).attr("class") == undefined ? false : $(this).attr("class").indexOf("ui-inputmask") == -1) && ($(this).attr("class") == undefined ? false : $(this).attr("class").indexOf("omitirChangeUppercase") == -1))) {
            this.value = this.value.toUpperCase();
        }
    }
});

function next(tab) {

    var tabindex = tab;
    var resultado = (parseInt(tabindex) + 1);

    var status = $("form").find("[tabindex='" + resultado + "']").attr('disabled');
    if (status == "disabled") {
        var res = "n";

        while (res == "n") {
            var status2 = $("form").find("[tabindex='" + resultado + "']").attr('disabled');
            var val = $("form").find("[tabindex='" + resultado + "']").attr('id');
            if (status2 != "disabled") {
                res = "y";
                $("form").find("[tabindex='" + resultado + "']").focus();
            } else {
                resultado++;
            }
        }

    } else {
        $("[tabindex='" + resultado + "']").focus();
    }

}

function validaSession(status) {
    return "222";
}

function idleLogout() {
    var t;
    window.onload = resetTimer;
    window.onmousemove = resetTimer;
    window.onmousedown = resetTimer;
    window.onclick = resetTimer;
    window.onscroll = resetTimer;
    window.onkeypress = resetTimer;

    function resetTimer() {

    }
}


//Ayuda que aparece cuando las cookies estan vacios
function validaCookie() {

    if (document.cookie == "") {
        var width = document.documentElement.clientWidth;
        if (width > 960) {
            helpSystem();
        }
        document.cookie = "";
        $.getJSON("http://jsonip.com?callback=?", function (data) {
            document.cookie = data.ip;
        });
    }

    //ReadCookie(document.cookie);
}

function helpSystem() {
//    alert('sda');
    $('#joyRideTipContent').joyride({
        autoStart: true,
        postStepCallback: function (index, tip) {
            if (index == 2) {
                $(this).joyride('set_li', false, 1);
            }
        },
        modal: true,
        expose: true
    });
}

function ayuda() {
    $('#ayudaBtnHidden').click();
}

$(document).on('blur', '.currency', function (e) {
    moneyFormat('es');
});

//var deployJava=function(){var l={core:["id","class","title","style"],i18n:["lang","dir"],events:["onclick","ondblclick","onmousedown","onmouseup","onmouseover","onmousemove","onmouseout","onkeypress","onkeydown","onkeyup"],applet:["codebase","code","name","archive","object","width","height","alt","align","hspace","vspace"],object:["classid","codebase","codetype","data","type","archive","declare","standby","height","width","usemap","name","tabindex","align","border","hspace","vspace"]};var b=l.object.concat(l.core,l.i18n,l.events);var m=l.applet.concat(l.core);function g(o){if(!d.debug){return}if(console.log){console.log(o)}else{alert(o)}}function k(p,o){if(p==null||p.length==0){return true}var r=p.charAt(p.length-1);if(r!="+"&&r!="*"&&(p.indexOf("_")!=-1&&r!="_")){p=p+"*";r="*"}p=p.substring(0,p.length-1);if(p.length>0){var q=p.charAt(p.length-1);if(q=="."||q=="_"){p=p.substring(0,p.length-1)}}if(r=="*"){return(o.indexOf(p)==0)}else{if(r=="+"){return p<=o}}return false}function e(){var o="//java.com/js/webstart.png";try{return document.location.protocol.indexOf("http")!=-1?o:"http:"+o}catch(p){return"http:"+o}}function n(p){var o="http://java.com/dt-redirect";if(p==null||p.length==0){return o}if(p.charAt(0)=="&"){p=p.substring(1,p.length)}return o+"?"+p}function j(q,p){var o=q.length;for(var r=0;r<o;r++){if(q[r]===p){return true}}return false}function c(o){return j(m,o.toLowerCase())}function i(o){return j(b,o.toLowerCase())}function a(o){if("MSIE"!=deployJava.browserName){return true}if(deployJava.compareVersionToPattern(deployJava.getPlugin().version,["10","0","0"],false,true)){return true}if(o==null){return false}return !k("1.6.0_33+",o)}var d={debug:null,version:"20120801",firefoxJavaVersion:null,myInterval:null,preInstallJREList:null,returnPage:null,brand:null,locale:null,installType:null,EAInstallEnabled:false,EarlyAccessURL:null,oldMimeType:"application/npruntime-scriptable-plugin;DeploymentToolkit",mimeType:"application/java-deployment-toolkit",launchButtonPNG:e(),browserName:null,browserName2:null,getJREs:function(){var t=new Array();if(this.isPluginInstalled()){var r=this.getPlugin();var o=r.jvms;for(var q=0;q<o.getLength();q++){t[q]=o.get(q).version}}else{var p=this.getBrowser();if(p=="MSIE"){if(this.testUsingActiveX("1.7.0")){t[0]="1.7.0"}else{if(this.testUsingActiveX("1.6.0")){t[0]="1.6.0"}else{if(this.testUsingActiveX("1.5.0")){t[0]="1.5.0"}else{if(this.testUsingActiveX("1.4.2")){t[0]="1.4.2"}else{if(this.testForMSVM()){t[0]="1.1"}}}}}}else{if(p=="Netscape Family"){this.getJPIVersionUsingMimeType();if(this.firefoxJavaVersion!=null){t[0]=this.firefoxJavaVersion}else{if(this.testUsingMimeTypes("1.7")){t[0]="1.7.0"}else{if(this.testUsingMimeTypes("1.6")){t[0]="1.6.0"}else{if(this.testUsingMimeTypes("1.5")){t[0]="1.5.0"}else{if(this.testUsingMimeTypes("1.4.2")){t[0]="1.4.2"}else{if(this.browserName2=="Safari"){if(this.testUsingPluginsArray("1.7.0")){t[0]="1.7.0"}else{if(this.testUsingPluginsArray("1.6")){t[0]="1.6.0"}else{if(this.testUsingPluginsArray("1.5")){t[0]="1.5.0"}else{if(this.testUsingPluginsArray("1.4.2")){t[0]="1.4.2"}}}}}}}}}}}}}if(this.debug){for(var q=0;q<t.length;++q){g("[getJREs()] We claim to have detected Java SE "+t[q])}}return t},installJRE:function(r,p){var o=false;if(this.isPluginInstalled()&&this.isAutoInstallEnabled(r)){var q=false;if(this.isCallbackSupported()){q=this.getPlugin().installJRE(r,p)}else{q=this.getPlugin().installJRE(r)}if(q){this.refresh();if(this.returnPage!=null){document.location=this.returnPage}}return q}else{return this.installLatestJRE()}},isAutoInstallEnabled:function(o){if(!this.isPluginInstalled()){return false}if(typeof o=="undefined"){o=null}return a(o)},isCallbackSupported:function(){return this.isPluginInstalled()&&this.compareVersionToPattern(this.getPlugin().version,["10","2","0"],false,true)},installLatestJRE:function(q){if(this.isPluginInstalled()&&this.isAutoInstallEnabled()){var r=false;if(this.isCallbackSupported()){r=this.getPlugin().installLatestJRE(q)}else{r=this.getPlugin().installLatestJRE()}if(r){this.refresh();if(this.returnPage!=null){document.location=this.returnPage}}return r}else{var p=this.getBrowser();var o=navigator.platform.toLowerCase();if((this.EAInstallEnabled=="true")&&(o.indexOf("win")!=-1)&&(this.EarlyAccessURL!=null)){this.preInstallJREList=this.getJREs();if(this.returnPage!=null){this.myInterval=setInterval("deployJava.poll()",3000)}location.href=this.EarlyAccessURL;return false}else{if(p=="MSIE"){return this.IEInstall()}else{if((p=="Netscape Family")&&(o.indexOf("win32")!=-1)){return this.FFInstall()}else{location.href=n(((this.returnPage!=null)?("&returnPage="+this.returnPage):"")+((this.locale!=null)?("&locale="+this.locale):"")+((this.brand!=null)?("&brand="+this.brand):""))}}return false}}},runApplet:function(p,u,r){if(r=="undefined"||r==null){r="1.1"}var t="^(\\d+)(?:\\.(\\d+)(?:\\.(\\d+)(?:_(\\d+))?)?)?$";var o=r.match(t);if(this.returnPage==null){this.returnPage=document.location}if(o!=null){var q=this.getBrowser();if(q!="?"){if(this.versionCheck(r+"+")){this.writeAppletTag(p,u)}else{if(this.installJRE(r+"+")){this.refresh();location.href=document.location;this.writeAppletTag(p,u)}}}else{this.writeAppletTag(p,u)}}else{g("[runApplet()] Invalid minimumVersion argument to runApplet():"+r)}},writeAppletTag:function(r,w){var o="<"+"applet ";var q="";var t="<"+"/"+"applet"+">";var x=true;if(null==w||typeof w!="object"){w=new Object()}for(var p in r){if(!c(p)){w[p]=r[p]}else{o+=(" "+p+'="'+r[p]+'"');if(p=="code"){x=false}}}var v=false;for(var u in w){if(u=="codebase_lookup"){v=true}if(u=="object"||u=="java_object"||u=="java_code"){x=false}q+='<param name="'+u+'" value="'+w[u]+'"/>'}if(!v){q+='<param name="codebase_lookup" value="false"/>'}if(x){o+=(' code="dummy"')}o+=">";document.write(o+"\n"+q+"\n"+t)},versionCheck:function(p){var v=0;var x="^(\\d+)(?:\\.(\\d+)(?:\\.(\\d+)(?:_(\\d+))?)?)?(\\*|\\+)?$";var y=p.match(x);if(y!=null){var r=false;var u=false;var q=new Array();for(var t=1;t<y.length;++t){if((typeof y[t]=="string")&&(y[t]!="")){q[v]=y[t];v++}}if(q[q.length-1]=="+"){u=true;r=false;q.length--}else{if(q[q.length-1]=="*"){u=false;r=true;q.length--}else{if(q.length<4){u=false;r=true}}}var w=this.getJREs();for(var t=0;t<w.length;++t){if(this.compareVersionToPattern(w[t],q,r,u)){return true}}return false}else{var o="Invalid versionPattern passed to versionCheck: "+p;g("[versionCheck()] "+o);alert(o);return false}},isWebStartInstalled:function(r){var q=this.getBrowser();if(q=="?"){return true}if(r=="undefined"||r==null){r="1.4.2"}var p=false;var t="^(\\d+)(?:\\.(\\d+)(?:\\.(\\d+)(?:_(\\d+))?)?)?$";var o=r.match(t);if(o!=null){p=this.versionCheck(r+"+")}else{g("[isWebStartInstaller()] Invalid minimumVersion argument to isWebStartInstalled(): "+r);p=this.versionCheck("1.4.2+")}return p},getJPIVersionUsingMimeType:function(){for(var p=0;p<navigator.mimeTypes.length;++p){var q=navigator.mimeTypes[p].type;var o=q.match(/^application\/x-java-applet;jpi-version=(.*)$/);if(o!=null){this.firefoxJavaVersion=o[1];if("Opera"!=this.browserName2){break}}}},launchWebStartApplication:function(r){var o=navigator.userAgent.toLowerCase();this.getJPIVersionUsingMimeType();if(this.isWebStartInstalled("1.7.0")==false){if((this.installJRE("1.7.0+")==false)||((this.isWebStartInstalled("1.7.0")==false))){return false}}var u=null;if(document.documentURI){u=document.documentURI}if(u==null){u=document.URL}var p=this.getBrowser();var q;if(p=="MSIE"){q="<"+'object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" '+'width="0" height="0">'+"<"+'PARAM name="launchjnlp" value="'+r+'"'+">"+"<"+'PARAM name="docbase" value="'+u+'"'+">"+"<"+"/"+"object"+">"}else{if(p=="Netscape Family"){q="<"+'embed type="application/x-java-applet;jpi-version='+this.firefoxJavaVersion+'" '+'width="0" height="0" '+'launchjnlp="'+r+'"'+'docbase="'+u+'"'+" />"}}if(document.body=="undefined"||document.body==null){document.write(q);document.location=u}else{var t=document.createElement("div");t.id="div1";t.style.position="relative";t.style.left="-10000px";t.style.margin="0px auto";t.className="dynamicDiv";t.innerHTML=q;document.body.appendChild(t)}},createWebStartLaunchButtonEx:function(q,p){if(this.returnPage==null){this.returnPage=q}var o="javascript:deployJava.launchWebStartApplication('"+q+"');";document.write("<"+'a href="'+o+"\" onMouseOver=\"window.status=''; "+'return true;"><'+"img "+'src="'+this.launchButtonPNG+'" '+'border="0" /><'+"/"+"a"+">")},createWebStartLaunchButton:function(q,p){if(this.returnPage==null){this.returnPage=q}var o="javascript:"+"if (!deployJava.isWebStartInstalled(&quot;"+p+"&quot;)) {"+"if (deployJava.installLatestJRE()) {"+"if (deployJava.launch(&quot;"+q+"&quot;)) {}"+"}"+"} else {"+"if (deployJava.launch(&quot;"+q+"&quot;)) {}"+"}";document.write("<"+'a href="'+o+"\" onMouseOver=\"window.status=''; "+'return true;"><'+"img "+'src="'+this.launchButtonPNG+'" '+'border="0" /><'+"/"+"a"+">")},launch:function(o){document.location=o;return true},isPluginInstalled:function(){var o=this.getPlugin();if(o&&o.jvms){return true}else{return false}},isAutoUpdateEnabled:function(){if(this.isPluginInstalled()){return this.getPlugin().isAutoUpdateEnabled()}return false},setAutoUpdateEnabled:function(){if(this.isPluginInstalled()){return this.getPlugin().setAutoUpdateEnabled()}return false},setInstallerType:function(o){this.installType=o;if(this.isPluginInstalled()){return this.getPlugin().setInstallerType(o)}return false},setAdditionalPackages:function(o){if(this.isPluginInstalled()){return this.getPlugin().setAdditionalPackages(o)}return false},setEarlyAccess:function(o){this.EAInstallEnabled=o},isPlugin2:function(){if(this.isPluginInstalled()){if(this.versionCheck("1.6.0_10+")){try{return this.getPlugin().isPlugin2()}catch(o){}}}return false},allowPlugin:function(){this.getBrowser();var o=("Safari"!=this.browserName2&&"Opera"!=this.browserName2);return o},getPlugin:function(){this.refresh();var o=null;if(this.allowPlugin()){o=document.getElementById("deployJavaPlugin")}return o},compareVersionToPattern:function(v,p,r,t){if(v==undefined||p==undefined){return false}var w="^(\\d+)(?:\\.(\\d+)(?:\\.(\\d+)(?:_(\\d+))?)?)?$";var x=v.match(w);if(x!=null){var u=0;var y=new Array();for(var q=1;q<x.length;++q){if((typeof x[q]=="string")&&(x[q]!="")){y[u]=x[q];u++}}var o=Math.min(y.length,p.length);if(t){for(var q=0;q<o;++q){if(y[q]<p[q]){return false}else{if(y[q]>p[q]){return true}}}return true}else{for(var q=0;q<o;++q){if(y[q]!=p[q]){return false}}if(r){return true}else{return(y.length==p.length)}}}else{return false}},getBrowser:function(){if(this.browserName==null){var o=navigator.userAgent.toLowerCase();g("[getBrowser()] navigator.userAgent.toLowerCase() -> "+o);if((o.indexOf("msie")!=-1)&&(o.indexOf("opera")==-1)){this.browserName="MSIE";this.browserName2="MSIE"}else{if(o.indexOf("trident")!=-1||o.indexOf("Trident")!=-1){this.browserName="MSIE";this.browserName2="MSIE"}else{if(o.indexOf("iphone")!=-1){this.browserName="Netscape Family";this.browserName2="iPhone"}else{if((o.indexOf("firefox")!=-1)&&(o.indexOf("opera")==-1)){this.browserName="Netscape Family";this.browserName2="Firefox"}else{if(o.indexOf("chrome")!=-1){this.browserName="Netscape Family";this.browserName2="Chrome"}else{if(o.indexOf("safari")!=-1){this.browserName="Netscape Family";this.browserName2="Safari"}else{if((o.indexOf("mozilla")!=-1)&&(o.indexOf("opera")==-1)){this.browserName="Netscape Family";this.browserName2="Other"}else{if(o.indexOf("opera")!=-1){this.browserName="Netscape Family";this.browserName2="Opera"}else{this.browserName="?";this.browserName2="unknown"}}}}}}}}g("[getBrowser()] Detected browser name:"+this.browserName+", "+this.browserName2)}return this.browserName},testUsingActiveX:function(o){var q="JavaWebStart.isInstalled."+o+".0";if(typeof ActiveXObject=="undefined"||!ActiveXObject){g("[testUsingActiveX()] Browser claims to be IE, but no ActiveXObject object?");return false}try{return(new ActiveXObject(q)!=null)}catch(p){return false}},testForMSVM:function(){var p="{08B0E5C0-4FCB-11CF-AAA5-00401C608500}";if(typeof oClientCaps!="undefined"){var o=oClientCaps.getComponentVersion(p,"ComponentID");if((o=="")||(o=="5,0,5000,0")){return false}else{return true}}else{return false}},testUsingMimeTypes:function(p){if(!navigator.mimeTypes){g("[testUsingMimeTypes()] Browser claims to be Netscape family, but no mimeTypes[] array?");return false}for(var q=0;q<navigator.mimeTypes.length;++q){s=navigator.mimeTypes[q].type;var o=s.match(/^application\/x-java-applet\x3Bversion=(1\.8|1\.7|1\.6|1\.5|1\.4\.2)$/);if(o!=null){if(this.compareVersions(o[1],p)){return true}}}return false},testUsingPluginsArray:function(p){if((!navigator.plugins)||(!navigator.plugins.length)){return false}var o=navigator.platform.toLowerCase();for(var q=0;q<navigator.plugins.length;++q){s=navigator.plugins[q].description;if(s.search(/^Java Switchable Plug-in (Cocoa)/)!=-1){if(this.compareVersions("1.5.0",p)){return true}}else{if(s.search(/^Java/)!=-1){if(o.indexOf("win")!=-1){if(this.compareVersions("1.5.0",p)||this.compareVersions("1.6.0",p)){return true}}}}}if(this.compareVersions("1.5.0",p)){return true}return false},IEInstall:function(){location.href=n(((this.returnPage!=null)?("&returnPage="+this.returnPage):"")+((this.locale!=null)?("&locale="+this.locale):"")+((this.brand!=null)?("&brand="+this.brand):""));return false},done:function(p,o){},FFInstall:function(){location.href=n(((this.returnPage!=null)?("&returnPage="+this.returnPage):"")+((this.locale!=null)?("&locale="+this.locale):"")+((this.brand!=null)?("&brand="+this.brand):"")+((this.installType!=null)?("&type="+this.installType):""));return false},compareVersions:function(r,t){var p=r.split(".");var o=t.split(".");for(var q=0;q<p.length;++q){p[q]=Number(p[q])}for(var q=0;q<o.length;++q){o[q]=Number(o[q])}if(p.length==2){p[2]=0}if(p[0]>o[0]){return true}if(p[0]<o[0]){return false}if(p[1]>o[1]){return true}if(p[1]<o[1]){return false}if(p[2]>o[2]){return true}if(p[2]<o[2]){return false}return true},enableAlerts:function(){this.browserName=null;this.debug=true},poll:function(){this.refresh();var o=this.getJREs();if((this.preInstallJREList.length==0)&&(o.length!=0)){clearInterval(this.myInterval);if(this.returnPage!=null){location.href=this.returnPage}}if((this.preInstallJREList.length!=0)&&(o.length!=0)&&(this.preInstallJREList[0]!=o[0])){clearInterval(this.myInterval);if(this.returnPage!=null){location.href=this.returnPage}}},writePluginTag:function(){var o=this.getBrowser();if(o=="MSIE"){document.write("<"+'object classid="clsid:CAFEEFAC-DEC7-0000-0001-ABCDEFFEDCBA" '+'id="deployJavaPlugin" width="0" height="0">'+"<"+"/"+"object"+">")}else{if(o=="Netscape Family"&&this.allowPlugin()){this.writeEmbedTag()}}},refresh:function(){navigator.plugins.refresh(false);var o=this.getBrowser();if(o=="Netscape Family"&&this.allowPlugin()){var p=document.getElementById("deployJavaPlugin");if(p==null){this.writeEmbedTag()}}},writeEmbedTag:function(){var o=false;if(navigator.mimeTypes!=null){for(var p=0;p<navigator.mimeTypes.length;p++){if(navigator.mimeTypes[p].type==this.mimeType){if(navigator.mimeTypes[p].enabledPlugin){document.write("<"+'embed id="deployJavaPlugin" type="'+this.mimeType+'" hidden="true" />');o=true}}}if(!o){for(var p=0;p<navigator.mimeTypes.length;p++){if(navigator.mimeTypes[p].type==this.oldMimeType){if(navigator.mimeTypes[p].enabledPlugin){document.write("<"+'embed id="deployJavaPlugin" type="'+this.oldMimeType+'" hidden="true" />')}}}}}}};d.writePluginTag();if(d.locale==null){var h=null;if(h==null){try{h=navigator.userLanguage}catch(f){}}if(h==null){try{h=navigator.systemLanguage}catch(f){}}if(h==null){try{h=navigator.language}catch(f){}}if(h!=null){h.replace("-","_");d.locale=h}}return d}();
//
//deployQZ();
//
//function deployQZ() {
//    var attributes = {id: "qz", code: 'qz.PrintApplet.class',
//        archive: 'qz-print.jar', width: 1, height: 1};
//    var parameters = {jnlp_href: 'qz-print_jnlp.jnlp',
//        cache_option: 'plugin', disable_logging: 'false',
//        initial_focus: 'false'};
//    if (deployJava.versionCheck("1.7+") == true) {
//    } else if (deployJava.versionCheck("1.6+") == true) {
//        delete parameters['jnlp_href'];
//    }
//    deployJava.runApplet(attributes, parameters, '1.5');
//}
//
///**
// * Returns whether or not the applet is not ready to print.
// * Displays an alert if not ready.
// */
//function notReady() {
//    // If applet is not loaded, display an error
//    if (!isLoaded()) {
//        return true;
//    }
//    // If a printer hasn't been selected, display a message.
//    else if (!qz.getPrinter()) {
//        alert('Please select a printer first by using the "Detect Printer" button.');
//        return true;
//    }
//    return false;
//}
//
///**
// * Returns is the applet is not loaded properly
// */
//function isLoaded() {
//    if (!qz) {
//        alert('Error:\n\n\tPrint plugin is NOT loaded!');
//        return false;
//    } else {
//        try {
//            if (!qz.isActive()) {
//                alert('Error:\n\n\tPrint plugin is loaded but NOT active!');
//                return false;
//            }
//        } catch (err) {
//            alert('Error:\n\n\tPrint plugin is NOT loaded properly!');
//            return false;
//        }
//    }
//    return true;
//}
//
///***************************************************************************
// * Prototype function for finding the closest match to a printer name.
// * Usage:
// *    qz.findPrinter('zebra');
// *    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
// ***************************************************************************/
//function findPrinter(printerName, printFileName) {
//    // Get printer name from input box
//    /*var p = document.getElementById('printer');
//     if (name) {
//     p.value = name;
//     }*/
//
//    if (isLoaded()) {
//        // Searches for locally installed printer with specified name
//        qz.findPrinter(printerName);
//        //qz.findPrinter(p.value);
//
//        // Automatically gets called when "qz.findPrinter()" is finished.
//        window['qzDoneFinding'] = function () {
//            //var p = document.getElementById('printer');
//            var printer = qz.getPrinter();
//
//            // Alert the printer name to user
//            /*alert(printer !== null ? 'Printer found: "' + printer + 
//             '" after searching for "' + printerName + '"' : 'Printer "' + 
//             printerName + '" not found.');*/
//
//            printFile(printFileName);
//
//            // Remove reference to this function
//            window['qzDoneFinding'] = null;
//        };
//    }
//}
//
///***************************************************************************
// * Prototype function for printing a text or binary file containing raw 
// * print commands.
// * Usage:
// *    qz.appendFile('/path/to/file.txt');
// *    window['qzDoneAppending'] = function() { qz.print(); };
// ***************************************************************************/
//function printFile(file) {
//    //alert('entro a printFile()');
//    //alert('getPath() '+getPath());
//    //alert('printFile '+getPath() + "Guiaprint/" + file);
//    if (notReady()) {
//        return;
//    }
//
//    qz.appendImage(getPath() + "images/Z.GRF");
//    // Append raw or binary text file containing raw print commands
//    qz.appendFile(getPath() + "Guiaprint/BRPETIQ_1456886150950.ETI");
//
//    // Automatically gets called when "qz.appendFile()" is finished.
//    window['qzDoneAppending'] = function () {
//        // Tell the applet to print.
//        qz.print();
//
//        // Remove reference to this function
//        window['qzDoneAppending'] = null;
//    };
//}
//
///***************************************************************************
// * Gets the current url's path, such as http://site.com/example/dist/
// ***************************************************************************/
//function getPath() {
//    var path = window.location.href;
//    return path.substring(0, path.lastIndexOf("/")) + "/";
//}
//
//
//function init() {
//    findPrinter('ptxEtiquetas', "C:/Oracle/Middleware/user_projects/domains/base_domain/");
//}

function clickMenu() {

    if ($('#datos').html() == "" && document.documentElement.clientWidth > 810) {
        $('#datos').html('1');
        $("#menu-button").click();
    }
}
function mouseOut() {
    $('#datos').html("");
}

function bridgeLinkModal(url) {
    changeIDDynamic(url);
    myRemote([{name: 'url', value: url}]);    
}

function changeIDDynamic(id){
    var src = id;
    var positionglp = src.lastIndexOf("/");
    var positionAns = src.lastIndexOf(validaUrlGet(id));
    var buscarglp = src.substring(positionglp+1, positionAns);  
    buscarglp = buscarglp.substring(0,buscarglp.length-4);

    var randomPostID = Math.floor(Math.random() * 110000) + 1;
    buscarglp = buscarglp+randomPostID;
   // alert(buscarglp);
   //$("#frmFavoritos\\:remoteLink_dlg").css({"top": "" });
   $("#frmFavoritos\\:remoteLink_dlg").attr("id",buscarglp);
   $("#"+buscarglp).addClass("multiDialog");
}

function validaUrlGet(src){
 
    var parametroGet = "";
    if (src.indexOf("?") > -1) {
        parametroGet = "?";
    } else{
        if (src.indexOf("&") > -1) {
            parametroGet = "&";
        }else{
            parametroGet = "null";
        }
    }   
    return parametroGet;
}

function simulateHoverHour() {
    $(".horaEntregaBloque").addClass("horaEntregaBloqueActive");
    $(".horaEntregaFixed").addClass("horaEntregaBloqueActive");
    setTimeout('removeSimulateHoverHour()', 5000);
}
function removeSimulateHoverHour() {
    $(".horaEntregaBloque").removeClass("horaEntregaBloqueActive");
    $(".horaEntregaFixed").removeClass("horaEntregaBloqueActive");
}
function headerStiky() {
    var header = $(".dtHeight100 .ui-datatable-tablewrapper > table > thead").clone();
    $(".dtHeight100 .ui-datatable-tablewrapper").append(header);
    alert("Ok");
}
function showFilters() {
    var cbx = $("#form\\:tabFilters\\:cbxTypeFilter_input").val();
    $(".gridFiltrosGenericosHorizontal:not(#" + cbx + ")").addClass("displayNone");
    $(".gridFiltrosGenericosHorizontal#" + cbx + "").removeClass("displayNone");

    //alert("Okk"+cbx);
}
function handlePointClick2(event) {
    if (currentMarker === null) {
        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('lng').value = event.latLng.lng();

        currentMarker = new google.maps.Marker({
            position: new google.maps.LatLng(event.latLng.lat(), event.latLng.lng())
        });

        PF('map').addOverlay(currentMarker);

        PF('dlg').show();
    }
}

var currentMarker = null;
function handlePointClick(lat, lng) {
    var gmap = PF('wvGmap').getMap();
    //alert(gmap.markers.length);
    //gmap.markers.length = 0;

//        var markersLenght = gmap.markers.length;
//        for (var i=0 ; i < markersLenght ; i++){
//            var marker = gmap.markers[i];
//            google.maps.event.addListener(gmap, 'center_changed', function() {
//                marker.setMap(null);
//            });
//        }

    currentMarker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        clickable: true,
        title: 'Movil 23011245',
    });
    PF('wvGmap').addOverlay(currentMarker);
}
function cancel() {
    currentMarker.setMap(null);
    currentMarker = null;

    return false;
}

//var configMap = function() {
//        geocoder = new google.maps.Geocoder();
//        map = PF('wvGmap').getMap();
//        marker = map.markers[0];
//        google.maps.event.addListener(map, 'center_changed', function() {
//            marker.setMap(null);
//            marker.setPosition(map.getCenter());
//        });
//        google.maps.event.addListener(map, 'dragend', function() {
//            marker.setPosition(map.getCenter());
//            var lat = marker.getPosition().G;
//            var lng = marker.getPosition().K;
//            // firing Geocode only when the 'drag' ends
//            PF('wvGmap').reverseGeocode(lat, lng);
//        });
//};    

//function findMe() {
//    navigator.geolocation.getCurrentPosition(
//            function (position) {
//                var map = PF('gmap').getMap(),
//                        latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
//                map.setCenter(latlng);
//                var marker = new google.maps.Marker({
//                    position: latlng
//                });
//                marker.setMap(map);
//            },
//            function (error) {
//                alert(error.message);
//            },
//            {
//                enableHighAccuracy: true
//            });
//}

function markerAddComplete() {
    var title = "Móvil";
    currentMarker.setTitle(title.value);
    title.value = "";

}

// function handleComplete(xhr, status, args){
//   var gmap = PF('wvGmap').getMap();
//   for(var i in gmap.markers)
//   {
//      var newMarker = eval("args.marker"+i);
//      var oldMarker = gmap.markers[i];
//      oldMarker.icon = newMarker.icon;
//      oldMarker.setMap(gmap);
//   }   
//}

//function handleComplete(xhr, status, args){
//    var map = PF('wvGmap');
//    var latlng = new google.maps.LatLng(args.lat, args.lng);
//    var gmap = map.getMap();
//    gmap.setCenter(latlng);
//    gmap.setMap(latlng);
//} 

//function handleComplete(xhr, status, args) {
// 
//    var gmap = PF('vgmap').getMap();
//    for (var i in gmap.markers)
//    {
//        var json = JSON.parse(eval("args.marker" + i));
//
//        var newMarker = json;
//
//        var oldMarker = gmap.markers[i];
//        oldMarker.icon = newMarker.icon;
//        oldMarker.setMap(gmap);
//    }
//}



//<![CDATA[
function handleComplete(xhr, status, args) {
    try {

        var gmap = PF('wvGmap').getMap();
        var newMarkers = eval('(' + args.newMarkers + ')');
        if (typeof (newMarkers) !== "undefined") {
            for (var i in gmap.markers)
            {
                var oldMarker = gmap.markers[i];
                var newMarker = newMarkers[i];
                if (newMarker != null) {
                    oldMarker.setPosition(newMarker.latlng);
                    oldMarker.title = newMarker.title;
                    oldMarker.setMap(gmap);
                    oldMarker.id = newMarker.id;
                    oldMarker.clickable = newMarker.clickable;
                    oldMarker.optimized = false;
                    oldMarker.icon = newMarker.icon;
                    
//                    var flag = $("#form\\:tabFilters\\:flagLayerIdMobil").val();
//                     if (flag == "true"){
//                        var infowindow = new google.maps.InfoWindow({
//                            content: newMarker.title
//                        });
//
//                        infowindow.open(gmap, oldMarker);                      
//                    }
                     
                    
                } else {
                    oldMarker.setMap(null);
                }
            }

        }
        if (typeof (oldMarkersLength) !== "undefined") {

            var oldMarkersLength = gmap.markers.length;
            var newMarkersLength = newMarkers.length;
            // alert(oldMarkersLength+",,"+newMarkersLength);
            for (var i = oldMarkersLength; i < newMarkersLength; i++) {
                var newMarker = newMarkers[i];
                var marker = new google.maps.Marker({
                    position: newMarker.latlng,
                    title: newMarker.title,
                    clickable: newMarker.clickable,
                    icon: newMarker.icon,
                    optimized: false,
                    id: newMarker.id
                });
                marker.setAnimation(google.maps.Animation.BOUNCE);
             
                gmap.markers[i] = marker;
            }
            PF('wvGmap').addOverlays(gmap.markers);
            //    PF('wvGmap')._render();


        }
    } catch (err) {
        console.log(err.message);
    }

}
// ]]>

//<![CDATA[
function handleCompletePoly(xhr, status, args) {
    var gmap = PF('wvGmap').getMap();
    alert(args.poly);
    //var newMarkers = eval('(' + args.poly + ')');
    var triangleCoords = [
        {lat: 25.774, lng: -80.190},
        {lat: 18.466, lng: -66.118},
        {lat: 32.321, lng: -64.757}
    ];
    var bermudaTriangle = new google.maps.Polyline({
        paths: triangleCoords,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#FF0000',
        fillOpacity: 0.35
    });

    PF('wvGmap').addOverlays(bermudaTriangle);

}
// ]]>

function handleSelectedMovil(xhr, status, args) {
    try {
        var gmap = PF('wvGmap').getMap();
        for (var i in gmap.markers) {
            var rowMarker = gmap.markers[i].position;
            var newMarker = eval("args.marker");
            var newPosition = eval("args.position");
            var newMovil = eval("args.idMovil");
            var newLatitude = eval("args.latitude");
            var newLongitude = eval("args.longitude");
            newMovil = newMovil.replace('"', "");
            newMovil = newMovil.replace('"', "");
            //gmap.markers[i].setIcon("/enginedata/resources/images/icons/gps/truck24.png");
            var oldMovil = gmap.markers[i].title;
            if (newMovil === oldMovil) {
                var oldMarker = gmap.markers[i];
                oldMarker.setPosition(newPosition);
                //oldMarker.setIcon("/enginedata/resources/images/icons/gps/truck24.png");
                oldMarker.setAnimation(google.maps.Animation.DROP);

                newLatitude = newLatitude.replace('"', "");
                newLatitude = newLatitude.replace('"', "");
                newLongitude = newLongitude.replace('"', "");
                newLongitude = newLongitude.replace('"', "");
 
                gmap.setCenter(new google.maps.LatLng(newLatitude, newLongitude));
                
//                var infowindow = new google.maps.InfoWindow({
//                  content:newMovil
//                  });
//
//                infowindow.open(gmap,oldMarker);
                
                gmap.setZoom(15);

            }


        }


    } catch (err) {
        console.log(err.message);
    }

}


function isWithinPoly(event) {
    alert(event.latLng);
}

//function isWithinPoly(event){
//    alert(event.latLng);
//    var isWithinPolygon = google.maps.geometry.poly.containsLocation(event.latLng, this);
//    alert(isWithinPolygon);
//    console.log(isWithinPolygon);
//}


function checkInside() {
    var coordinate = new google.maps.LatLng(25.78214, -108.98242);
    var polygon = new google.maps.Polygon([], "#000000", 1, 1, "#336699", 0.3);
    var isWithinPolygon = polygon.containsLatLng(coordinate);
    alert(isWithinPolygon);
}

function initMap() {

    myCoordinates = [
        new google.maps.LatLng(25.782357859663048, -108.9828591534424),
        new google.maps.LatLng(25.782319216417342, -108.98268749206545),
        new google.maps.LatLng(25.782087356678783, -108.98243000000002),
        new google.maps.LatLng(25.782357859663048, -108.98225833862307),
        new google.maps.LatLng(25.782473789324598, -108.98212959259035),
        new google.maps.LatLng(25.78266700517541, -108.98191501586916),
        new google.maps.LatLng(25.782821577629427, -108.98174335449221),
        new google.maps.LatLng(25.783053435932786, -108.98165752380373),
        new google.maps.LatLng(25.78319834714222, -108.9819579312134),
        new google.maps.LatLng(25.78335291890382, -108.98222615211489),
        new google.maps.LatLng(25.783430204709074, -108.98252655952456),
        new google.maps.LatLng(25.783739347426543, -108.98306300132754),
        new google.maps.LatLng(25.784048489338236, -108.9835779854584),
        new google.maps.LatLng(25.784744055693476, -108.98497273414614),
        new google.maps.LatLng(25.785053194986403, -108.98558427780154),
        new google.maps.LatLng(25.785768076515097, -108.98678590744021),
        new google.maps.LatLng(25.78665684213646, -108.98850252120974),
        new google.maps.LatLng(25.78742967639313, -108.98940374343874),
        new google.maps.LatLng(25.78793201595915, -108.99039079635622),
        new google.maps.LatLng(25.78843435339722, -108.99120618789675),
        new google.maps.LatLng(25.789245817071205, -108.99223615615847),
        new google.maps.LatLng(25.789979993705654, -108.99348070114138),
        new google.maps.LatLng(25.790791446802388, -108.99519731491091),
        new google.maps.LatLng(25.79129377212697, -108.99622728317263),
        new google.maps.LatLng(25.79198929598616, -108.9974289128113),
        new google.maps.LatLng(25.792568896085427, -108.9983301350403),
        new google.maps.LatLng(25.792568896085427, -108.99858762710574),
        new google.maps.LatLng(25.79214385628969, -108.99878074615481),
        new google.maps.LatLng(25.791371052757288, -108.99946739166262),
        new google.maps.LatLng(25.790617564463467, -108.99987508743288),
        new google.maps.LatLng(25.78997999370568, -109.00036861389162)];

    polyOptions = {
        path: myCoordinates,
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#0000FF",
        fillOpacity: 0.6};

    var rightShoulderFront = new google.maps.Polygon(polyOptions);

    PF('wvGmap').addOverlays(rightShoulderFront);

}

//
//function addLatLng(event) {
//  var path = poly.getPath();
// 
//  path.push(event.latLng);
//
//   var marker = new google.maps.Marker({
//    position: event.latLng,
//    title: '#' + path.getLength(),
//    map: map
//  });
//}


//<![CDATA[
function movingNewLocation(xhr, status, args) {
    try {

        var gmap = PF('wvGmap').getMap();
        var newMarkers = eval('(' + args.marker + ')');
        if (typeof (newMarkers) !== "undefined") {
      
                var newMovil = newMarkers[0].idMovil;
                var newLatitude = newMarkers[0].latitude;
                var newLongitude = newMarkers[0].longitude;
                
                var oldLatitude = newMarkers[0].oldLatitude;
                var oldLongitude = newMarkers[0].oldLongitude;                
                
                //if (newMarker != null) {
                    var result = [newLatitude,newLongitude];
                    var position = [oldLatitude,oldLongitude];
                    
                    //alert(oldLatitude+","+oldLongitude+".-.."+position[0]+","+ position[1]);
                    
                    var latlng = new google.maps.LatLng(position[0], position[1]);
                    
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: gmap,
                        icon : "/enginedata/resources/images/icons/gps/truck24.png",
                        title: "Your current location!"
                    });                      
                     
                    
                    var numDeltas = 100;
                    var delay = 50; //milliseconds
                    var i = 0;
                    var deltaLat;
                    var deltaLng;
                    transition(result);
                  
                    function transition(result) {
                        i = 0;
                        deltaLat = (result[0] - position[0]) / numDeltas;
                        deltaLng = (result[1] - position[1]) / numDeltas;
                        moveMarker();
                    }

                    function moveMarker() {                   
                        
                        position[0] += deltaLat;
                        position[1] += deltaLng;
                        alert(position[0]+","+position[1]);
                        var latlng = new google.maps.LatLng(position[0], position[1]);
                       
                        marker.setPosition(latlng);
                        if (i !== numDeltas) {
                            i++;
                            setTimeout(moveMarker, delay);
                        }

                    }                    
                     gmap.setZoom(15);
                     gmap.setCenter(newLatitude,newLongitude);

        }
       
        
        PF('wvGmap').addOverlays(gmap.markers);
    } catch (err) {
        console.log(err.message);
    }

}
// ]]>


function toogleTopMap() {
    var clase = $(".toogleTopMap").attr("class");
    var buscar = "toogleTopMapActive";
    if (clase.indexOf(buscar) > -1) {
        $(".toogleTopMap").removeClass("toogleTopMapActive");
        $(".toogleTopMap").removeClass("fa-chevron-up");
        $(".toogleTopMap").addClass("fa-chevron-down");
        
        $('.toogleTopMap').attr('title', 'Minimizar');
        //Minimizar
    } else {
        $(".toogleTopMap").addClass("toogleTopMapActive");
        
        $(".toogleTopMap").addClass("fa-chevron-up");
        $(".toogleTopMap").removeClass("fa-chevron-down");
        
        $('.toogleTopMap').attr('title', 'Maximizar');
        //Maxmimizar
    }
   
    $("#form\\:pnlClnt_header #form\\:pnlClnt_toggler .ui-icon").click();
}

function buildMinimizeBar(name, wv , icon){
    //
    var html = $("#zoneMinimizeBar").html();
       //     alert(html);
    var buscar2 = wv;
    PF(wv).hide();
    if (html.indexOf(buscar2) === -1) {
        var onclick = "PF('"+wv+"').show();$(this).remove()";
        $("#zoneMinimizeBar").append("<div id='"+wv+"' title='Maximizar "+name+"' class='minimizeChild ' onclick="+onclick+"><strong><i class='"+icon+"'></i>"+name+"</strong></div>");            
    }
 
}
function validateVisible(wv,buscarglp,urlItem){
    var html = $("#zoneMinimizeBar").html();
    var buscar2 = wv;
    if (html.indexOf(buscar2) !== -1) {
        $("#"+wv+"").remove();
    }else{
        //var name = 'ifrMainContextModal' + buscarglp;
        refreshIframe(buscarglp,urlItem );        
    }     
    PF(wv).show();       
}
function refreshIframe(name, urlPage) {
    $("#" + name, parent.document).get(0).src = urlPage;
}


function getClientInfo() {
    var clientinfo = "";
    clientinfo = screen.width;
    //$('#frmFavoritos\\:widthBrowser').text(clientinfo);
 } 
 
 
 function focusTest(){
     alert($(this).val());
     $("#menuform\\:menuBar a.ui-state-hover").focus();
     $("#menuform\\:menuBar a.ui-state-hover").click();
     //$("#menuform\\:menuBar a.ui-menu-active").addClass("ui-menu-active");
     //$("#menuform\\:menuBar > ul:nth-child(2) > li:nth-child(1) > ul:nth-child(2) > li:nth-child(1) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)").click();
 }
 
//    $(document).on('mouseover', "#frmTable\\:srvcItemDesc_panel .ui-autocomplete-items li.ui-autocomplete-item", function (e) {
//        var txt = $("#frmTable\\:srvcItemDesc_input").val();
//         if (txt !== ""){
//            $("#frmTable\\:srvcItemDesc_panel .ui-autocomplete-items").addClass("nonePointEvent");
//        }else{
//            $("#frmTable\\:srvcItemDesc_panel .ui-autocomplete-items").removeClass("nonePointEvent");
//        }
//   });
   
   
//    $(document).on('mouseenter', "#frmTable\\:srvcItemDesc_panel .ui-autocomplete-items li.ui-autocomplete-item", function (e) {
//        var txt = $("#frmTable\\:srvcItemDesc_input").val();
//         if (txt !== ""){
//            $("#frmTable\\:srvcItemDesc_panel .ui-autocomplete-items").addClass("nonePointEvent");
//        }else{
//            $("#frmTable\\:srvcItemDesc_panel .ui-autocomplete-items").removeClass("nonePointEvent");
//        }
//   });   
 
    
    $(document).on('focus', ".srvcItemCants .ui-inputtext", function (e) {
        $(this).select();
    });      
    
    function addClassAutocomplete() {
        var clase = $("#frmTable\\:srvcItemDesc_panel .ui-autocomplete-items").attr("class");
        var classs = "nonePointEvent";
        if (typeof (clase) !== "undefined") {             
            if (clase.indexOf(classs) > -1) {
                $("#frmTable\\:srvcItemDesc_panel .ui-autocomplete-items").addClass("nonePointEvent");
            }else{
                $("#frmTable\\:srvcItemDesc_panel .ui-autocomplete-items").removeClass("nonePointEvent");
            }
        }
    }
 
    $(document).on('click', '.ui-columntoggler-items .ui-columntoggler-item', function (e) {
        var index = $(this).index() ;
        var html = $(this).html();
        index = (parseInt(index) + 1);
        if (html.indexOf("ui-icon ui-icon-check")>-1){
            $("#cloneToggle ul").append("<li class='"+index+"'>"+index+"</li>");  
        }else{
            $("#cloneToggle > ul > li." + index + " ").remove();
        }
    
    });
    
    function forElementsToggle(){
        var size = $("#cloneToggle > ul > li").length;
       
        for (var i = 0; i <= size; i++) {
            var num = $("#cloneToggle > ul > li:eq(" + i + ") ").html();
            num = (parseInt(num) - 1);
            var sizeRow = $(".DTToggleSort .ui-datatable-scrollable-body .ui-datatable-data tr").length;
            for (var e = 0; e <= sizeRow; e++) {
                $(".DTToggleSort .ui-datatable-scrollable-body .ui-datatable-data tr:eq(" + e + ") > td:eq(" + num + ")").removeClass("ui-helper-hidden");
            }
         }        
    }
  
 
 function closeDialog(name){
 //   $("#"+name).get(0).src = "";     
 }
function validateSearchPoll() {
    var flag = $('#form\\:tabFilters\\:flagEmpty').html();
    //console.log(flag+".."+flag.indexOf("0"));
    if (flag.indexOf("0") <= 0) {        
        actionMoveMobiles();
        //executeThread();
    }
}
function statusDialog(action){
    if (action === 'show'){
        $(".stusDialogSentinel").removeClass("displayNone");
    }else{
        $(".stusDialogSentinel").addClass("displayNone");
    }
  
}
function progressBar(width){
    alert(width);
    $("#pnlDialog .sr-only").css({"width": width });
    
}



//function load() {
//var dnsComp = Components.classes["@mozilla.org/network/dns-service;1"]; 
//var dnsSvc = dnsComp.getService(Components.interfaces.nsIDNSService);
//var compName = dnsSvc.myHostName;
//     alert(compName);
//}


//var myExtension = {
//  myListener: function(evt) {
//
// var dnsComp = Components.classes["@mozilla.org/network/dns-service;1"]; 
//var dnsSvc = dnsComp.getService(Components.interfaces.nsIDNSService);
//var compName = dnsSvc.myHostName;
//        alert(compName);
//  }
//}
//document.addEventListener("MyExtensionEvent", function(e) { myExtension.myListener(e); }, false, true); //this event will raised from the webpage
// 
// function load2(){ 
//    var evt = document.createEvent("Events");
//    evt.initEvent("MyExtensionEvent", true, false);
//    document.getElementById("compname").dispatchEvent(evt); //this raises the MyExtensionEvent event , which assigns the client computer name to the hidden variable.
//    alert("your computer name is " + document.getElementById("compname").value);
//}

function oblurCalendar(value,update) {
    value = value.replace("__","");
    value = value.replace("_","");
    var valor = value;
    valor = valor.replace(":","");
    
    var size = valor.length;
    var total = 5;
    var resta = total  - size;
    var cadena = "";
    cadena = "";
   
    if (size === 2) {
        resta = resta - 1;
        for (var i = 0; i < resta; i++) {
            cadena = cadena + "0";
        }
        var final = valor + ":" + cadena;
        remoteExitTime([{name: 'hora', value: final},{name: 'update', value: update}]);                     
     }else{
         if (size === 1) {
             resetCalendar(update);
         }
     }     
}
function resetCalendar(update) {
   resetCalendarComponent([{name: 'update', value: update}]);                     
}

function validaTwice(xhr, status, args) {
    var correct = eval("args.correct");
    if (typeof (correct) !== "undefined") {
        correct = correct.replace('"', "");
        correct = correct.replace('"', "");
        if (correct === "Y") {
            document.getElementById('form:solicitudes:btnPrintOnConverter').click();
        }
    }
}
function handleClickNoConfirm(xhr, status, args) {
    var response = eval("args.response");
    response = response.replace('"', "");
    response = response.replace('"', "");
    console.log(response);
    if (response === 'si'){
        document.getElementById('frmTable:btnSaveListRAD').click();
    }
}
 
function validaFocusFromMonitoring(){
    setTimeout(
    function () {
    var ids = ["frmTable\\:txtCdClientReqID","frmTable\\:txtCdClientID","frmTable\\:txtDdClientID","frmTable\\:txtDdContact","frmTable\\:srvcItemQty"];
    for (var i = 0; i < ids.length; i++){
        var input = $("#"+ids[i]);
        if (input.val()===""){
            $(input).focus();
            return false;
        }
    }
    },300);
} 

function handleListToSearch(xhr, status, args) {
    var list = eval("args.list");
    var myObject = JSON.parse(list);
      //  console.log("length.."+ myObject.lenght);
    for (var i = 0; i < myObject.length; i++){
        var obj = myObject[i];
        var keysFiltered = Object.keys(obj).filter(function (item) {
            return !(item == "Ndb_No" || obj[item] == undefined)
        });
        var valuesFiltered = keysFiltered.map(function (item) {
            return obj[item]
        });       
        var allFields = "";
        for (var x = 0; x < valuesFiltered.length; x++){
            allFields = allFields + "," + valuesFiltered[x];
        }
        var offid = myObject[i];
        console.log("KL.."+ offid.guiaNo + "," +allFields);
    }
    
}
        
function openDialogDetailMobile(mobile) {
    remoteShowDialogDetail([{name: 'mobile', value: mobile}]);
}
 
function formatMoney(n, currency) {
    return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}

$(function () {
    $('.searchDatagrid').keyup(function () {
        var val = $(this).val().toLowerCase();
        val = deleteVowels(val);        
        filterInDom(val);
    });
});

function filterInDom(val) {
    val = deleteVowels(val);    
    var dom = "#form\\:tabOptions\\:modelGrid > .ui-datagrid-content > .ui-grid-row > .ui-datagrid-column.ui-grid-col-3";
    $(dom).each(function () {
        var text = $(this).text().toLowerCase();
        text = deleteVowels(text);
        $(this).css('display', 'none');
        if (text.indexOf(val) !== -1) {
            $(this).css('display', 'block');
        }
    });
}
function filterInDomDynamic(val, dom) {
    val = deleteVowels(val);
    dom = dom + '>.ui-datatable-scrollable-body > table > tbody > tr ';
    $(dom).each(function () {
        var text = $(this).text().toLowerCase();
        text = deleteVowels(text);
        $(this).addClass("displayNone");
        if (text.indexOf(val) !== -1) {
            $(this).removeClass("displayNone");
        }
    });
}


function deleteVowels(s){
    if (s.normalize != undefined) {
        s = s.normalize ("NFKD");
    }
    return s.replace (/[\u0300-\u036F]/g, "");
}

 
$(document).on('click', '.dynamicClicTable tbody tr', function (e) { 
    var newMovil = $(this).find('.idmovil').text();
    var newLatitude = $(this).find('.latitude').text(); 
    var newLongitude = $(this).find('.longitude').text();           
    if ((newLatitude!=="") && (newLongitude!=="")){
        setCenterDynamic( newMovil , newLatitude , newLongitude);
    }
    
    
});
  
           
//var windowActive = "";
//$(document).on('mouseenter', '.dynamicClicTable tbody tr', function (e) {
//    PF('statusDialog').show(); 
//    var newMovil = $(this).find('.idmovil').text();
//    //showDynamicDialogMap(newMovil);
//    PF('WV' + newMovil).show();
//
//    var position = $(this).offset();
//    var top = position.top;
//    top = top + 20;
//    var left = position.left;
//    left = left + 100;
//    $(".dialogInfoWindow-" + newMovil).css({top: top + 'px', left: left + 'px'});
//    console.log(".." + top);
//
//    windowActive = newMovil;
//    PF('statusDialog').hide();
//});

function showDynamicDialogMap(newMovil) {
    try {
        PF('WV' + newMovil).show();

        var position = $(this).offset();
        var top = position.top;
        top = top + 20;
        var left = position.left;
        left = left + 100;
        $(".dialogInfoWindow-" + newMovil).css({top: top + 'px', left: left + 'px'});
        console.log(".." + top);

        windowActive = newMovil;
    } catch (err) {
        console.log(err.message);
    }
}

//$(document).on('mouseleave', '.dynamicClicTable tbody tr', function (e) {     
//    PF('WV'+windowActive).hide();  
//    
//});

 
function setCenterDynamic(newMovil , newLatitude , newLongitude) {
    try {
        var gmap = PF('wvGmap').getMap();
        gmap.setCenter(new google.maps.LatLng(newLatitude, newLongitude));
        gmap.setZoom(15);
//        for (var i in gmap.markers) {
//            var oldMovil = gmap.markers[i].title;
//            if (newMovil === oldMovil) {
//                var oldMarker = gmap.markers[i];
//                oldMarker.setAnimation(google.maps.Animation.DROP);
// 
//                gmap.setCenter(new google.maps.LatLng(newLatitude, newLongitude));
//                gmap.setZoom(15);
//                break;                
//            }
//        }
//        console.log("termino");

    } catch (err) {
        console.log(err.message);
    }

}

function checkValue(obj) {
    if (obj.value == '') {
        obj.value = '0';
    }
}

// Se habilita Google analytics segun tenga salida a internet.
function ping() {
//   var img = new Image();
//   img.src = "https://cdn.sstatic.net/Sites/stackoverflow/img/sprites.svg";
//   if (img.height > 0) {
       //alert("Acceso libre!...." + img.height);
       (function (i, s, o, g, r, a, m) {
           i['GoogleAnalyticsObject'] = r;
           i[r] = i[r] || function () {
               (i[r].q = i[r].q || []).push(arguments)
           }, i[r].l = 1 * new Date();
           a = s.createElement(o),
                   m = s.getElementsByTagName(o)[0];
           a.async = 1;
           a.src = g;
           m.parentNode.insertBefore(a, m)
       })(window, document, 'script', '//www.google-analytics.com/analytics.js', '__gaTracker');

       userId = window.top.$('#uData').data().secTyp + "_" + window.top.$('#uData').data().secUID;
       cookiIdGA = window.top.$('#uData').data().cookieGA;

       console.log('userId = ' + userId);
       __gaTracker('create', cookiIdGA, {'cookieDomain': 'auto'});
       if (userId !== null) {
           __gaTracker('set', 'userId', userId);
       }
       __gaTracker('send', 'pageview');
//
//   } else {
//       console.log('Sin acceso a internet');
//   }
}