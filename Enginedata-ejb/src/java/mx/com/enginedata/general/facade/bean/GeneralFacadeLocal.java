/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.general.facade.bean;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants;
import mx.com.enginedata.lgn.dto.MenuProcessDTO;
import mx.com.enginedata.lgn.dto.UserDTO;

/**
 *
 * @author evillegas
 */
@Local
public interface GeneralFacadeLocal {

    public List<MenuProcessDTO> getMenuByUser(UserDTO user) throws Exception;

    ArrayList getAllItemsByFilters(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception;

}
