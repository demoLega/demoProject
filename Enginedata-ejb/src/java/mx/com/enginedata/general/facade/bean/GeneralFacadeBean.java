/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.general.facade.bean;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import mx.com.enginedata.bean.util.GenericQueryDAOLocal;
import mx.com.enginedata.lgn.dao.MenuDAO;
import mx.com.enginedata.lgn.dto.MenuProcessDTO;
import mx.com.enginedata.lgn.dto.UserDTO;

/**
 *
 * @author evillegas
 */
@Stateless(name = "GeneralFacade", mappedName = "PQEXPRESS.GENERAL.CATALOGS.GeneralFacade")
public class GeneralFacadeBean implements GeneralFacade, GeneralFacadeLocal {

//    @EJB
//    private UtileriasDAOLocal utileriasDAOLocal;
//    @EJB
//    private DocumentacionCoreLocal documentacionCore;
//    @EJB
//    private GeneralCoreLocal generalCore;
    @EJB
    private GenericQueryDAOLocal genericQueryDAOLocal;
//
//
//    @EJB
//    private GlobalDAOLocal globalDao;
//
//    @EJB
//    private PpgCoreLocal ppgCoreLocal;
//
//    @EJB
//    private RadClntDailyCoreLocal radClntDailyCoreLocal;

    public List<MenuProcessDTO> getMenuByUser(UserDTO user) throws Exception {
        MenuDAO dao = new MenuDAO();
        return dao.getMenuByUser(user);
    }

    public ArrayList getAllItemsByFilters(String nameQuery, Object[] filters, Object dto, mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants propertiesFile) throws Exception {
        return genericQueryDAOLocal.getAllItemsByFilters(nameQuery, filters, dto, propertiesFile);
    }
    /*
    public List<MenuProcessDTO> getMenuProcessAll() throws Exception {
        return null;
    }

    public void saveMenuProcess(List<MenuProcessDTO> listMenuProcessDTONewOrUpdate, List<MenuProcessDTO> listMenuProcessDTODelete) throws Exception {
        //generalCore.saveMenuProcess(listMenuProcessDTONewOrUpdate, listMenuProcessDTODelete);
    }

    public Long getMaxMenuProcess() throws Exception {
        return null;
    }

    public LgnUserMstrDTO getUserMstrForUserAccess(String userAccess, String typeUser) throws Exception {
        return null;
    }

    public LgnUserMstrDTO getUserMstrForEmail(String userEmail, String umUserType) throws Exception {
        return null;
    }

    public List<LgnModuleDTO> getAllLgnModule() throws Exception {
        return null;
    }

    public LgnModuleDTO saveLgnModule(LgnModuleDTO entityDTO) {
        return null;
    }

    public boolean updateLgnModule(LgnModuleDTO entityDTO) throws Exception {
        return true;
    }

    public boolean deleteLgnModule(LgnModuleDTO entityDTO) throws Exception {
        return true;
    }

    public ArrayList getAllItemsByFilters(SelectEntity select, WhereCondition where, Object dto) throws Exception {
        return genericQueryDAOLocal.getAllItemsByFilters(nameQuery, filters, dto, propertiesFile);
    }

    public ArrayList getAllItemsByFilters(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception {
        return null;
    }

    public ArrayList getAllItemsByFilters(String nameQuery, WhereCondition where, Object dto, TypePropertiesConstants propertiesFile) throws Exception {
        return null;
    }
    
    public ArrayList getAllItemsByFiltersIn(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception {
        return null;
    }    

    public ArrayList getAllItemsByFilters(String nameQuery, WhereCondition where, Object dto, TypePropertiesConstants propertiesFile, TypeDataSourceConstants typeDataSource) throws Exception {
        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="USUARIOS">    
    public List<LgnUserMstrDTO> getUserMstrAll(TypeUser typeUserMstr) throws Exception {
        return null;
    }

    public LgnUserMstrDTO saveLgnUserMstr(LgnUserMstrDTO entityDTO) throws mx.com.enginedata.comun.SmartGeneralException, Exception {
        return null;
    }
    
    public LgnUserMstrDTO saveUserMstrSpecialized(LgnUserMstrDTO entityDTO, String modoUsuario) throws mx.com.enginedata.comun.SmartGeneralException, Exception {
        return null;
    }

    public boolean updateLgnUserMstr(LgnUserMstrDTO entityDTO) throws Exception {
        return true;
    }

    public boolean deleteLgnUserMstr(LgnUserMstrDTO entityDTO) throws Exception {
        return true;
    }

    public LgnUserMstrDTO getUserMstrForUserID(String userID) throws Exception {
        return null;
    }

    public List<Object> authentication(TypeUser typeUser, String userLogin, String userPwd) throws mx.com.enginedata.comun.SmartGeneralException {
        return null;
    }

    public List<SysEmployeeDTO> getListEmployee(WhereCondition whereCondition) throws Exception {
        return null;
    }

    public List<Object> changePasswordLdap(TypeUser typeUser, LgnUserMstrDTO entityDTO, String pwdOld, String pwd, String pwdConfirmation) throws Exception {
        return null;
    }
    
    public LgnUserMstrDTO reactivarUsuario(TypeUser typeUserMstr, String userAccess, String mdfdBy) throws Exception{
        return null;
    }
    //</editor-fold>

    public List<LgnProfilesDTO> getProfilesAll() throws Exception {
        return null;
    }
    
    public List<LgnProfilesDTO> getProfilesActiveAll() throws Exception {
        return null;
    }
    //<editor-fold defaultstate="collapsed" desc="CLIENTES">

    public List<SysClntMstrDTO> getListCliente(WhereCondition where, TypeUser typeUser, int cantidadMaxima, boolean filterEspecialRequest) throws mx.com.enginedata.comun.SmartGeneralException, Exception {
        return null;
    }

    public SysClntMstrDTO createClntComplete(String nameUserAccess, String branchNameAccess, SysClntMstrDTO entityDTO, SysAddrMstrDTO entityDTO2) throws Exception {
        return null;
    }

    public List<SysClntMstrDTO> getListCustomerSimilarity(String nameCustomer, String nameStreet, String zipCode, String codColonia, TypeUser typeUser) throws Exception {
        return null;
    }

    //</editor-fold>

    public List<SysConcMstrDTO> getContactByClnt(String clntID, String addrCode) throws Exception {
        return null;
    }
    
    public SysConcMstrDTO saveContactMstr(SysConcMstrDTO entityDTO) throws Exception {
        return null;
    }
    
    public Date getCurrentDate() throws Exception {
        return null;
    }

    public HistoryDTO getHistory(String guiaOrRastreo) throws Exception {
        return null;
    }

    public DataSecurityDTO getDataSecurityDTO() {
        return null;
    }

    public ConfiguracionEmailDTO getConfiguracionEmail() {
        return null;
    }

    public ConfiguracionServidorDTO getConfiguracionServidor() {
        return null;
    }

    public ConfiguracionServidorDTO getConfiguracionServidorSystemAccess() {
        return null;
    }

    public Object getOnlyOneItemByFilters(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception {
        return null;
    }

    public Object getOnlyOneItemByFilters(String nameQuery, WhereCondition where, Object dto, TypePropertiesConstants propertiesFile) throws Exception {
        return null;
    }

    public LgnUserFncDTO saveFavorites(LgnUserFncDTO entityDTO) {
        return null;
    }

    public String savePickListFavorites(List<LgnUserFncDTO> listDTO, String idEmploye) throws Exception{
        try {
            return null;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public double getCreditByClnt(String clienteId) throws Exception {
        return 0;
    }

    //Validar el horario de recoleccion cuando se genera la solicitud
    public HashMap<String, Object> validateBrncHour(String sucOrigen, Date dateRecolection) throws mx.com.enginedata.comun.SmartGeneralException, Exception {
        try {
            return null;
        } catch (mx.com.enginedata.comun.SmartGeneralException es) {
            throw es;
        } catch (Exception ex) {
            throw ex;
        }
    }

    //Validar el horario de recoleccion cuando se genera la solicitud
    public Date getNextDayNoFree(Date date) throws mx.com.enginedata.comun.SmartGeneralException, Exception {
        try {
            return null;
        } catch (mx.com.enginedata.comun.SmartGeneralException es) {
            throw es;
        } catch (Exception ex) {
            throw ex;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="GUIAS PP">
    public List<PpgBokGuiaDtlDTO> getInformationGuiaPP(String tracNumber, boolean activo) throws Exception {
        return null;
    }

    public PpgZoneMstrDTO distanceEnvelopesGuiaPP(String zoneId) throws Exception {
        return null;
    }

    public List<PpgBokGuiaMstrDTO> getInformationGuiaPPActiveByCustomer(String customerID, String usuarioLogin, String[] guiasUsed) throws Exception {
        return null;
    }
    public List<PpgBokGuiaAsigDTO> getInformationDetailPPBySet(String customerID,String rfnNo, String usuarioLogin) throws Exception {
        return null;
    }
//</editor-fold>

    public RadConvertResultDTO convertRadToGuia(ParameterDTO parameterDTO) throws SmartGeneralException, Exception {
        try {
            return null;
        }catch (SmartGeneralException sme) {
            throw sme;
        }catch (Exception ex) {
            throw ex;
        }
    }

    public List<SysOlMstrDTO> informationOperationLogistic(String[] logisticsOperation) throws Exception {
        return null;
    }

    public InvoiceDTO createInvoiceToGuia(ParameterDTO param) throws Exception {
        try {
            return null;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public String createPayment(ParameterDTO param) throws Exception {
        try {
            return null;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public HistoryDTO getTraceabilityByGuia(String guiaOrRastreo) throws Exception {
        return null;
    }
    
    public HistoryDTO getTraceabilityByGuia_no_Rad(String guiaOrRastreo) throws Exception {
        return null;
    }

    public List<LgnFunctionsDTO> getFunctionsByRolActive(String idRol) throws Exception {
        return null;
    }

    public LgnRolesDTO updateFunctionsRoles(LgnRolesDTO lgnRolesDTO, List<LgnFunctionsDTO> listUpdate) throws Exception {
        return null;
    }

    public LgnFunctionsDTO updateFunctionsAction(LgnFunctionsDTO lgnFunctionsDTO, List<LgnActionDTO> listUpdate) throws Exception {
        return null;
    }

    public boolean deleteFunctions(String idFunction, String MdfdBy) throws Exception {
        return true;
    }

    public List<LgnFunctionsDTO> getFunctionNotInMenuProcess() throws Exception {
        return null;
    }

    public List<LgnRolesDTO> getRolesAll() throws Exception {
        return null;
    }

    public List<LgnRolesDTO> getRolesAllActive() throws Exception {
        return null;
    }

    public LgnRolesDTO getRolesById(String idRoles) throws Exception {
        return null;
    }

    public boolean deleteRole(String idRol, String MdfdBy) throws Exception {
        return true;
    }

    public List<LgnFunctionsDTO> getFunctionsAll(boolean sinRol) throws Exception {
        return null;
    }

    public List<LgnProfileRoleDTO> getProfileRoleAll() throws Exception {
        return null;
    }

    public List<LgnProfileRoleDTO> getProfileRoleByProfile(String idProfile) throws Exception {
        return null;
    }

    public List<LgnFuncTypeDTO> getFuncTypeAll() throws Exception {
        return null;
    }

    public LgnProfilesDTO updateProfileRole(LgnProfilesDTO lgnProfilesDTO, List<LgnProfileRoleDTO> listUpdate) throws Exception {
        return null;
    }

    public boolean deleteProfileRole(String idRol, String MdfdBy) throws Exception, SmartGeneralException {
        return true;
    }

    public LgnProfilesDTO getProfilesById(String idProfile) throws Exception {
        return null;
    }

    public List<LgnActionDTO> getActionAllActive() throws Exception {
        return null;
    }

    public List<LgnFunctionActionDTO> getFunctionActionByFunction(String idFunction) throws Exception {
        return null;
    }

    public List<Object> getActionExceptionByFunction(String idProfile, String idFunction) throws Exception {
        return null;
    }

    public List<LgnActionDTO> getExcepcionActionByProfile(String uri, String idProfile) throws Exception {
        return null;
    }

    public String getCadenaImpresion(String rastreo, boolean multicaja, boolean acuseXT, String rastreo_retorno , String typePrint) throws Exception {
        try {
            return null;
        } catch (Exception ex) {
            throw ex;
        }
    }
        
    public String getCadenaImpresion(String guias, String type ,String numero , String doc , boolean buildQuery) throws Exception{
        return null;
    }    

    public String checkPropertiesFilesQuerys() throws Exception {
        return null;
    }

    public List<SysClntGeoDTO> saveClntGeolocation(List<SysClntGeoDTO> clntGeo, String userAccess) throws Exception {
        return null;
    }

    public Object getOnlyOneValue(String nameQuery, Object[] filters, TypePropertiesConstants propertiesFile) throws Exception {
        try {
            return null;
        } catch (Exception ex) {
            throw ex;
        }
    }
    //ArrayList getAllItemsByFiltersWithConnGeo(String nameQuery, String[] parameters, Object dto, TypePropertiesConstants propertiesFile , Connection con , boolean close) throws Exception;

    public ArrayList getAllItemsByFiltersWithConnGeo(String nameQuery, String[] parameters, Object dto, TypePropertiesConstants propertiesFile, Connection con, boolean close) throws Exception {
        return null;
    }

    public ArrayList getAllItemsByFilters(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile, TypeDataSourceConstants typeDataSource) throws Exception {
        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="CUSTOMERDAILY">
    public List<RadClntDailyDTO> getCustomerDailyByClntType(String clntType, String brancID) throws Exception {
        return null;
    }

    public List<RadClntDailyDTO> saveCustomerDaily(List<RadClntDailyDTO> listCustomer, List<RadClntDailyDTO> listElimina) throws Exception {
        return null;
    }

    public void deleteCustomerDaily(List<RadClntDailyDTO> listCustomer) throws Exception {
        //radClntDailyCoreLocal.deleteCustomerDaily(listCustomer);
    }

    public List<RadClntDailyDTO> getCustomerDailyByClntTypeAndDay(String clntType, String dayWeek, String brancID) throws Exception {
        return null;
    }
    public List<RadClntDailyDTO> getCustomerDailyWithOrderCreated(String docutype, String addrType, Date dateCreated) throws Exception {
        return null;
    }
    
    public void updateCustomerDailyVehicle(List<RadClntDailyDTO> listRadClntDailyDTO, String vehicle, String userID) throws Exception {
        //radClntDailyCoreLocal.updateCustomerDailyVehicle(listRadClntDailyDTO, vehicle, userID);
    }
    
    public List<RadClntDailyDTO> getCustomerDailyWithCollectionTime(String dayWeek, String clntId, String addrCode) throws Exception {
        return null;
    }
    //</editor-fold>

    public ArrayList getAllItemsByFiltersPostgress(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile, Connection con) throws Exception {
        return null;
    }
 
    //Validar el horario limite de recoleccion de la sucursal 
    public HashMap<String, Object> validateBrncHourCollection(String sucOrigen, Date dateRecolection) throws mx.com.enginedata.comun.SmartGeneralException, Exception {
        try {
            return null;
        } catch (mx.com.enginedata.comun.SmartGeneralException es) {
            throw es;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    public SysAddrMstrDTO createClntAddr(RadGuiaAddrDTO radGuiaAddrDTO, String userId) throws Exception{
        return null;
    }
    //<editor-fold defaultstate="collapsed" desc="BOKGUIAREL">

    public List<BokGuiaRelDTO> getInformationGuiaRelation(String tracNumber) throws Exception {
        return null;
    }
	//</editor-fold>
    
    public SysBrncMstrDTO getSucursalDefaultOcurre(String plazaId, String clntId, String addrCode) throws Exception{
        return null;
    }
    
    public String saveInvtPrinter(String user, GlpPcInvtPrinterDTO invent) throws Exception {
        try {
            return null;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public List<CierreConClienteDTO> endCollectionALL(List<CierreConClienteDTO> obj) throws SmartGeneralException, Exception {
        try {
            return null;
        }catch (SmartGeneralException ex) {
            throw ex;
        } 
        catch (Exception ex) {
            throw ex;
        }
    }
    
    public String insertPrintingGuia(String guiaNo , String brncId , String user , String fromNo ) throws Exception {
        try { 
            return null;
        } catch (Exception ex) {
            throw ex;
        }
    }    

    public LgnActionDTO saveLgnAction(LgnActionDTO entityDTO) throws SmartGeneralException, Exception {
        return null;
    }

    public boolean editLgnAction(LgnActionDTO entityDTO) throws Exception {
        return true;
    }

    public boolean removeLgnAction(LgnActionDTO entityDTO) throws Exception {
        return true;
    } 

    public ArrayList getAllItemsByFiltersQuery(String query, Object dto, TypePropertiesConstants propertiesFile, Connection con) throws Exception {
        return null;
    }
    
    public List<SysParmDTO> getTripCurrentByType(String[] parames) throws Exception{
        return null;
    }
   
    public List<LstIncidentDTO> getIncidentAllActive() throws Exception {
        return null;
    }
    
     public LstIncidentDTO saveLstIncident(LstIncidentDTO entityDTO) throws SmartGeneralException, Exception {
        return null;
    }

    public boolean editLstIncident(LstIncidentDTO entityDTO) throws Exception {
        return true;
    }

    public boolean removeLstIncident(LstIncidentDTO entityDTO) throws Exception {
        return true;
    } 
     */
}
