package mx.com.enginedata.general.facade.bean;

import java.sql.Connection;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Remote;
import mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants;
import mx.com.enginedata.lgn.dto.MenuProcessDTO;
import mx.com.enginedata.lgn.dto.UserDTO;

/**
 *
 * @author evillegas
 */
@Remote
public interface GeneralFacade {

    public List<MenuProcessDTO> getMenuByUser(UserDTO user) throws Exception;
    
    ArrayList getAllItemsByFilters(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception;

}
