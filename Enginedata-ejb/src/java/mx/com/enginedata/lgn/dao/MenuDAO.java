/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dao;

/**
 *
 * @author Edgar
 */
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import mx.com.enginedata.lgn.dto.LgnFunctionsDTO;
import mx.com.enginedata.lgn.dto.MenuProcessDTO;
import mx.com.enginedata.lgn.dto.UserDTO;
import mx.com.enginedata.properties.InitPropertiesPrincipal;
import mx.com.paquetexpress.ado.PostGresConnection;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;

/**
 *
 * @author evillegas
 */
public class MenuDAO {

    private String datasourceOracle = null;
    private PostGresConnection oracleConnection = null;

    public MenuDAO() {
        oracleConnection = new PostGresConnection();
        //datasourceOracle = InitPropertiesApplication.getInstance().getProperty("datasourceOracle");
    }

    public List<MenuProcessDTO> getMenuByUser(UserDTO user) throws Exception {
        List<MenuProcessDTO> list = new ArrayList<MenuProcessDTO>();
        PostGresConnection asistenteOracle = null;
        Properties propertiesQuerys = null;
        //Properties propertiesApplication = null;
        Connection conPost = null;
        try {
            asistenteOracle = new PostGresConnection();
            propertiesQuerys = InitPropertiesPrincipal.getInstance();
            //propertiesApplication = InitPropertiesApplication.getInstance();
            //String datasourceOracle = propertiesApplication.getProperty("datasourceOracle");
            conPost = asistenteOracle.getOracleConnection("localhost", "postgres", "postgres", "postgres");

            String sqlProcesos = propertiesQuerys.getProperty("query_struct_Menu_Process");
            
            sqlProcesos = "SELECT DISTINCT M.MP_PROC_ID mpProcId, \n" +
"CASE WHEN FUNC.LF_MULTIDIALOG_WIDTH IS NOT NULL THEN FUNC.LF_MULTIDIALOG_WIDTH ELSE 'N' END lfWidth , \n" +
"CASE WHEN FUNC.LF_MULTIDIALOG_HEIGHT IS NOT NULL THEN FUNC.LF_MULTIDIALOG_HEIGHT ELSE 'N' END lfHeight  ,\n" +
"CASE WHEN FUNC.LF_MULTIDIALOG IS NOT NULL THEN FUNC.LF_MULTIDIALOG ELSE 'N' END lfMultiDialog ,\n" +
"M.MP_LABL mpLabl, M.MP_ORDR mpOrdr,  M.MP_STUS mpStus,  M.MP_PRNT_ID mpPrntId,  \n" +
"M.MP_PROC_TYPE mpProcType, FUNC.ID_FUNCTION idFunction ,FUNC.LF_NAME lfName , FUNC.LF_ICON lfIcon , \n" +
"FUNC.LF_STUS lfStus, FUNC.LF_LEVEL lfLevel, FUNC.LF_URL lfUrl \n" +
"FROM MENU_PROCESS M \n" +
"left JOIN LGN_FUNCTIONS FUNC ON M.ID_FUNCTION=FUNC.ID_FUNCTION \n" +
"Where M.MP_PRNT_ID = '0'\n" +
"UNION ALL\n" +
"SELECT DISTINCT M.MP_PROC_ID mpProcId, \n" +
"CASE WHEN FUNC.LF_MULTIDIALOG_WIDTH IS NOT NULL THEN FUNC.LF_MULTIDIALOG_WIDTH ELSE 'N' END lfWidth , \n" +
"CASE WHEN FUNC.LF_MULTIDIALOG_HEIGHT IS NOT NULL THEN FUNC.LF_MULTIDIALOG_HEIGHT ELSE 'N' END lfHeight  ,\n" +
"CASE WHEN FUNC.LF_MULTIDIALOG IS NOT NULL THEN FUNC.LF_MULTIDIALOG ELSE 'N' END lfMultiDialog ,\n" +
"M.MP_LABL mpLabl, M.MP_ORDR mpOrdr,  M.MP_STUS mpStus,  M.MP_PRNT_ID mpPrntId,  \n" +
"M.MP_PROC_TYPE mpProcType, FUNC.ID_FUNCTION idFunction ,FUNC.LF_NAME lfName , FUNC.LF_ICON lfIcon , \n" +
"FUNC.LF_STUS lfStus, FUNC.LF_LEVEL lfLevel, FUNC.LF_URL lfUrl \n" +
"FROM MENU_PROCESS M \n" +
"left JOIN LGN_FUNCTIONS FUNC ON M.ID_FUNCTION=FUNC.ID_FUNCTION \n" +
"Where M.ID_FUNCTION IN (SELECT FUNCT.ID_FUNCTION \n" +
"                             FROM LGN_FUNCTIONS FUNCT \n" +
"                             INNER JOIN LGN_PROFILE_ROLE PROFIL ON FUNCT.ID_ROL=PROFIL.ID_ROL \n" +
"                             WHERE PROFIL.ID_PROFILE='5'\n" +
"                             AND FUNCT.LF_DELETE_FLAG='N' AND FUNCT.LF_STUS='A' \n" +
"                             AND FUNCT.LF_LEVEL<=PROFIL.PR_LEVL) \n" +
"and M.MP_DELETE_FLAG='N' and M.MP_STUS='A' \n" +
"/*CONNECT BY M.MP_PROC_ID = PRIOR M.MP_PRNT_ID and M.MP_STUS='A' */\n" +
"--ORDER BY M.MP_PRNT_ID , M.MP_ORDR";
            sqlProcesos = sqlProcesos.replace("@1", "'" + user.getPerfilId() + "'");
            list = asistenteOracle.runOracleQueryBeanList(sqlProcesos, new BeanListHandler(MenuProcessDTO.class, new MenuProcessDTOHandler()), "jdbc_Querys");
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            propertiesQuerys = null;
            //propertiesApplication = null;
        }
        return list;
    }

    public class MenuProcessDTOHandler extends BasicRowProcessor {

        @Override
        public List toBeanList(ResultSet rs, Class clazz) {
            try {
                List newlist = new ArrayList();
                while (rs.next()) {
                    newlist.add(toBean(rs, clazz));
                }
                return newlist;
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex1) {
                        ex1.printStackTrace();
                    }
                }
            }
        }

        @Override
        public Object toBean(ResultSet rs, Class type) throws SQLException {
            MenuProcessDTO menuProcessDTO = new MenuProcessDTO();
            menuProcessDTO.setMpProcId(rs.getLong("mpProcId"));
            menuProcessDTO.setMpLabl(rs.getString("mpLabl"));
            menuProcessDTO.setMpOrdr(rs.getInt("mpOrdr"));
            menuProcessDTO.setMpStus(rs.getString("mpStus"));
            menuProcessDTO.setMpPrntId(rs.getLong("mpPrntId"));
            menuProcessDTO.setMpProcType(rs.getString("mpProcType"));
            LgnFunctionsDTO functionsDTO = new LgnFunctionsDTO();
            functionsDTO.setIdFunction(rs.getString("idFunction"));
            functionsDTO.setLfName(rs.getString("lfName"));
            functionsDTO.setLfStus(rs.getString("lfStus"));
            functionsDTO.setLfLevel(rs.getString("lfLevel"));
            functionsDTO.setLfUrl(rs.getString("lfUrl"));
            functionsDTO.setLfIcon(rs.getString("lfIcon"));
            if (rs.getString("lfMultiDialog") != null) {
                functionsDTO.setLfMultiDialog(rs.getString("lfMultiDialog"));
            }
            if (rs.getString("lfHeight") != null) {
                functionsDTO.setLfHeight(rs.getString("lfHeight"));
            }
            if (rs.getString("lfWidth") != null) {
                functionsDTO.setLfWidht(rs.getString("lfWidth"));
            }
            menuProcessDTO.setIdFunctionDTO(functionsDTO);
            return menuProcessDTO;
        }
    }
}
