package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author evillegas
 */
public class GeneralDTO implements Serializable{

    private String orgnBrncId;
    private String destBrncId;
    private Date crtdOn;
    private String crtdBy;
    private Date mdfdOn;
    private String mdfdBy;
    private String status, guiaNo, rastreo;

    public String getOrgnBrncId() {
        return orgnBrncId;
    }

    public void setOrgnBrncId(String orgnBrncId) {
        this.orgnBrncId = orgnBrncId;
    }

    public String getDestBrncId() {
        return destBrncId;
    }

    public void setDestBrncId(String destBrncId) {
        this.destBrncId = destBrncId;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGuiaNo() {
        return guiaNo;
    }

    public void setGuiaNo(String guiaNo) {
        this.guiaNo = guiaNo;
    }

    public String getRastreo() {
        return rastreo;
    }

    public void setRastreo(String rastreo) {
        this.rastreo = rastreo;
    }
    
}
