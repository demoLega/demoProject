/*
 * Copyright(c) enginedata Company, Inc.  All Rights Reserved.
 * This software is the proprietary information of enginedata Company.
 *
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

/**
 *
 * @author evillegas
 */

public class LogGlpDTO implements Serializable {

    public static final long serialVersionUID = 1L;
    public String logId;
    public String logDatetime;
    public String logPriority;
    public String logMessage;
    public String logStacktrace;
    public String logModule;
    public String logPayload;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getLogDatetime() {
        return logDatetime;
    }

    public void setLogDatetime(String logDatetime) {
        this.logDatetime = logDatetime;
    }

    public String getLogPriority() {
        return logPriority;
    }

    public void setLogPriority(String logPriority) {
        this.logPriority = logPriority;
    }

    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }

    public String getLogStacktrace() {
        return logStacktrace;
    }

    public void setLogStacktrace(String logStacktrace) {
        this.logStacktrace = logStacktrace;
    }

    public String getLogModule() {
        return logModule;
    }

    public void setLogModule(String logModule) {
        this.logModule = logModule;
    }

    public String getLogPayload() {
        return logPayload;
    }

    public void setLogPayload(String logPayload) {
        this.logPayload = logPayload;
    }
    
    
    
}
