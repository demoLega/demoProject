/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

/**
 *
 * @author jgil
 */
public class VehiclesDTO {
    private String movilId;
    private String nick;
    private String type;
     private String mark;
     private String color;
    private String plates;

    public VehiclesDTO() {
        nick = "";
        movilId = "";
    }

    
    
    public String getMovilId() {
        return movilId;
    }

    public void setMovilId(String movilId) {
        this.movilId = movilId;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPlates() {
        return plates;
    }

    public void setPlates(String plates) {
        this.plates = plates;
    }

}
