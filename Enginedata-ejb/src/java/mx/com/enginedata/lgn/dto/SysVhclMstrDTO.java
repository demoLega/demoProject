/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

/**
 *
 * @author mdimas
 */
public class SysVhclMstrDTO implements Serializable {
    public String vhclId;
    public String vhclName;

    public String getVhclId() {
        return vhclId;
    }

    public void setVhclId(String vhclId) {
        this.vhclId = vhclId;
    }

    public String getVhclName() {
        return vhclName;
    }

    public void setVhclName(String vhclName) {
        this.vhclName = vhclName;
    }
    
    

}
