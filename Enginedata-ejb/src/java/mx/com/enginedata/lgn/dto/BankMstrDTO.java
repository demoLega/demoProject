/*
 * Copyright(c) enginedata Company, Inc.  All Rights Reserved.
 * This software is the proprietary information of enginedata Company.
 *
 */

package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

public class BankMstrDTO implements Serializable {

    private String bankId;
    private String bankName;
    
    public BankMstrDTO() {
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    
}
