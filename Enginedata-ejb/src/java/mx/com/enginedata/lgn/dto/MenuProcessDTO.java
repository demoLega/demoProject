package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;

public class MenuProcessDTO implements Serializable, Cloneable {

    private Long mpProcId;
    private String mpLabl;
    private Integer mpOrdr;
    private String mpStus;
    private Long mpPrntId;
    private String mpProcType;
    private LgnFunctionsDTO idFunctionDTO;
    private String deleteFlag;
    private Date crtdOn;
    private String crtdBy;
    private Date mdfdOn;
    private String mdfdBy;

    public MenuProcessDTO() {
    }

    public MenuProcessDTO(Long mpProcId) {
        this.mpProcId = mpProcId;
    }

    public Long getMpProcId() {
        return mpProcId;
    }

    public void setMpProcId(Long mpProcId) {
        this.mpProcId = mpProcId;
    }

    public String getMpLabl() {
        return mpLabl;
    }

    public void setMpLabl(String mpLabl) {
        this.mpLabl = mpLabl;
    }

    public Integer getMpOrdr() {
        return mpOrdr;
    }

    public void setMpOrdr(Integer mpOrdr) {
        this.mpOrdr = mpOrdr;
    }

    public String getMpStus() {
        return mpStus;
    }

    public void setMpStus(String mpStus) {
        this.mpStus = mpStus;
    }

    public Long getMpPrntId() {
        return mpPrntId;
    }

    public void setMpPrntId(Long mpPrntId) {
        this.mpPrntId = mpPrntId;
    }

    public String getMpProcType() {
        return mpProcType;
    }

    public void setMpProcType(String mpProcType) {
        this.mpProcType = mpProcType;
    }

    public LgnFunctionsDTO getIdFunctionDTO() {
        return idFunctionDTO;
    }

    public void setIdFunctionDTO(LgnFunctionsDTO idFunctionDTO) {
        this.idFunctionDTO = idFunctionDTO;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    @Override
    public MenuProcessDTO clone() {
        try {
            final MenuProcessDTO result = (MenuProcessDTO) super.clone();
            // copy fields that need to be copied here!
            return result;
        } catch (final CloneNotSupportedException ex) {
            throw new AssertionError();
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mpProcId != null ? mpProcId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuProcessDTO)) {
            return false;
        }
        MenuProcessDTO other = (MenuProcessDTO) object;
        if ((this.mpProcId == null && other.mpProcId != null) || (this.mpProcId != null && !this.mpProcId.equals(other.mpProcId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MenuProcessDTO{" + "mpProcId=" + mpProcId + ", mpLabl=" + mpLabl + ", mpOrdr=" + mpOrdr + ", mpStus=" + mpStus + ", mpPrntId=" + mpPrntId + ", mpProcType=" + mpProcType + ", idFunctionDTO=" + idFunctionDTO + ", deleteFlag=" + deleteFlag + ", mdfdOn=" + mdfdOn + '}';
    }
    
}
