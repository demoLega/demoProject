/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LgnRolesDTO implements Serializable {

    public String idRol;
    public String lrName;
    private String lrStus;
    private Date crtdOn;
    private String crtdBy;
    private Date mdfdOn;
    private String mdfdBy;
    private String lrDeleteFlag;
    private List<LgnProfileRoleDTO> lgnProfileRoleDTOList;
    private List<LgnFunctionsDTO> listFunstionCurrent;

    public LgnRolesDTO() {
    }

    public LgnRolesDTO(String idRol) {
        this.idRol = idRol;
    }

    public String getIdRol() {
        return idRol;
    }

    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }

    public String getLrName() {
        return lrName;
    }

    public void setLrName(String lrName) {
        this.lrName = lrName;
    }

    public String getLrStus() {
        return lrStus;
    }

    public void setLrStus(String lrStus) {
        this.lrStus = lrStus;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public List<LgnProfileRoleDTO> getLgnProfileRoleDTOList() {
        return lgnProfileRoleDTOList;
    }

    public void setLgnProfileRoleDTOList(List<LgnProfileRoleDTO> lgnProfileRoleDTOList) {
        this.lgnProfileRoleDTOList = lgnProfileRoleDTOList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRol != null ? idRol.hashCode() : 0);
        return hash;
    }

    public String getLrDeleteFlag() {
        return lrDeleteFlag;
    }

    public void setLrDeleteFlag(String lrDeleteFlag) {
        this.lrDeleteFlag = lrDeleteFlag;
    }

    public List<LgnFunctionsDTO> getListFunstionCurrent() {
        return listFunstionCurrent;
    }

    public void setListFunstionCurrent(List<LgnFunctionsDTO> listFunstionCurrent) {
        this.listFunstionCurrent = listFunstionCurrent;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LgnRolesDTO)) {
            return false;
        }
        LgnRolesDTO other = (LgnRolesDTO) object;
        if ((this.idRol == null && other.idRol != null) || (this.idRol != null && !this.idRol.equals(other.idRol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LgnRolesDTO{" + "idRol=" + idRol + ", lrName=" + lrName + ", lrStus=" + lrStus + ", crtdOn=" + crtdOn + ", crtdBy=" + crtdBy + ", mdfdOn=" + mdfdOn + ", mdfdBy=" + mdfdBy + ", lgnProfileRoleDTOList=" + lgnProfileRoleDTOList + '}';
    }

}
