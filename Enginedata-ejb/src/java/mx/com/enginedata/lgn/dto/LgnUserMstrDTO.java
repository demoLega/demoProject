package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;
//import mx.com.enginedata.general.catalogo.model.dto.SysClntMstrDTO;

public class LgnUserMstrDTO implements Serializable {

    public String umUserId;
    public String umUserName;
    public String umUserStus;
    public Date crtdOn;
    public String crtdBy;
    public Date mdfdOn;
    public String mdfdBy;
    public String umUserType;
    public LgnProfilesDTO idProfileDTO;
    public String umDeleteFlag;
    public String umUserAccess;
    public String umUserEmail;
//    public SysClntMstrDTO idClntMstrDTO;
    public String idEmployee;
    private String employeeName;
    public String umUserPass;

    public LgnUserMstrDTO() {
    }

    public LgnUserMstrDTO(String umUserId) {
        this.umUserId = umUserId;
    }

    public String getUmUserId() {
        return umUserId;
    }

    public void setUmUserId(String umUserId) {
        this.umUserId = umUserId;
    }

    public String getUmUserName() {
        return umUserName;
    }

    public void setUmUserName(String umUserName) {
        this.umUserName = umUserName;
    }

    public String getUmUserStus() {
        return umUserStus;
    }

    public void setUmUserStus(String umUserStus) {
        this.umUserStus = umUserStus;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public String getUmUserType() {
        return umUserType;
    }

    public void setUmUserType(String umUserType) {
        this.umUserType = umUserType;
    }

    public LgnProfilesDTO getIdProfileDTO() {
        return idProfileDTO;
    }

    public void setIdProfileDTO(LgnProfilesDTO idProfileDTO) {
        this.idProfileDTO = idProfileDTO;
    }

    public String getUmDeleteFlag() {
        return umDeleteFlag;
    }

    public void setUmDeleteFlag(String umDeleteFlag) {
        this.umDeleteFlag = umDeleteFlag;
    }

    public String getUmUserAccess() {
        return umUserAccess;
    }

    public void setUmUserAccess(String umUserAccess) {
        this.umUserAccess = umUserAccess;
    }

    public String getUmUserEmail() {
        return umUserEmail;
    }

    public void setUmUserEmail(String umUserEmail) {
        this.umUserEmail = umUserEmail;
    }

//    public SysClntMstrDTO getIdClntMstrDTO() {
//        return idClntMstrDTO;
//    }
//
//    public void setIdClntMstrDTO(SysClntMstrDTO idClntMstrDTO) {
//        this.idClntMstrDTO = idClntMstrDTO;
//    }

    public String getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(String idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getUmUserPass() {
        return umUserPass;
    }

    public void setUmUserPass(String umUserPass) {
        this.umUserPass = umUserPass;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LgnUserMstrDTO other = (LgnUserMstrDTO) obj;
        if ((this.umUserId == null) ? (other.umUserId != null) : !this.umUserId.equals(other.umUserId)) {
            return false;
        }
        if ((this.umUserName == null) ? (other.umUserName != null) : !this.umUserName.equals(other.umUserName)) {
            return false;
        }
        if ((this.umUserAccess == null) ? (other.umUserAccess != null) : !this.umUserAccess.equals(other.umUserAccess)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "";
    }
    
}
