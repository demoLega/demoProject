/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LgnFuncTypeDTO implements Serializable {

    private String idFuntype;
    private String ftName;
    private String ftStus;
    private Date crtdOn;
    private String crtdBy;
    private Date mdfdOn;
    private String mdfdBy;
    private List<LgnFunctionsDTO> lgnFunctionsDTOList;

    public LgnFuncTypeDTO() {
    }

    public LgnFuncTypeDTO(String idFuntype) {
        this.idFuntype = idFuntype;
    }

    public String getIdFuntype() {
        return idFuntype;
    }

    public void setIdFuntype(String idFuntype) {
        this.idFuntype = idFuntype;
    }

    public String getFtName() {
        return ftName;
    }

    public void setFtName(String ftName) {
        this.ftName = ftName;
    }

    public String getFtStus() {
        return ftStus;
    }

    public void setFtStus(String ftStus) {
        this.ftStus = ftStus;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public List<LgnFunctionsDTO> getLgnFunctionsDTOList() {
        return lgnFunctionsDTOList;
    }

    public void setLgnFunctionsDTOList(List<LgnFunctionsDTO> lgnFunctionsDTOList) {
        this.lgnFunctionsDTOList = lgnFunctionsDTOList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFuntype != null ? idFuntype.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LgnFuncTypeDTO)) {
            return false;
        }
        LgnFuncTypeDTO other = (LgnFuncTypeDTO) object;
        if ((this.idFuntype == null && other.idFuntype != null) || (this.idFuntype != null && !this.idFuntype.equals(other.idFuntype))) {
            return false;
        }
        return true;
    }
}
