/*
 * Copyright(c) enginedata Company, Inc.  All Rights Reserved.
 * This software is the proprietary information of enginedata Company.
 *
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.List;

public class SysClntGeoDTO implements Serializable {
   
   public String clntId;
   public String drnr;
   public String coloZipCode;   
   public String city;  
   public String colony;  
   public String address;
   public String delMun;
   public String statee;
   public String country;
   public String brncId;   
   public String clntName;
   public String updateFlag;
   public String addrId;
   public String latitude;
   public String longitude;
   public String truck;
   public String viaje;
   public String guiaNo;
   public String dlvyDate;   
   public String color;   
   public String status;
   public String confirmed;
   public String colorIcon; 
   public String flagMultiClnt; 
   public String rowNum; 
   public String eadFlag; 
   public int multiClntCant; 
   public List<SysClntGeoDTO> listMultiClnt;
   public String type; 
   public String formNo;  
   public String prepBrncId;  
   public List<byte[]> listImgReturns;
   public byte[] imgReturns;
   public String reasonReturn;  
   public String statusFilter;  
   public String truckCoordinates;
   public String dlvyPerson;
   public String typeGrouper;
   public String flagGrouper;
   
   public String clntOrgn;
   public String clntNameOrgn;
   public String orgnBrncId;
   public String numbPack;
   public String guiaAmnt;
   public String totlVlum;
   public String totlWght;
   
   public String timeBetweenDlvy;
   public String pymtMode ,  pymtType;
   public String latitudeClntDlvy;
   public String longitudeClntDlvy;   
   public String carita;  
   
   public void SysClntGeoDTO(){
       
   }
 
    public String getClntId() {
        return clntId;
    }

    public void setClntId(String clntId) {
        this.clntId = clntId;
    }

    public String getColoZipCode() {
        return coloZipCode;
    }

    public void setColoZipCode(String coloZipCode) {
        this.coloZipCode = coloZipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDelMun() {
        return delMun;
    }

    public void setDelMun(String delMun) {
        this.delMun = delMun;
    }

    public String getBrncId() {
        return brncId;
    }

    public void setBrncId(String brncId) {
        this.brncId = brncId;
    }

    public String getAddrId() {
        return addrId;
    }

    public void setAddrId(String addrId) {
        this.addrId = addrId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
 
    public String getUpdateFlag() {
        return updateFlag;
    }

    public void setUpdateFlag(String updateFlag) {
        this.updateFlag = updateFlag;
    }

    public String getDrnr() {
        return drnr;
    }

    public void setDrnr(String drnr) {
        this.drnr = drnr;
    }

    public String getClntName() {
        return clntName;
    }

    public void setClntName(String clntName) {
        this.clntName = clntName;
    }
 
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatee() {
        return statee;
    }

    public void setStatee(String statee) {
        this.statee = statee;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }

    public String getViaje() {
        return viaje;
    }

    public void setViaje(String viaje) {
        this.viaje = viaje;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(String confirmed) {
        this.confirmed = confirmed;
    }

    public String getColorIcon() {
        return colorIcon;
    }

    public void setColorIcon(String colorIcon) {
        this.colorIcon = colorIcon;
    }

    public String getGuiaNo() {
        return guiaNo;
    }

    public void setGuiaNo(String guiaNo) {
        this.guiaNo = guiaNo;
    }

    public String getDlvyDate() {
        return dlvyDate;
    }

    public void setDlvyDate(String dlvyDate) {
        this.dlvyDate = dlvyDate;
    }

    public String getTruckCoordinates() {
        return truckCoordinates;
    }

    public void setTruckCoordinates(String truckCoordinates) {
        this.truckCoordinates = truckCoordinates;
    }

    public List<SysClntGeoDTO> getListMultiClnt() {
        return listMultiClnt;
    }

    public void setListMultiClnt(List<SysClntGeoDTO> listMultiClnt) {
        this.listMultiClnt = listMultiClnt;
    }

    public String getFlagMultiClnt() {
        return flagMultiClnt;
    }

    public void setFlagMultiClnt(String flagMultiClnt) {
        this.flagMultiClnt = flagMultiClnt;
    }

    public int getMultiClntCant() {
        return multiClntCant;
    }

    public void setMultiClntCant(int multiClntCant) {
        this.multiClntCant = multiClntCant;
    }

    public String getRowNum() {
        return rowNum;
    }

    public void setRowNum(String rowNum) {
        this.rowNum = rowNum;
    }

    public String getEadFlag() {
        return eadFlag;
    }

    public void setEadFlag(String eadFlag) {
        this.eadFlag = eadFlag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFormNo() {
        return formNo;
    }

    public void setFormNo(String formNo) {
        this.formNo = formNo;
    }

    public String getPrepBrncId() {
        return prepBrncId;
    }

    public void setPrepBrncId(String prepBrncId) {
        this.prepBrncId = prepBrncId;
    }

    public List<byte[]> getListImgReturns() {
        return listImgReturns;
    }

    public void setListImgReturns(List<byte[]> listImgReturns) {
        this.listImgReturns = listImgReturns;
    }

    public byte[] getImgReturns() {
        return imgReturns;
    }

    public void setImgReturns(byte[] imgReturns) {
        this.imgReturns = imgReturns;
    }

    public String getReasonReturn() {
        return reasonReturn;
    }

    public void setReasonReturn(String reasonReturn) {
        this.reasonReturn = reasonReturn;
    }

    public String getStatusFilter() {
        return statusFilter;
    }

    public void setStatusFilter(String statusFilter) {
        this.statusFilter = statusFilter;
    }

    public String getDlvyPerson() {
        return dlvyPerson;
    }

    public void setDlvyPerson(String dlvyPerson) {
        this.dlvyPerson = dlvyPerson;
    }

    public String getTypeGrouper() {
        return typeGrouper;
    }

    public void setTypeGrouper(String typeGrouper) {
        this.typeGrouper = typeGrouper;
    }

    public String getFlagGrouper() {
        return flagGrouper;
    }

    public void setFlagGrouper(String flagGrouper) {
        this.flagGrouper = flagGrouper;
    }

    public String getClntOrgn() {
        return clntOrgn;
    }

    public void setClntOrgn(String clntOrgn) {
        this.clntOrgn = clntOrgn;
    }

    public String getClntNameOrgn() {
        return clntNameOrgn;
    }

    public void setClntNameOrgn(String clntNameOrgn) {
        this.clntNameOrgn = clntNameOrgn;
    }

    public String getOrgnBrncId() {
        return orgnBrncId;
    }

    public void setOrgnBrncId(String orgnBrncId) {
        this.orgnBrncId = orgnBrncId;
    }

    public String getLatitudeClntDlvy() {
        return latitudeClntDlvy;
    }

    public void setLatitudeClntDlvy(String latitudeClntDlvy) {
        this.latitudeClntDlvy = latitudeClntDlvy;
    }

    public String getLongitudeClntDlvy() {
        return longitudeClntDlvy;
    }

    public void setLongitudeClntDlvy(String longitudeClntDlvy) {
        this.longitudeClntDlvy = longitudeClntDlvy;
    }
    
    
 
//    public int compareTo(Object o) {
//        SysClntGeoDTO dto = (SysClntGeoDTO) o;
//        int t = this.clntId.compareTo(dto.getDlvyDate());
//        if (t < 0) {
//            t = 1;
//        } else {
//            t = -1;
//        }
//        return t;
//    }

    public String getNumbPack() {
        return numbPack;
    }

    public void setNumbPack(String numbPack) {
        this.numbPack = numbPack;
    }

    public String getGuiaAmnt() {
        return guiaAmnt;
    }

    public void setGuiaAmnt(String guiaAmnt) {
        this.guiaAmnt = guiaAmnt;
    }

    public String getTotlVlum() {
        return totlVlum;
    }

    public void setTotlVlum(String totlVlum) {
        this.totlVlum = totlVlum;
    }

    public String getTotlWght() {
        return totlWght;
    }

    public void setTotlWght(String totlWght) {
        this.totlWght = totlWght;
    }

    public String getTimeBetweenDlvy() {
        return timeBetweenDlvy;
    }

    public void setTimeBetweenDlvy(String timeBetweenDlvy) {
        this.timeBetweenDlvy = timeBetweenDlvy;
    }

    public String getPymtMode() {
        return pymtMode;
    }

    public void setPymtMode(String pymtMode) {
        this.pymtMode = pymtMode;
    }

    public String getPymtType() {
        return pymtType;
    }

    public void setPymtType(String pymtType) {
        this.pymtType = pymtType;
    }

    public String getCarita() {
        return carita;
    }

    public void setCarita(String carita) {
        this.carita = carita;
    }
 

}
