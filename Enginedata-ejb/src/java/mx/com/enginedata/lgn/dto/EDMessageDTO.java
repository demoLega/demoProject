/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

/**
 *
 * @author fcanedo
 */
public class EDMessageDTO implements Serializable, Comparable {
    
    private int idMessage;
    private String cveMessage;
    private String message;
    private String module;
    private String type;
    private String deleteFlag;
    
    public EDMessageDTO(){
        
    }

    public int getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(int idMessage) {
        this.idMessage = idMessage;
    }

    public String getCveMessage() {
        return cveMessage;
    }

    public void setCveMessage(String cveMessage) {
        this.cveMessage = cveMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public int compareTo(Object o) {
        
        EDMessageDTO dto = (EDMessageDTO) o;
        int t = this.cveMessage.compareTo(dto.cveMessage);
        if (t < 0) {
            t = 1;
        } else {
            t = -1;
        }
        return t;
        
    }
    

}
