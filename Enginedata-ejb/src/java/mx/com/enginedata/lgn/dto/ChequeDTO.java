/*
 * Copyright(c) enginedata Company, Inc.  All Rights Reserved.
 * This software is the proprietary information of enginedata Company.
 *
 */

package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;

public class ChequeDTO implements Serializable {

    private String numero;
    private Date fecha;
    private String fechaSt;    
    private String tarjeta;    
    
    public ChequeDTO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    public String getFechaSt() {
        return fechaSt;
    }

    public void setFechaSt(String fechaSt) {
        this.fechaSt = fechaSt;
    }    

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }
    
    

}
