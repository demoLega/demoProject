/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author fcanedo
 */
public class InvoiceDTO implements Serializable {

    private Boolean invoiceGenerated;
    private String invoiceId;
    private Date invoiceDate;
    private String paymentType;
    private String guiaNo;
    //private ParameterDTO guiasInvoice;

    public Boolean getInvoiceGenerated() {
        return invoiceGenerated;
    }

    public void setInvoiceGenerated(Boolean invoiceGenerated) {
        this.invoiceGenerated = invoiceGenerated;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getGuiaNo() {
        return guiaNo;
    }

    public void setGuiaNo(String guiaNo) {
        this.guiaNo = guiaNo;
    }

//    public ParameterDTO getGuiasInvoice() {
//        return guiasInvoice;
//    }
//
//    public void setGuiasInvoice(ParameterDTO guiasInvoice) {
//        this.guiasInvoice = guiasInvoice;
//    }

}
