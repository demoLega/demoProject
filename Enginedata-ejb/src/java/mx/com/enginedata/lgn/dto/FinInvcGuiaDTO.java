/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author fcanedo
 */
public class FinInvcGuiaDTO implements Serializable{
    
    private String invcBrncId;
    private String invcFormNo;
    private String guiaNo;
    private Date crtdOn;
    private String crtdBy;
    private Date mdfdOn;
    private String mdfdBy;

    public String getInvcBrncId() {
        return invcBrncId;
    }

    public void setInvcBrncId(String invcBrncId) {
        this.invcBrncId = invcBrncId;
    }

    public String getInvcFormNo() {
        return invcFormNo;
    }

    public void setInvcFormNo(String invcFormNo) {
        this.invcFormNo = invcFormNo;
    }

    public String getGuiaNo() {
        return guiaNo;
    }

    public void setGuiaNo(String guiaNo) {
        this.guiaNo = guiaNo;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }
    
    
}
