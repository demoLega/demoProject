package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;

public class LgnProfileRoleDTO implements Serializable {

    private String prId;
    public String prLevl;
    private Date crtdOn;
    private String crtdBy;
    private Date mdfdOn;
    private String mdfdBy;
    private LgnRolesDTO idRolesDTO;
    private LgnProfilesDTO idProfilesDTO;
    public String prLevlBD;

    public LgnProfileRoleDTO() {
    }

    public LgnProfileRoleDTO(String prId) {
        this.prId = prId;
    }

    public String getPrId() {
        return prId;
    }

    public void setPrId(String prId) {
        this.prId = prId;
    }

    public String getPrLevl() {
        return prLevl;
    }

    public void setPrLevl(String prLevl) {
        this.prLevl = prLevl;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public LgnRolesDTO getIdRolesDTO() {
        return idRolesDTO;
    }

    public void setIdRolesDTO(LgnRolesDTO idRolesDTO) {
        this.idRolesDTO = idRolesDTO;
    }

    public LgnProfilesDTO getIdProfilesDTO() {
        return idProfilesDTO;
    }

    public void setIdProfilesDTO(LgnProfilesDTO idProfilesDTO) {
        this.idProfilesDTO = idProfilesDTO;
    }

    public String getPrLevlBD() {
        return prLevlBD;
    }

    public void setPrLevlBD(String prLevlBD) {
        this.prLevlBD = prLevlBD;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prId != null ? prId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LgnProfileRoleDTO other = (LgnProfileRoleDTO) obj;
        if ((this.prId == null) ? (other.prId != null) : !this.prId.equals(other.prId)) {
            return false;
        }
        if (this.idRolesDTO != other.idRolesDTO && (this.idRolesDTO == null || !this.idRolesDTO.equals(other.idRolesDTO))) {
            return false;
        }
        if (this.idProfilesDTO != other.idProfilesDTO && (this.idProfilesDTO == null || !this.idProfilesDTO.equals(other.idProfilesDTO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LgnProfileRoleDTO{" + "prId=" + prId + ", prLevl=" + prLevl + ", crtdOn=" + crtdOn + ", crtdBy=" + crtdBy + ", mdfdOn=" + mdfdOn + ", mdfdBy=" + mdfdBy + ", idRolesDTO=" + idRolesDTO + ", idProfilesDTO=" + idProfilesDTO + '}';
    }

}
