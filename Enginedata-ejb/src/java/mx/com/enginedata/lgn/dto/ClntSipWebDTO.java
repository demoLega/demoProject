/*
 * Copyright(c) enginedata Company, Inc.  All Rights Reserved.
 * This software is the proprietary information of enginedata Company.
 *
 */

package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

public class ClntSipWebDTO implements Serializable {
     
      private String account_name;
      private String account_id;
      private String id;
      private String  name;
      private String description;
      private String no_cliente_c;
      private String tipo_cliente_c;
      private String pe_su_sucursal_accounts_1_name;
      private String pe_su_sucursal_accounts_1pe_su_sucursal_ida;
      private String assigned_user_id;
      private String assigned_user_name;
      private String email;
      private String email1;
      private String email2;
      private String account_type;
      private String delegacion_municipio_prin_c;
      private String colonia_principal_c;
      private String razon_social_c;
      private String billing_address_street;
      private String billing_address_street_2;
      private String billing_address_street_3;
      private String billing_address_street_4;
      private String billing_address_city;
      private String billing_address_state;
      private String billing_address_postalcode;
      private String billing_address_country;
      private String phone_office;
      private String rfc_c;

    public ClntSipWebDTO() {
    }

    public ClntSipWebDTO(String account_name, String account_id, String id, String name, String description, String no_cliente_c, String tipo_cliente_c, String pe_su_sucursal_accounts_1_name, String pe_su_sucursal_accounts_1pe_su_sucursal_ida, String assigned_user_id, String assigned_user_name, String email, String email1, String email2, String account_type, String delegacion_municipio_prin_c, String colonia_principal_c, String razon_social_c, String billing_address_street, String billing_address_street_2, String billing_address_street_3, String billing_address_street_4, String billing_address_city, String billing_address_state, String billing_address_postalcode, String billing_address_country, String phone_office, String rfc_c) {
        this.account_name = account_name;
        this.account_id = account_id;
        this.id = id;
        this.name = name;
        this.description = description;
        this.no_cliente_c = no_cliente_c;
        this.tipo_cliente_c = tipo_cliente_c;
        this.pe_su_sucursal_accounts_1_name = pe_su_sucursal_accounts_1_name;
        this.pe_su_sucursal_accounts_1pe_su_sucursal_ida = pe_su_sucursal_accounts_1pe_su_sucursal_ida;
        this.assigned_user_id = assigned_user_id;
        this.assigned_user_name = assigned_user_name;
        this.email = email;
        this.email1 = email1;
        this.email2 = email2;
        this.account_type = account_type;
        this.delegacion_municipio_prin_c = delegacion_municipio_prin_c;
        this.colonia_principal_c = colonia_principal_c;
        this.razon_social_c = razon_social_c;
        this.billing_address_street = billing_address_street;
        this.billing_address_street_2 = billing_address_street_2;
        this.billing_address_street_3 = billing_address_street_3;
        this.billing_address_street_4 = billing_address_street_4;
        this.billing_address_city = billing_address_city;
        this.billing_address_state = billing_address_state;
        this.billing_address_postalcode = billing_address_postalcode;
        this.billing_address_country = billing_address_country;
        this.phone_office = phone_office;
        this.rfc_c = rfc_c;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNo_cliente_c() {
        return no_cliente_c;
    }

    public void setNo_cliente_c(String no_cliente_c) {
        this.no_cliente_c = no_cliente_c;
    }

    public String getTipo_cliente_c() {
        return tipo_cliente_c;
    }

    public void setTipo_cliente_c(String tipo_cliente_c) {
        this.tipo_cliente_c = tipo_cliente_c;
    }

    public String getPe_su_sucursal_accounts_1_name() {
        return pe_su_sucursal_accounts_1_name;
    }

    public void setPe_su_sucursal_accounts_1_name(String pe_su_sucursal_accounts_1_name) {
        this.pe_su_sucursal_accounts_1_name = pe_su_sucursal_accounts_1_name;
    }

    public String getPe_su_sucursal_accounts_1pe_su_sucursal_ida() {
        return pe_su_sucursal_accounts_1pe_su_sucursal_ida;
    }

    public void setPe_su_sucursal_accounts_1pe_su_sucursal_ida(String pe_su_sucursal_accounts_1pe_su_sucursal_ida) {
        this.pe_su_sucursal_accounts_1pe_su_sucursal_ida = pe_su_sucursal_accounts_1pe_su_sucursal_ida;
    }

    public String getAssigned_user_id() {
        return assigned_user_id;
    }

    public void setAssigned_user_id(String assigned_user_id) {
        this.assigned_user_id = assigned_user_id;
    }

    public String getAssigned_user_name() {
        return assigned_user_name;
    }

    public void setAssigned_user_name(String assigned_user_name) {
        this.assigned_user_name = assigned_user_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getDelegacion_municipio_prin_c() {
        return delegacion_municipio_prin_c;
    }

    public void setDelegacion_municipio_prin_c(String delegacion_municipio_prin_c) {
        this.delegacion_municipio_prin_c = delegacion_municipio_prin_c;
    }

    public String getColonia_principal_c() {
        return colonia_principal_c;
    }

    public void setColonia_principal_c(String colonia_principal_c) {
        this.colonia_principal_c = colonia_principal_c;
    }

    public String getRazon_social_c() {
        return razon_social_c;
    }

    public void setRazon_social_c(String razon_social_c) {
        this.razon_social_c = razon_social_c;
    }

    public String getBilling_address_street() {
        return billing_address_street;
    }

    public void setBilling_address_street(String billing_address_street) {
        this.billing_address_street = billing_address_street;
    }

    public String getBilling_address_street_2() {
        return billing_address_street_2;
    }

    public void setBilling_address_street_2(String billing_address_street_2) {
        this.billing_address_street_2 = billing_address_street_2;
    }

    public String getBilling_address_street_3() {
        return billing_address_street_3;
    }

    public void setBilling_address_street_3(String billing_address_street_3) {
        this.billing_address_street_3 = billing_address_street_3;
    }

    public String getBilling_address_street_4() {
        return billing_address_street_4;
    }

    public void setBilling_address_street_4(String billing_address_street_4) {
        this.billing_address_street_4 = billing_address_street_4;
    }

    public String getBilling_address_city() {
        return billing_address_city;
    }

    public void setBilling_address_city(String billing_address_city) {
        this.billing_address_city = billing_address_city;
    }

    public String getBilling_address_state() {
        return billing_address_state;
    }

    public void setBilling_address_state(String billing_address_state) {
        this.billing_address_state = billing_address_state;
    }

    public String getBilling_address_postalcode() {
        return billing_address_postalcode;
    }

    public void setBilling_address_postalcode(String billing_address_postalcode) {
        this.billing_address_postalcode = billing_address_postalcode;
    }

    public String getBilling_address_country() {
        return billing_address_country;
    }

    public void setBilling_address_country(String billing_address_country) {
        this.billing_address_country = billing_address_country;
    }

    public String getPhone_office() {
        return phone_office;
    }

    public void setPhone_office(String phone_office) {
        this.phone_office = phone_office;
    }

    public String getRfc_c() {
        return rfc_c;
    }

    public void setRfc_c(String rfc_c) {
        this.rfc_c = rfc_c;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }


}
