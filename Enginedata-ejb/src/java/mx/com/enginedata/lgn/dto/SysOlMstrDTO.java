package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SysOlMstrDTO implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    private String olId;
    private String crtdBy;
    private Date crtdOn;
    private String mdfdBy;
    private Date mdfdOn;
    private String dirFtExt;
    private String dirFtp;
    private BigDecimal maxValDecl;
    private String olActv;
    private Date olActvDate;
    private String olFlag;
    private String olGroup;
    private String olName;
    private String olNameLarge;
    private String olReqRef;
    private String olReqRef2;
    private String olReqType;
    private String olType;
    private String olTypeOog;
    private String olrefruta;
    private String password;
    private BigDecimal tarifAckx;
    private BigDecimal tarifAgr;
    private BigDecimal tarifAmnt;
    private String taxId;
    private String userId;
    private String userPwd;

    public SysOlMstrDTO() {
    }

    public String getCrtdBy() {
        return this.crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getCrtdOn() {
        return this.crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getMdfdBy() {
        return this.mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public Date getMdfdOn() {
        return this.mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getOlId() {
        return olId;
    }

    public void setOlId(String olId) {
        this.olId = olId;
    }

    public String getDirFtExt() {
        return dirFtExt;
    }

    public void setDirFtExt(String dirFtExt) {
        this.dirFtExt = dirFtExt;
    }

    public String getDirFtp() {
        return dirFtp;
    }

    public void setDirFtp(String dirFtp) {
        this.dirFtp = dirFtp;
    }

    public BigDecimal getMaxValDecl() {
        return maxValDecl;
    }

    public void setMaxValDecl(BigDecimal maxValDecl) {
        this.maxValDecl = maxValDecl;
    }

    public String getOlActv() {
        return olActv;
    }

    public void setOlActv(String olActv) {
        this.olActv = olActv;
    }

    public Date getOlActvDate() {
        return olActvDate;
    }

    public void setOlActvDate(Date olActvDate) {
        this.olActvDate = olActvDate;
    }

    public String getOlFlag() {
        return olFlag;
    }

    public void setOlFlag(String olFlag) {
        this.olFlag = olFlag;
    }

    public String getOlGroup() {
        return olGroup;
    }

    public void setOlGroup(String olGroup) {
        this.olGroup = olGroup;
    }

    public String getOlName() {
        return olName;
    }

    public void setOlName(String olName) {
        this.olName = olName;
    }

    public String getOlNameLarge() {
        return olNameLarge;
    }

    public void setOlNameLarge(String olNameLarge) {
        this.olNameLarge = olNameLarge;
    }

    public String getOlReqRef() {
        return olReqRef;
    }

    public void setOlReqRef(String olReqRef) {
        this.olReqRef = olReqRef;
    }

    public String getOlReqRef2() {
        return olReqRef2;
    }

    public void setOlReqRef2(String olReqRef2) {
        this.olReqRef2 = olReqRef2;
    }

    public String getOlReqType() {
        return olReqType;
    }

    public void setOlReqType(String olReqType) {
        this.olReqType = olReqType;
    }

    public String getOlType() {
        return olType;
    }

    public void setOlType(String olType) {
        this.olType = olType;
    }

    public String getOlTypeOog() {
        return olTypeOog;
    }

    public void setOlTypeOog(String olTypeOog) {
        this.olTypeOog = olTypeOog;
    }

    public String getOlrefruta() {
        return olrefruta;
    }

    public void setOlrefruta(String olrefruta) {
        this.olrefruta = olrefruta;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getTarifAckx() {
        return tarifAckx;
    }

    public void setTarifAckx(BigDecimal tarifAckx) {
        this.tarifAckx = tarifAckx;
    }

    public BigDecimal getTarifAgr() {
        return tarifAgr;
    }

    public void setTarifAgr(BigDecimal tarifAgr) {
        this.tarifAgr = tarifAgr;
    }

    public BigDecimal getTarifAmnt() {
        return tarifAmnt;
    }

    public void setTarifAmnt(BigDecimal tarifAmnt) {
        this.tarifAmnt = tarifAmnt;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }
    
    @Override 
    public SysOlMstrDTO clone() {
        try {
            final SysOlMstrDTO result = (SysOlMstrDTO) super.clone();
            // copy fields that need to be copied here!
            return result;
        } catch (final CloneNotSupportedException ex) {
            throw new AssertionError();
        }
    }
}
