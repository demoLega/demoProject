/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author mdimas
 */
public class WebManifestDetailDTO implements Serializable{
   
    String mnftno;
    String clntid;
    String brncid;
    String issedate;
    String numbordr;
    Integer numbpack;
    BigDecimal mnftamnt;
    String mnftstus;
    String invcdate;
    String actvflag;
    String plancolldate;
    String actlcolldate;
    String invcno;
    String tripno;
    String tripstus;
    String routeid;
    String truckno;
    String fttrefrno;
    String cttrefrno;
    String flag1;
    String flag2;
    String flag3;
    String flag4;
    String crtdon;
    String crtdby;
    String fdon;
    String fdby;
    String flag5;
    String addrlin4;
    String addrlin5;
    String addrlin6;
    String addrlin7;
    String addrcode;
    String strtname;
    String drnr;
    String phno1;
    String florno;
    String suitno;
    String zipcode;
    String addrlin1;
    String addrlin2;
    String addrlin3;
    String subtotl;
    String sbranchid;
    String sinvrefno;
    String pymtmode;
    String arguflag;
    String orderplanini;
    String orderplanend;
    String deleteflag;
    String docutype;
    String radstus;
    String signseqn;
    String mnftrel;
    String rightno;
    String breakini;
    String breakend;

    public String getMnftno() {
        return mnftno;
    }

    public void setMnftno(String mnftno) {
        this.mnftno = mnftno;
    }

    public String getClntid() {
        return clntid;
    }

    public void setClntid(String clntid) {
        this.clntid = clntid;
    }

    public String getBrncid() {
        return brncid;
    }

    public void setBrncid(String brncid) {
        this.brncid = brncid;
    }

    public String getIssedate() {
        return issedate;
    }

    public void setIssedate(String issedate) {
        this.issedate = issedate;
    }

    public String getNumbordr() {
        return numbordr;
    }

    public void setNumbordr(String numbordr) {
        this.numbordr = numbordr;
    }

    public Integer getNumbpack() {
        return numbpack;
    }

    public void setNumbpack(Integer numbpack) {
        this.numbpack = numbpack;
    }

    public BigDecimal getMnftamnt() {
        return mnftamnt;
    }

    public void setMnftamnt(BigDecimal mnftamnt) {
        this.mnftamnt = mnftamnt;
    }

    public String getMnftstus() {
        return mnftstus;
    }

    public void setMnftstus(String mnftstus) {
        this.mnftstus = mnftstus;
    }

    public String getInvcdate() {
        return invcdate;
    }

    public void setInvcdate(String invcdate) {
        this.invcdate = invcdate;
    }

    public String getActvflag() {
        return actvflag;
    }

    public void setActvflag(String actvflag) {
        this.actvflag = actvflag;
    }

    public String getPlancolldate() {
        return plancolldate;
    }

    public void setPlancolldate(String plancolldate) {
        this.plancolldate = plancolldate;
    }

    public String getActlcolldate() {
        return actlcolldate;
    }

    public void setActlcolldate(String actlcolldate) {
        this.actlcolldate = actlcolldate;
    }

    public String getInvcno() {
        return invcno;
    }

    public void setInvcno(String invcno) {
        this.invcno = invcno;
    }

    public String getTripno() {
        return tripno;
    }

    public void setTripno(String tripno) {
        this.tripno = tripno;
    }

    public String getTripstus() {
        return tripstus;
    }

    public void setTripstus(String tripstus) {
        this.tripstus = tripstus;
    }

    public String getRouteid() {
        return routeid;
    }

    public void setRouteid(String routeid) {
        this.routeid = routeid;
    }

    public String getTruckno() {
        return truckno;
    }

    public void setTruckno(String truckno) {
        this.truckno = truckno;
    }

    public String getFttrefrno() {
        return fttrefrno;
    }

    public void setFttrefrno(String fttrefrno) {
        this.fttrefrno = fttrefrno;
    }

    public String getCttrefrno() {
        return cttrefrno;
    }

    public void setCttrefrno(String cttrefrno) {
        this.cttrefrno = cttrefrno;
    }

    public String getFlag1() {
        return flag1;
    }

    public void setFlag1(String flag1) {
        this.flag1 = flag1;
    }

    public String getFlag2() {
        return flag2;
    }

    public void setFlag2(String flag2) {
        this.flag2 = flag2;
    }

    public String getFlag3() {
        return flag3;
    }

    public void setFlag3(String flag3) {
        this.flag3 = flag3;
    }

    public String getFlag4() {
        return flag4;
    }

    public void setFlag4(String flag4) {
        this.flag4 = flag4;
    }

    public String getCrtdon() {
        return crtdon;
    }

    public void setCrtdon(String crtdon) {
        this.crtdon = crtdon;
    }

    public String getCrtdby() {
        return crtdby;
    }

    public void setCrtdby(String crtdby) {
        this.crtdby = crtdby;
    }

    public String getFdon() {
        return fdon;
    }

    public void setFdon(String fdon) {
        this.fdon = fdon;
    }

    public String getFdby() {
        return fdby;
    }

    public void setFdby(String fdby) {
        this.fdby = fdby;
    }

    public String getFlag5() {
        return flag5;
    }

    public void setFlag5(String flag5) {
        this.flag5 = flag5;
    }

    public String getAddrlin4() {
        return addrlin4;
    }

    public void setAddrlin4(String addrlin4) {
        this.addrlin4 = addrlin4;
    }

    public String getAddrlin5() {
        return addrlin5;
    }

    public void setAddrlin5(String addrlin5) {
        this.addrlin5 = addrlin5;
    }

    public String getAddrlin6() {
        return addrlin6;
    }

    public void setAddrlin6(String addrlin6) {
        this.addrlin6 = addrlin6;
    }

    public String getAddrlin7() {
        return addrlin7;
    }

    public void setAddrlin7(String addrlin7) {
        this.addrlin7 = addrlin7;
    }

    public String getAddrcode() {
        return addrcode;
    }

    public void setAddrcode(String addrcode) {
        this.addrcode = addrcode;
    }

    public String getStrtname() {
        return strtname;
    }

    public void setStrtname(String strtname) {
        this.strtname = strtname;
    }

    public String getDrnr() {
        return drnr;
    }

    public void setDrnr(String drnr) {
        this.drnr = drnr;
    }

    public String getPhno1() {
        return phno1;
    }

    public void setPhno1(String phno1) {
        this.phno1 = phno1;
    }

    public String getFlorno() {
        return florno;
    }

    public void setFlorno(String florno) {
        this.florno = florno;
    }

    public String getSuitno() {
        return suitno;
    }

    public void setSuitno(String suitno) {
        this.suitno = suitno;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getAddrlin1() {
        return addrlin1;
    }

    public void setAddrlin1(String addrlin1) {
        this.addrlin1 = addrlin1;
    }

    public String getAddrlin2() {
        return addrlin2;
    }

    public void setAddrlin2(String addrlin2) {
        this.addrlin2 = addrlin2;
    }

    public String getAddrlin3() {
        return addrlin3;
    }

    public void setAddrlin3(String addrlin3) {
        this.addrlin3 = addrlin3;
    }

    public String getSubtotl() {
        return subtotl;
    }

    public void setSubtotl(String subtotl) {
        this.subtotl = subtotl;
    }

    public String getSbranchid() {
        return sbranchid;
    }

    public void setSbranchid(String sbranchid) {
        this.sbranchid = sbranchid;
    }

    public String getSinvrefno() {
        return sinvrefno;
    }

    public void setSinvrefno(String sinvrefno) {
        this.sinvrefno = sinvrefno;
    }

    public String getPymtmode() {
        return pymtmode;
    }

    public void setPymtmode(String pymtmode) {
        this.pymtmode = pymtmode;
    }

    public String getArguflag() {
        return arguflag;
    }

    public void setArguflag(String arguflag) {
        this.arguflag = arguflag;
    }

    public String getOrderplanini() {
        return orderplanini;
    }

    public void setOrderplanini(String orderplanini) {
        this.orderplanini = orderplanini;
    }

    public String getOrderplanend() {
        return orderplanend;
    }

    public void setOrderplanend(String orderplanend) {
        this.orderplanend = orderplanend;
    }

    public String getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getDocutype() {
        return docutype;
    }

    public void setDocutype(String docutype) {
        this.docutype = docutype;
    }

    public String getRadstus() {
        return radstus;
    }

    public void setRadstus(String radstus) {
        this.radstus = radstus;
    }

    public String getSignseqn() {
        return signseqn;
    }

    public void setSignseqn(String signseqn) {
        this.signseqn = signseqn;
    }

    public String getMnftrel() {
        return mnftrel;
    }

    public void setMnftrel(String mnftrel) {
        this.mnftrel = mnftrel;
    }

    public String getRightno() {
        return rightno;
    }

    public void setRightno(String rightno) {
        this.rightno = rightno;
    }

    public String getBreakini() {
        return breakini;
    }

    public void setBreakini(String breakini) {
        this.breakini = breakini;
    }

    public String getBreakend() {
        return breakend;
    }

    public void setBreakend(String breakend) {
        this.breakend = breakend;
    }

    
    
}
