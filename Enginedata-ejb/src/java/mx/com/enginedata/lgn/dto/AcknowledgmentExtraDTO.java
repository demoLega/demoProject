/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

/**
 *
 * @author jasanchez Esta clase es usada para cuando una guia tenga activo el
 * acuse XT y sera generado indicar el rastreo que se genero y a que guia.
 */
public class AcknowledgmentExtraDTO implements Serializable {

    public AcknowledgmentExtraDTO() {
    }

    private String trackingNumberExtra;
    private String trackingNumber;

    public String getTrackingNumberExtra() {
        return trackingNumberExtra;
    }

    public void setTrackingNumberExtra(String trackingNumberExtra) {
        this.trackingNumberExtra = trackingNumberExtra;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

}
