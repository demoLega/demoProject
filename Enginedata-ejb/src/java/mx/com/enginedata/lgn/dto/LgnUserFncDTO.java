/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LgnUserFncDTO implements Serializable {
    
    private String modulo;
    private String functionId;
    private String nombre;
    private String descripcion;        
    private String moduloFav;
    private String funcionIdFav;
    private String nombreFav;
    private String descripcionFav;
 
    private String idUser; 
    private String idFunction;
    private String descripction;
    private String orden;
    
    private String urlFav;    
    private String lfIcon;    
    private String lfMultiDialog;
    private String lfHeight;
    private String lfWidht;

    public String getLfIcon() {
        return lfIcon;
    }

    public void setLfIcon(String lfIcon) {
        this.lfIcon = lfIcon;
    }
 
    public String getUrlFav() {
        return urlFav;
    }

    public void setUrlFav(String urlFav) {
        this.urlFav = urlFav;
    }    
 
    public String getModuloFav() {
        return moduloFav;
    }

    public void setModuloFav(String moduloFav) {
        this.moduloFav = moduloFav;
    } 

    public String getFuncionIdFav() {
        return funcionIdFav;
    }

    public void setFuncionIdFav(String funcionIdFav) {
        this.funcionIdFav = funcionIdFav;
    }

    public String getNombreFav() {
        return nombreFav;
    }

    public void setNombreFav(String nombreFav) {
        this.nombreFav = nombreFav;
    }

    public String getDescripcionFav() {
        return descripcionFav;
    }

    public void setDescripcionFav(String descripcionFav) {
        this.descripcionFav = descripcionFav;
    }
    
    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdFunction() {
        return idFunction;
    }

    public void setIdFunction(String idFunction) {
        this.idFunction = idFunction;
    }

    public String getDescripction() {
        return descripction;
    }

    public void setDescripction(String descripction) {
        this.descripction = descripction;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getLfMultiDialog() {
        return lfMultiDialog;
    }

    public void setLfMultiDialog(String lfMultiDialog) {
        this.lfMultiDialog = lfMultiDialog;
    }

    public String getLfHeight() {
        return lfHeight;
    }

    public void setLfHeight(String lfHeight) {
        this.lfHeight = lfHeight;
    }

    public String getLfWidht() {
        return lfWidht;
    }

    public void setLfWidht(String lfWidht) {
        this.lfWidht = lfWidht;
    }


   
}
