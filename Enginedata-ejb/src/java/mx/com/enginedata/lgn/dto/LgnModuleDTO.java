/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author fcanedo
 */
public class LgnModuleDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long idModule;
    private String lmName;
    private String lmStus;
    private Date crtdOn;
    private String crtdBy;
    private Date mdfdOn;
    private String mdfdBy;
    //private List<LgnFunctionsEO> lgnFunctionsEOList;

    public LgnModuleDTO() {
    }

    public LgnModuleDTO(Long idModule) {
        this.idModule = idModule;
    }

    public Long getIdModule() {
        return idModule;
    }

    public void setIdModule(Long idModule) {
        this.idModule = idModule;
    }

    public String getLmName() {
        return lmName;
    }

    public void setLmName(String lmName) {
        this.lmName = lmName;
    }

    public String getLmStus() {
        return lmStus;
    }

    public void setLmStus(String lmStus) {
        this.lmStus = lmStus;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }
    /*
    public List<LgnFunctionsEO> getLgnFunctionsEOList() {
        return lgnFunctionsEOList;
    }

    public void setLgnFunctionsEOList(List<LgnFunctionsEO> lgnFunctionsEOList) {
        this.lgnFunctionsEOList = lgnFunctionsEOList;
    }
    */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModule != null ? idModule.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LgnModuleDTO)) {
            return false;
        }
        LgnModuleDTO other = (LgnModuleDTO) object;
        if ((this.idModule == null && other.idModule != null) || (this.idModule != null && !this.idModule.equals(other.idModule))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.enginedata.gobernance.model.entity.LgnModuleEO[ idModule=" + idModule + " ]";
    }
    
}
