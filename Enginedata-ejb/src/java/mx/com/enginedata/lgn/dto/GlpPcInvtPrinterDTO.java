/*
 * Copyright(c) enginedata Company, Inc.  All Rights Reserved.
 * This software is the proprietary information of enginedata Company.
 *
 */

package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

public class GlpPcInvtPrinterDTO implements Serializable {

    private String macAddr;
    private String name;
    private String type;
    private String id;   

    public String getMacAddr() {
        return macAddr;
    }

    public void setMacAddr(String macAddr) {
        this.macAddr = macAddr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
