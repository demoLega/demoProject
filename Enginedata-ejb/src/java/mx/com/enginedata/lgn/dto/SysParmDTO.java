/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

/**
 *
 * @author janaya
 */
public class SysParmDTO implements Serializable {

    private String paramID;
    private String paramDesc;
    private String paramValue;

    public SysParmDTO() {
    }

    public SysParmDTO(String paramID, String paramDesc) {
        this.paramID = paramID;
        this.paramDesc = paramDesc;
    }

    public String getParamID() {
        return paramID;
    }

    public void setParamID(String ParamID) {
        this.paramID = ParamID;
    }

    public String getParamDesc() {
        return paramDesc;
    }

    public void setParamDesc(String paramDesc) {
        this.paramDesc = paramDesc;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
    
    @Override
    public String toString() {
        return "SysParmDTO{" + "paramID=" + paramID + ", paramDesc=" + paramDesc + '}';
    }

}
