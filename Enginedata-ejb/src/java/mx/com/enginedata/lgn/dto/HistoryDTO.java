/*
 * Copyright(c) enginedata Company, Inc.  All Rights Reserved.
 * This software is the proprietary information of enginedata Company.
 *
 */

package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.List;

public class HistoryDTO implements Serializable {

    private String imagen;
    private String guiaOrRastreo;
    private String orig;
    private String dest;
    private String status;
    private String persona;
    private String fecha;
    private String solicito;
    
    private String dia;
    private String dianum;
    private String mes;
    private String anio;
    
    
    private List<HistoryDetailDTO> listHistoryDetails;

    public HistoryDTO() {
    }
    

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }    

    public String getGuiaOrRastreo() {
        return guiaOrRastreo;
    }

    public void setGuiaOrRastreo(String guiaOrRastreo) {
        this.guiaOrRastreo = guiaOrRastreo;
    }
    
    public String getOrig() {
        return orig;
    }

    public void setOrig(String orig) {
        this.orig = orig;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<HistoryDetailDTO> getListHistoryDetails() {
        return listHistoryDetails;
    }

    public void setListHistoryDetails(List<HistoryDetailDTO> listHistoryDetails) {
        this.listHistoryDetails = listHistoryDetails;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getDianum() {
        return dianum;
    }

    public void setDianum(String dianum) {
        this.dianum = dianum;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getSolicito() {
        return solicito;
    }

    public void setSolicito(String solicito) {
        this.solicito = solicito;
    }
 
}
