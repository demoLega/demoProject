package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author fcanedo
 */
public class RadConvertResultDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private InvoiceDTO invoice;
    private List<PaymentDTO> listPaymentsNoInvoice;
    private List<AcknowledgmentExtraDTO> listAcknowledgmentExtraDTO;

    public RadConvertResultDTO() {

    }

    public RadConvertResultDTO(InvoiceDTO invoice, List<PaymentDTO> listPaymentsNoInvoice) {
        this.invoice = invoice;
        this.listPaymentsNoInvoice = listPaymentsNoInvoice;
    }

    public InvoiceDTO getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceDTO invoice) {
        this.invoice = invoice;
    }

    public List<PaymentDTO> getListPaymentsNoInvoice() {
        return listPaymentsNoInvoice;
    }

    public void setListPaymentsNoInvoice(List<PaymentDTO> listPaymentsNoInvoice) {
        this.listPaymentsNoInvoice = listPaymentsNoInvoice;
    }

    public List<AcknowledgmentExtraDTO> getListAcknowledgmentExtraDTO() {
        return listAcknowledgmentExtraDTO;
    }

    public void setListAcknowledgmentExtraDTO(List<AcknowledgmentExtraDTO> listAcknowledgmentExtraDTO) {
        this.listAcknowledgmentExtraDTO = listAcknowledgmentExtraDTO;
    }

}
