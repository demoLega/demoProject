package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LgnFunctionsDTO implements Serializable {

    public String idFunction;
    public String lfName;
    private String lfStus;
    public String lfLevel;
    public String lfUrl;
    private Date crtdOn;
    private String crtdBy;
    private Date mdfdOn;
    private String mdfdBy;
    private LgnModuleDTO lgnModuleDTO;
    private LgnFuncTypeDTO funcTypeDTO;
    public LgnRolesDTO idRolesDTO;
    private String deleteFlag;
    private String lfIcon;
    public transient StructIdiomaDTO structIdiomaDTO;
    private List<LgnActionDTO> listActionCurrentTMP;
    private List<LgnActionDTO> listActionInFunctionBD;
    
    private String lfMultiDialog;
    private String lfHeight;
    private String lfWidht;

    public LgnFunctionsDTO() {
    }

    public String getIdFunction() {
        return idFunction;
    }

    public void setIdFunction(String idFunction) {
        this.idFunction = idFunction;
    }

    public String getLfName() {
        return lfName;
    }

    public void setLfName(String lfName) {
        this.lfName = lfName;
    }

    public String getLfStus() {
        return lfStus;
    }

    public void setLfStus(String lfStus) {
        this.lfStus = lfStus;
    }

    public String getLfLevel() {
        return lfLevel;
    }

    public void setLfLevel(String lfLevel) {
        this.lfLevel = lfLevel;
    }

    public String getLfUrl() {
        return lfUrl;
    }

    public void setLfUrl(String lfUrl) {
        this.lfUrl = lfUrl;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public LgnModuleDTO getLgnModuleDTO() {
        return lgnModuleDTO;
    }

    public void setLgnModuleDTO(LgnModuleDTO lgnModuleDTO) {
        this.lgnModuleDTO = lgnModuleDTO;
    }

    public LgnFuncTypeDTO getFuncTypeDTO() {
        return funcTypeDTO;
    }

    public void setFuncTypeDTO(LgnFuncTypeDTO funcTypeDTO) {
        this.funcTypeDTO = funcTypeDTO;
    }

    public LgnRolesDTO getIdRolesDTO() {
        return idRolesDTO;
    }

    public void setIdRolesDTO(LgnRolesDTO idRolesDTO) {
        this.idRolesDTO = idRolesDTO;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getLfIcon() {
        return lfIcon;
    }

    public void setLfIcon(String lfIcon) {
        this.lfIcon = lfIcon;
    }

    public StructIdiomaDTO getStructIdiomaDTO() {
        return structIdiomaDTO;
    }

    public void setStructIdiomaDTO(StructIdiomaDTO structIdiomaDTO) {
        this.structIdiomaDTO = structIdiomaDTO;
    }

    public List<LgnActionDTO> getListActionCurrentTMP() {
        return listActionCurrentTMP;
    }

    public void setListActionCurrentTMP(List<LgnActionDTO> listActionCurrentTMP) {
        this.listActionCurrentTMP = listActionCurrentTMP;
    }

    public List<LgnActionDTO> getListActionInFunctionBD() {
        return listActionInFunctionBD;
    }

    public void setListActionInFunctionBD(List<LgnActionDTO> listActionInFunctionBD) {
        this.listActionInFunctionBD = listActionInFunctionBD;
    }

    public String getLfMultiDialog() {
        return lfMultiDialog;
    }

    public void setLfMultiDialog(String lfMultiDialog) {
        this.lfMultiDialog = lfMultiDialog;
    }

    public String getLfHeight() {
        return lfHeight;
    }

    public void setLfHeight(String lfHeight) {
        this.lfHeight = lfHeight;
    }

    public String getLfWidht() {
        return lfWidht;
    }

    public void setLfWidht(String lfWidht) {
        this.lfWidht = lfWidht;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LgnFunctionsDTO other = (LgnFunctionsDTO) obj;
        if ((this.idFunction == null) ? (other.idFunction != null) : !this.idFunction.equals(other.idFunction)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LgnFunctionsDTO{" + "idFunction=" + idFunction + ", lfName=" + lfName + ", lfStus=" + lfStus + ", lfLevel=" + lfLevel + ", lfUrl=" + lfUrl + ", crtdOn=" + crtdOn + ", crtdBy=" + crtdBy + ", mdfdOn=" + mdfdOn + ", mdfdBy=" + mdfdBy + ", lgnModuleDTO=" + lgnModuleDTO + ", funcTypeDTO=" + funcTypeDTO + ", idRolesDTO=" + idRolesDTO + '}';
    }

}
