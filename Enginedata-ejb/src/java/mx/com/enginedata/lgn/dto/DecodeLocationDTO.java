 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 
package mx.com.enginedata.lgn.dto;
  
 
import java.io.Serializable;
//import static weblogic.xml.crypto.utils.DOMUtils.is;
/**
 *
 * @author mdimas
 */
 
public class DecodeLocationDTO implements Serializable {

    private double latitude;

    private double longitude;

    public DecodeLocationDTO() {

    }

    public DecodeLocationDTO(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }
}

