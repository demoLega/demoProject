/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;
import java.io.Serializable;

/**
 *
 * @author mdimas
 */
public class GenericDTO implements Serializable{
    
    public String genericString1;
    public String genericString2;
    public String genericString3;
    public String genericString4;

    public String getGenericString1() {
        return genericString1;
    }

    public void setGenericString1(String genericString1) {
        this.genericString1 = genericString1;
    }

    public String getGenericString2() {
        return genericString2;
    }

    public void setGenericString2(String genericString2) {
        this.genericString2 = genericString2;
    }

    public String getGenericString3() {
        return genericString3;
    }

    public void setGenericString3(String genericString3) {
        this.genericString3 = genericString3;
    }

    public String getGenericString4() {
        return genericString4;
    }

    public void setGenericString4(String genericString4) {
        this.genericString4 = genericString4;
    }

    
    
}
