/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author fcanedo
 */
public class ClientCreditDTO implements Serializable, Comparable {
    
    private String clientId;
    private BigDecimal creditAvailable;

    public ClientCreditDTO() {
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getCreditAvailable() {
        return creditAvailable;
    }

    public void setCreditAvailable(BigDecimal creditAvailable) {
        this.creditAvailable = creditAvailable;
    }  
    
    public int compareTo(Object o) {
        ClientCreditDTO dto = (ClientCreditDTO) o;
        int t = this.clientId.compareTo(dto.clientId);
        if (t < 0) {
            t = 1;
        } else {
            t = -1;
        }
        return t;
    }
    
}
