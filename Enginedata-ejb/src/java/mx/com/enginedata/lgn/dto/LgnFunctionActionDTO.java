/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author jasanchez
 */
public class LgnFunctionActionDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private BigDecimal idFuncActi;
    private LgnFunctionsDTO idFunctionDTO;
    private LgnActionDTO idActionDTO;

    public LgnFunctionActionDTO() {
    }

    public BigDecimal getIdFuncActi() {
        return idFuncActi;
    }

    public void setIdFuncActi(BigDecimal idFuncActi) {
        this.idFuncActi = idFuncActi;
    }

    public LgnFunctionsDTO getIdFunctionDTO() {
        return idFunctionDTO;
    }

    public void setIdFunctionDTO(LgnFunctionsDTO idFunctionDTO) {
        this.idFunctionDTO = idFunctionDTO;
    }

    public LgnActionDTO getIdActionDTO() {
        return idActionDTO;
    }

    public void setIdActionDTO(LgnActionDTO idActionDTO) {
        this.idActionDTO = idActionDTO;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LgnFunctionActionDTO other = (LgnFunctionActionDTO) obj;
        if (this.idFunctionDTO != other.idFunctionDTO && (this.idFunctionDTO == null || !this.idFunctionDTO.equals(other.idFunctionDTO))) {
            return false;
        }
        if (this.idActionDTO != other.idActionDTO && (this.idActionDTO == null || !this.idActionDTO.equals(other.idActionDTO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LgnFunctionActionDTO{" + "idFuncActi=" + idFuncActi + ", idFunctionDTO=" + idFunctionDTO + ", idActionDTO=" + idActionDTO + '}';
    }

}
