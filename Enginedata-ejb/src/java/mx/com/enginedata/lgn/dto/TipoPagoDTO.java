/*
 * Copyright(c) enginedata Company, Inc.  All Rights Reserved.
 * This software is the proprietary information of enginedata Company.
 *
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

public class TipoPagoDTO implements Serializable {

    private Integer partida = 0;
    private String tipoId;
    private String tipoName;
    private double importe;
    private String actionCrud = "add";
    private String cardId;
    private String rastreo;

    private BankMstrDTO bankMstrDTO = new BankMstrDTO();
    private ChequeDTO chequeDTO = new ChequeDTO();

    public TipoPagoDTO() {
    }
    
     public TipoPagoDTO(Integer partida, String tipoId, String tipoName, double importe, String actionCrud, BankMstrDTO bankMstrDTO, ChequeDTO chequeDTO) {
         this.partida = partida;
         this.tipoId = tipoId;
         this.tipoName = tipoName;
         this.importe = importe;
         this.actionCrud = actionCrud;
         this.bankMstrDTO = bankMstrDTO;
         this.chequeDTO = chequeDTO;
    }

    public Integer getPartida() {
        return partida;
    }

    public void setPartida(Integer partida) {
        this.partida = partida;
    }

    public String getTipoId() {
        return tipoId;
    }

    public void setTipoId(String tipoId) {
        this.tipoId = tipoId;
    }

    public String getTipoName() {
        return tipoName;
    }

    public void setTipoName(String tipoName) {
        this.tipoName = tipoName;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public BankMstrDTO getBankMstrDTO() {
        return bankMstrDTO;
    }

    public void setBankMstrDTO(BankMstrDTO bankMstrDTO) {
        this.bankMstrDTO = bankMstrDTO;
    }

    public ChequeDTO getChequeDTO() {
        return chequeDTO;
    }

    public void setChequeDTO(ChequeDTO chequeDTO) {
        this.chequeDTO = chequeDTO;
    }

    public String getActionCrud() {
        return actionCrud;
    }

    public void setActionCrud(String actionCrud) {
        this.actionCrud = actionCrud;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getRastreo() {
        return rastreo;
    }

    public void setRastreo(String rastreo) {
        this.rastreo = rastreo;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoPagoDTO other = (TipoPagoDTO) obj;
        if (this.tipoId.equalsIgnoreCase(other.tipoId)) {
            return false;
        }
        return true;
    }

}
