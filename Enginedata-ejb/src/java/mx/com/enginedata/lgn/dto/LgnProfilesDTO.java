/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LgnProfilesDTO implements Serializable {

    public String idProfile;
    private String lpStus;
    public String lpName;
    private Date crtdOn;
    private String crtdBy;
    private Date mdfdOn;
    private String mdfdBy;
    private List<LgnUserMstrDTO> lgnUserMstrEOList;
    private List<LgnProfileRoleDTO> lgnProfileRoleEOList;
    private String lpDeleteFlag;

    public LgnProfilesDTO() {
    }

    public LgnProfilesDTO(String idProfile) {
        this.idProfile = idProfile;
    }

    public String getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(String idProfile) {
        this.idProfile = idProfile;
    }

    public String getLpStus() {
        return lpStus;
    }

    public void setLpStus(String lpStus) {
        this.lpStus = lpStus;
    }

    public String getLpName() {
        return lpName;
    }

    public void setLpName(String lpName) {
        this.lpName = lpName;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public List<LgnUserMstrDTO> getLgnUserMstrEOList() {
        return lgnUserMstrEOList;
    }

    public void setLgnUserMstrEOList(List<LgnUserMstrDTO> lgnUserMstrEOList) {
        this.lgnUserMstrEOList = lgnUserMstrEOList;
    }

    public List<LgnProfileRoleDTO> getLgnProfileRoleEOList() {
        return lgnProfileRoleEOList;
    }

    public void setLgnProfileRoleEOList(List<LgnProfileRoleDTO> lgnProfileRoleEOList) {
        this.lgnProfileRoleEOList = lgnProfileRoleEOList;
    }

    public String getLpDeleteFlag() {
        return lpDeleteFlag;
    }

    public void setLpDeleteFlag(String lpDeleteFlag) {
        this.lpDeleteFlag = lpDeleteFlag;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProfile != null ? idProfile.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LgnProfilesDTO)) {
            return false;
        }
        LgnProfilesDTO other = (LgnProfilesDTO) object;
        if ((this.idProfile == null && other.idProfile != null) || (this.idProfile != null && !this.idProfile.equals(other.idProfile))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LgnProfilesDTO{" + "idProfile=" + idProfile + ", lpStus=" + lpStus + ", lpName=" + lpName + ", crtdOn=" + crtdOn + ", crtdBy=" + crtdBy + ", mdfdOn=" + mdfdOn + ", mdfdBy=" + mdfdBy + ", lgnUserMstrEOList=" + lgnUserMstrEOList + ", lgnProfileRoleEOList=" + lgnProfileRoleEOList + ", lpDeleteFlag=" + lpDeleteFlag + '}';
    }
    
}
