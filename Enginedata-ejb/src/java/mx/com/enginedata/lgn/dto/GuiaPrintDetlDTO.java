/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author jgil
 */
public class GuiaPrintDetlDTO   implements Serializable {

    private String id;
    private String guiaNo;
    private String formNo;
    private String brncID;
    private String crtdBy;
    private Date crtdOn;
    private String mdfdBy;
    private Date mdfdOn;
    public GuiaPrintDetlDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGuiaNo() {
        return guiaNo;
    }

    public void setGuiaNo(String guiaNo) {
        this.guiaNo = guiaNo;
    }

    public String getFormNo() {
        return formNo;
    }

    public void setFormNo(String formNo) {
        this.formNo = formNo;
    }

    public String getBrncID() {
        return brncID;
    }

    public void setBrncID(String brncID) {
        this.brncID = brncID;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

}
