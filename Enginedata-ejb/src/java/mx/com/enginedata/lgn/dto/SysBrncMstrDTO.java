/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

/**
 *
 * @author mdimas
 */
public class SysBrncMstrDTO implements Serializable {

    private String bmBrncID, bmBrncName, siteId, latitude, longitude, bcZone, bmFlag2;

    public String getBmBrncID() {
        return bmBrncID;
    }

    public void setBmBrncID(String bmBrncID) {
        this.bmBrncID = bmBrncID;
    }

    public String getBmBrncName() {
        return bmBrncName;
    }

    public void setBmBrncName(String bmBrncName) {
        this.bmBrncName = bmBrncName;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getBcZone() {
        return bcZone;
    }

    public void setBcZone(String bcZone) {
        this.bcZone = bcZone;
    }

    public String getBmFlag2() {
        return bmFlag2;
    }

    public void setBmFlag2(String bmFlag2) {
        this.bmFlag2 = bmFlag2;
    }    
}