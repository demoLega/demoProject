package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;

public class SysParmMstrDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String crtdBy;
    private Date crtdOn;
    private String mdfdBy;
    private Date mdfdOn;
    private String comt;
    private String deleteFlag;
    private String mdulId;
    private String parmCode1;
    private String parmCode2;
    private String parmType;
    private String vlue1Desc;
    private String vlue1Id;
    private String vlue2Desc;
    private String vlue2Id;

    public SysParmMstrDTO() {
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getComt() {
        return comt;
    }

    public void setComt(String comt) {
        this.comt = comt;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getMdulId() {
        return mdulId;
    }

    public void setMdulId(String mdulId) {
        this.mdulId = mdulId;
    }

    public String getParmCode1() {
        return parmCode1;
    }

    public void setParmCode1(String parmCode1) {
        this.parmCode1 = parmCode1;
    }

    public String getParmCode2() {
        return parmCode2;
    }

    public void setParmCode2(String parmCode2) {
        this.parmCode2 = parmCode2;
    }

    public String getParmType() {
        return parmType;
    }

    public void setParmType(String parmType) {
        this.parmType = parmType;
    }

    public String getVlue1Desc() {
        return vlue1Desc;
    }

    public void setVlue1Desc(String vlue1Desc) {
        this.vlue1Desc = vlue1Desc;
    }

    public String getVlue1Id() {
        return vlue1Id;
    }

    public void setVlue1Id(String vlue1Id) {
        this.vlue1Id = vlue1Id;
    }

    public String getVlue2Desc() {
        return vlue2Desc;
    }

    public void setVlue2Desc(String vlue2Desc) {
        this.vlue2Desc = vlue2Desc;
    }

    public String getVlue2Id() {
        return vlue2Id;
    }

    public void setVlue2Id(String vlue2Id) {
        this.vlue2Id = vlue2Id;
    }

}
