/*
 * Copyright(c) enginedata Company, Inc.  All Rights Reserved.
 * This software is the proprietary information of enginedata Company.
 *
 */

package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

public class HistoryDetailDTO implements Serializable {
    
    private String fecha;
    private String hora;
    private String sucursal;
    private String status;
    private String dia;
    private String dianum;
    private String mes;
    private String anio;
    private String sucOrigen;
    private String sucDestino;

    public String getSucOrigen() {
        return sucOrigen;
    }

    public void setSucOrigen(String sucOrigen) {
        this.sucOrigen = sucOrigen;
    }

    public String getSucDestino() {
        return sucDestino;
    }

    public void setSucDestino(String sucDestino) {
        this.sucDestino = sucDestino;
    }
   
    public HistoryDetailDTO() {
    }        
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getDianum() {
        return dianum;
    }

    public void setDianum(String dianum) {
        this.dianum = dianum;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }
   

   


}
