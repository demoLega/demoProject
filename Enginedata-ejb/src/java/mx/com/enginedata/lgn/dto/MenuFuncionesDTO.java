/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

/**
 *
 * @author evillegas
 */
public class MenuFuncionesDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;
    private Integer padreId;   
    private Integer opcionId;
    private Integer orden;
    private Integer esCatalogo;
    private String opcionNombre;
    private int escritura;
    private int lectura;	
    private int borrado;
    private int imprimir;	
    private int autorizacion;	

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPadreId() {
        return padreId;
    }

    public void setPadreId(Integer padreId) {
        this.padreId = padreId;
    }

    public Integer getOpcionId() {
        return opcionId;
    }

    public void setOpcionId(Integer opcionId) {
        this.opcionId = opcionId;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getEsCatalogo() {
        return esCatalogo;
    }

    public void setEsCatalogo(Integer esCatalogo) {
        this.esCatalogo = esCatalogo;
    }

    public String getOpcionNombre() {
        return opcionNombre;
    }

    public void setOpcionNombre(String opcionNombre) {
        this.opcionNombre = opcionNombre;
    }

    public int getEscritura() {
        return escritura;
    }

    public void setEscritura(int escritura) {
        this.escritura = escritura;
    }

    public int getLectura() {
        return lectura;
    }

    public void setLectura(int lectura) {
        this.lectura = lectura;
    }

    public int getBorrado() {
        return borrado;
    }

    public void setBorrado(int borrado) {
        this.borrado = borrado;
    }

    public int getImprimir() {
        return imprimir;
    }

    public void setImprimir(int imprimir) {
        this.imprimir = imprimir;
    }

    public int getAutorizacion() {
        return autorizacion;
    }

    public void setAutorizacion(int autorizacion) {
        this.autorizacion = autorizacion;
    }
}