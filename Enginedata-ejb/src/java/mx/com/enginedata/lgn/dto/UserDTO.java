/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

/**
 *
 * @author evillegas
 */
public class UserDTO implements Serializable {

    private long userId;
    private String userName;
    private String userFirstName;
    private String userLastName;
    private String password;
    //private Integer empresa;
    //private Integer codigoEmpresa;

    private long sucursalId;
    private long perfilId;
    private boolean adminSucursales;
    private boolean changeSucursal;

    //private Integer empleado;
    //private EmpresaDTO empresaDTO = new EmpresaDTO();
    //private GrupoDTO grupoDTO = new GrupoDTO();
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getSucursalId() {
        return sucursalId;
    }

    public void setSucursalId(long sucursalId) {
        this.sucursalId = sucursalId;
    }

    public long getPerfilId() {
        return perfilId;
    }

    public void setPerfilId(long perfilId) {
        this.perfilId = perfilId;
    }

    public boolean isAdminSucursales() {
        return adminSucursales;
    }

    public void setAdminSucursales(boolean adminSucursales) {
        this.adminSucursales = adminSucursales;
    }

    public boolean isChangeSucursal() {
        return changeSucursal;
    }

    public void setChangeSucursal(boolean changeSucursal) {
        this.changeSucursal = changeSucursal;
    }

}
