package mx.com.enginedata.lgn.dto;

import java.io.Serializable;

public class StructIdiomaDTO implements Serializable {

    public StructIdiomaDTO() {
    }

    private String key;
    public String idioma;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
}
