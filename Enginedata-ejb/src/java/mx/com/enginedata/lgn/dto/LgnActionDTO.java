/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.lgn.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jasanchez
 */
public class LgnActionDTO implements Serializable {

    public static final long serialVersionUID = 1L;
    public Long idAction;
    public String name;
    public List<LgnFunctionActionDTO> lgnFunctionActionListDTO;
    public String stus;
    public String deleteFlag;
    public Date crtdOn;
    public String crtdBy;
    public Date mdfdOn;
    public String mdfdBy;
    public boolean actionActive;
    public boolean change;

    public LgnActionDTO() {
    }

    public Long getIdAction() {
        return idAction;
    }

    public void setIdAction(Long idAction) {
        this.idAction = idAction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LgnFunctionActionDTO> getLgnFunctionActionListDTO() {
        return lgnFunctionActionListDTO;
    }

    public void setLgnFunctionActionListDTO(List<LgnFunctionActionDTO> lgnFunctionActionListDTO) {
        this.lgnFunctionActionListDTO = lgnFunctionActionListDTO;
    }

    public String getStus() {
        return stus;
    }

    public void setStus(String stus) {
        this.stus = stus;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Date getCrtdOn() {
        return crtdOn;
    }

    public void setCrtdOn(Date crtdOn) {
        this.crtdOn = crtdOn;
    }

    public String getCrtdBy() {
        return crtdBy;
    }

    public void setCrtdBy(String crtdBy) {
        this.crtdBy = crtdBy;
    }

    public Date getMdfdOn() {
        return mdfdOn;
    }

    public void setMdfdOn(Date mdfdOn) {
        this.mdfdOn = mdfdOn;
    }

    public String getMdfdBy() {
        return mdfdBy;
    }

    public void setMdfdBy(String mdfdBy) {
        this.mdfdBy = mdfdBy;
    }

    public boolean isActionActive() {
        return actionActive;
    }

    public void setActionActive(boolean actionActive) {
        this.actionActive = actionActive;
    }

    public boolean isChange() {
        return change;
    }

    public void setChange(boolean change) {
        this.change = change;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LgnActionDTO other = (LgnActionDTO) obj;
        if (this.idAction != other.idAction && (this.idAction == null || !this.idAction.equals(other.idAction))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LgnActionDTO{" + "idAction=" + idAction + ", name=" + name + ", lgnFunctionActionCollection=" + lgnFunctionActionListDTO + ", stus=" + stus + ", deleteFlag=" + deleteFlag + ", crtdOn=" + crtdOn + ", crtdBy=" + crtdBy + ", mdfdOn=" + mdfdOn + ", mdfdBy=" + mdfdBy + '}';
    }
}
