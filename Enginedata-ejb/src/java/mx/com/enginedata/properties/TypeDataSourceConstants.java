package mx.com.enginedata.properties;

import java.io.Serializable;

public enum TypeDataSourceConstants implements Serializable {

    ENGINEDATA(0);
    private final int type;

    TypeDataSourceConstants(int clasificacion) {
        this.type = clasificacion;
    }

    public int getType() {
        return type;
    }

    public static TypeDataSourceConstants getEnum(int clasificador) {
        if (ENGINEDATA.getType() == clasificador) {
            return ENGINEDATA;
        }
        throw new IllegalArgumentException("No Enum specified for this string");
    }
}
