package mx.com.enginedata.properties;

import java.io.Serializable;
import static mx.com.enginedata.properties.TypePropertiesConstants.COMERCIAL;

public enum TypePropertiesConstants implements Serializable {
    RAD(0), PRINCIPAL(1), LOGISTICA(2), ANOMALIAS(3), EAD(4) , CENTINEL(5) , SEG(6), COMERCIAL(7);
    private final int type;

    TypePropertiesConstants(int clasificacion) {
        this.type = clasificacion;
    }

    public int getType() {
        return type;
    }

    public static TypePropertiesConstants getEnum(int clasificador) {
        if (RAD.getType() == clasificador) {
            return RAD;
        }else if (PRINCIPAL.getType() == clasificador) {
            return PRINCIPAL;
        }else if (LOGISTICA.getType() == clasificador) {
            return LOGISTICA;
        }else if (ANOMALIAS.getType() == clasificador) {
            return ANOMALIAS;
        }else if (EAD.getType() == clasificador) {
            return EAD;
        }else if (CENTINEL.getType() == clasificador) {
            return CENTINEL;
        }else if (SEG.getType() == clasificador) {
            return SEG;        
        }else if (COMERCIAL.getType() == clasificador) {
            return COMERCIAL;
        }
        throw new IllegalArgumentException("No Enum specified for this string");
    }
}
