package mx.com.enginedata.properties;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import mx.com.paquetexpress.comun.SmartGeneralException;

/**
 * Esta clase lee de forma Singleton o Normal el Archivo querys.properties el
 * cual contiene todos los querys usados por la parte comercial
 *
 * @author: Jose Armando
 * @version: 17/11/2016
 */
public class InitPropertiesQuerysCommercial {

    private static Properties properties;

    /**
     * Constructor sin parametros
     */
    private InitPropertiesQuerysCommercial() {
    }

    /**
     * Método que regresa un objeto properties singleton con todos los querys de
     * la parte comercial
     *
     * @return Objeto properties con querys de aplicacion comercial
     */
    public static Properties getInstance() {
        try {
            initSessionFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return properties;
    }

    /**
     * Método que que regresa un objeto properties no singleton con todos los
     * querys de la aplicacion comercial
     *
     * @return Objeto properties con querys de aplicacion comercial
     */
    public static Properties getReloadInstance() {
        try {
            properties = null;
            initSessionFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return properties;
    }

    /**
     * Método que regresa el query en base a la clave solicitada
     *
     * @param key es la clave para obtener el query
     * @return query con la clave
     */
    public String getValue(String key) {
        return properties.getProperty(key);
    }

    /**
     * Método que lee el archivo querys.Properties y los almacena en una
     * variable singleton
     */
    private static synchronized void initSessionFactory() throws Exception {
        FileInputStream is = null;
        try {
            String ruta = "";
            if (properties == null) {
                //System.out.println("Entra a querys_rad.properties");
                ruta = System.getProperty("user.dir") + File.separator + "enginedataConfig" + File.separator + "querysSipWeb" + File.separator + "querys_commercial.properties";
                //System.out.println("Entra a querys_Principal.properties:" + ruta);                
                is = new FileInputStream(ruta);
                properties = new Properties();
                properties.load(is);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SmartGeneralException("No se encontro el Archivo querys_commercial.properties");
        } finally {
            if (is != null) {
                is.close();
                is = null;
            }
        }
    }
}
