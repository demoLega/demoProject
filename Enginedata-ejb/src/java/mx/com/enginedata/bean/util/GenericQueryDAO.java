/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.bean.util;

/**
 *
 * @author Edgar
 */
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Properties;
import javax.ejb.Stateless;
import mx.com.paquetexpress.ado.DataRowSet;
import mx.com.paquetexpress.ado.OracleConnection;
import mx.com.paquetexpress.ado.PostGresConnection;
import mx.com.enginedata.bean.util.enumerator.TypeModule;
import mx.com.enginedata.properties.InitPropertiesQuerysAnmo;
import mx.com.enginedata.properties.InitPropertiesQuerysLogistica;
import mx.com.enginedata.properties.InitPropertiesQuerysRad;
import mx.com.enginedata.properties.TypeDataSourceConstants;
import mx.com.enginedata.properties.TypePropertiesConstants;
import mx.com.paquetexpress.log.LoggerCustom;
import mx.com.enginedata.properties.InitPropertiesApplication;
import mx.com.enginedata.properties.InitPropertiesPrincipal;
import mx.com.enginedata.properties.InitPropertiesQuerysCentinel;
import mx.com.enginedata.properties.InitPropertiesQuerysCommercial;
import mx.com.enginedata.properties.InitPropertiesQuerysEad;
import mx.com.enginedata.properties.InitPropertiesQuerysSeg;
import mx.jarc.util.dao.core.Parser;
import mx.jarc.util.dao.dto.SelectEntity;
import mx.jarc.util.dao.dto.WhereCondition;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author evillegas
 */
@Stateless(name = "GenericQueryDAO", mappedName = "PQEXPRESS.UTIL.CATALOGS.GenericQueryDAO")
public class GenericQueryDAO implements GenericQueryDAOLocal {

    private String datasourceOracle = null;
    private OracleConnection oracleConnection = null;
    private PostGresConnection postgresConnection = null;
    private final LoggerCustom log;

    public GenericQueryDAO() {
        oracleConnection = new OracleConnection();
        datasourceOracle = InitPropertiesApplication.getInstance().getProperty("datasourceOracle");
        log = (LoggerCustom) LoggerCustom.getLogger(GenericQueryDAO.class.getName());        
    }

     /**
     * **********************************
     * Metodo para hacer querys genericos
     * **********************************
     * @param select : Objeto donde se definen las columnas y tablas a consultar     
     * @param where : Objeto donde se define la clausula WHERE a aplica al query
     * @param dto : Objeto DTO a llenar y regresar en la lista
     * @return : Lista de DTOs obtenidos a partir de la consulta
     * @throws java.lang.Exception
     */
    public ArrayList getAllItemsByFilters(SelectEntity select, WhereCondition where, Object dto) throws Exception {
        ArrayList list = null;
        Parser parser = new Parser();
        String qryInfo = "";
        try {
            qryInfo = parser.queryParser(select, where);
            list = oracleConnection.runOracleQueryBeanList(qryInfo, new BeanListHandler(dto.getClass()), datasourceOracle);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), qryInfo);
            throw ex;
        }
        return list;
    }

    /**
     * **********************************
     * Metodo para hacer querys genericos
     * **********************************
     * @param nameQuery : Objeto donde se definen la clave del query a ejecutar
     * @param where : Objeto donde se define la clausula WHERE a aplica al query
     * @param dto : Objeto DTO a llenar y regresar en la lista
     * @param propertiesFile : Archivo properties donde se busca la clave del query a consultar
     * @return : Lista de DTOs obtenidos a partir de la consulta
     * @throws java.lang.Exception
     */
    public ArrayList getAllItemsByFilters(String nameQuery, WhereCondition where, Object dto, TypePropertiesConstants propertiesFile) throws Exception 
    {
        ArrayList list = null;
        Properties propertiesQuerys = null;
        Parser parser = new Parser();
        try {
            //System.out.println("propertiesFile " + propertiesFile);
            propertiesQuerys = getProperties(propertiesFile);

            //System.out.println("nameQuery " + nameQuery);
            nameQuery = propertiesQuerys.getProperty(nameQuery);
            nameQuery = nameQuery + parser.queryParser(new SelectEntity(), where);

            list = oracleConnection.runOracleQueryBeanList(nameQuery, new BeanListHandler(dto.getClass()), datasourceOracle);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), nameQuery);
            throw ex;
        } finally {
            propertiesQuerys = null;
        }
        return list;
    }

    /**
     * **********************************
     * Metodo para hacer querys genericos
     * **********************************
     * @param nameQuery : Objeto donde se definen la clave del query a ejecutar
     * @param where : Objeto donde se define la clausula WHERE a aplica al query
     * @param dto : Objeto DTO a llenar y regresar en la lista
     * @param propertiesFile : Archivo properties donde se busca la clave del query a consultar
     * @param typeDataSource : DataSource donde se ejecutara la consulta
     * @return : Lista de DTOs obtenidos a partir de la consulta
     * @throws java.lang.Exception
     */    
    public ArrayList getAllItemsByFilters(String nameQuery, WhereCondition where, Object dto, TypePropertiesConstants propertiesFile, TypeDataSourceConstants typeDataSource) throws Exception 
    {
        ArrayList list = null;
        Properties propertiesQuerys = null;
        Parser parser = new Parser();
        try {
            propertiesQuerys = getProperties(propertiesFile);
            String ds = getDataSource(typeDataSource);

            //System.out.println("QUERY: " +nameQuery + "ds " + ds); 
            String n = nameQuery;
            
            nameQuery = propertiesQuerys.getProperty(nameQuery);
            nameQuery = nameQuery + parser.queryParser(new SelectEntity(), where);

            list = oracleConnection.runOracleQueryBeanList(nameQuery, new BeanListHandler(dto.getClass()), ds);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), nameQuery);            
            throw ex;
        }
        return list;
    }
    
/**
     * **********************************
     * Metodo para hacer querys genericos
     * **********************************
     * @param nameQuery : Objeto donde se definen la clave del query a ejecutar
     * @param filters : Objeto donde vienen los parametros de la consulta
     * @param dto : Objeto DTO a llenar y regresar en la lista
     * @param propertiesFile : Archivo properties donde se busca la clave del query a consultar
     * @param typeDataSource : DataSource donde se ejecutara la consulta
     * @return : Lista de DTOs obtenidos a partir de la consulta
     * @throws java.lang.Exception
     */    
    public ArrayList getAllItemsByFilters(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile, TypeDataSourceConstants typeDataSource) throws Exception 
    {
        ArrayList list = null;
        Properties propertiesQuerys = null;
        try {
            propertiesQuerys = getProperties(propertiesFile);
            String ds = getDataSource(typeDataSource);
            nameQuery = propertiesQuerys.getProperty(nameQuery);
            if (nameQuery.contains("@")) {
                if (filters != null) {
                    for (short x = 0; x < filters.length; x++) {
                        nameQuery = nameQuery.replace("@" + x, (String) filters[x]);
                    }
                    filters = null;
                }
            }
            nameQuery = nameQuery.replaceAll("'null'", "null");
            list = oracleConnection.runOracleQueryBeanList(nameQuery, filters, new BeanListHandler(dto.getClass()), ds);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), nameQuery);            
            throw ex;
        }
        return list;
    }    

    /**
     * **********************************
     * Metodo para hacer querys genericos
     * **********************************
     * @param nameQuery : Objeto donde se definen la clave del query a ejecutar usando el metodo ? para lo parametros
     * @param filters : Lista de parametros
     * @param dto : Objeto DTO a llenar y regresar en la lista
     * @param propertiesFile : Archivo properties donde se busca la clave del query a consultar
     * @return : Lista de DTOs obtenidos a partir de la consulta
     * @throws java.lang.Exception
     */     
    public ArrayList getAllItemsByFilters(String nameQueryA, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception {
        ArrayList list = null;
        Properties propertiesQuerys = null;
        try {
            
            propertiesQuerys = getProperties(propertiesFile);
            String nameQuery = propertiesQuerys.getProperty(nameQueryA);
            list = oracleConnection.runOracleQueryBeanList(nameQuery, filters, new BeanListHandler(dto.getClass()), datasourceOracle);
        } catch (Exception ex) {
            log.error("Error en GenericQueryDAO (getAllItemsByFilters)", ex, TypeModule.GENERIC.getType(), nameQueryA);            
            throw ex;
        }
        return list;
    }
    
    /**
     * **************************************************************
     * Método para hacer querys genéricos, donde exista condición IN 
     * **************************************************************
     * @param nameQuery : Objeto donde se definen la clave del query a ejecutar usando el metodo ? para lo parametros
     * @param filters : Lista de parametros
     * @param dto : Objeto DTO a llenar y regresar en la lista
     * @param propertiesFile : Archivo properties donde se busca la clave del query a consultar
     * @return : Lista de DTOs obtenidos a partir de la consulta
     * @throws java.lang.Exception
     */     
    public ArrayList getAllItemsByFiltersIn(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception {
        ArrayList list = null;
        Properties propertiesQuerys = null;
        try {
            propertiesQuerys = getProperties(propertiesFile);
            nameQuery = propertiesQuerys.getProperty(nameQuery);
            String paramsPositions = "";
             for (int x = 0; x<filters.length; x++) {
//                paramsPositions += "'".concat(filters[x].toString()).concat("'").concat((x < filters.length - 1) ? ", " : "");
                paramsPositions += "?".concat((x < filters.length - 1) ? ", " : "");
            } 
            int times = StringUtils.countMatches(nameQuery, "@inParams");
            nameQuery = nameQuery.replace("@inParams", paramsPositions);
            Object[] filters_repeated = new Object[filters.length*times];
            int fr_idx = 0;
            for(int i = 0; i < times; i++){
                for (Object filter : filters) {
                    filters_repeated[fr_idx] = filter;
                    fr_idx++;
                }
            }
            
            list = oracleConnection.runOracleQueryBeanList(nameQuery, filters_repeated, new BeanListHandler(dto.getClass()), datasourceOracle);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), nameQuery);            
            throw ex;
        }
        return list;
    }
    
//    public ArrayList getAllItemsByFiltersMsSql(String nameQuery, WhereCondition where, Object dto, TypePropertiesConstants propertiesFile, TypeDataSourceConstants typeDataSource) throws Exception {
//        ArrayList list = null;
//        Properties propertiesApplication = null;
//        Properties propertiesQuerys = null;
//        Parser parser = new Parser();
//        try {
//            propertiesApplication = InitPropertiesApplication.getInstance();
//            propertiesQuerys = getProperties(propertiesFile);
//            nameQuery = propertiesQuerys.getProperty(nameQuery);
//            nameQuery = nameQuery + parser.queryParser(new SelectEntity(), where);
//            list = new SQLServerConnection().runSQLServerQueryBeanList(nameQuery, new BeanListHandler(dto.getClass()), propertiesApplication.getProperty("datasourceSQLUTRAX"));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), nameQuery);
//            throw ex;
//        }
//        return list;
//    }

    public Object getOnlyOneItemByFilters(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception {
        Properties propertiesQuerys = null;
        try {
            propertiesQuerys = getProperties(propertiesFile);
            nameQuery = propertiesQuerys.getProperty(nameQuery);
            dto = oracleConnection.runOracleQueryBean(nameQuery, filters, new BeanHandler(dto.getClass()), datasourceOracle);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), nameQuery);            
            throw ex;
        }
        return dto;
    }

    public Object getOnlyOneItemByFilters(String nameQuery, WhereCondition where, Object dto, TypePropertiesConstants propertiesFile) throws Exception {
        Properties propertiesQuerys = null;
        Parser parser = new Parser();
        try {
            propertiesQuerys = getProperties(propertiesFile);
            nameQuery = propertiesQuerys.getProperty(nameQuery);
            nameQuery = nameQuery + parser.queryParser(new SelectEntity(), where);
            dto = oracleConnection.runOracleQueryBean(nameQuery, new BeanHandler(dto.getClass()), datasourceOracle);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), nameQuery);
            throw ex;
        }
        return dto;
    }

    public Object getOnlyOneValue(String nameQuery, Object[] filters, TypePropertiesConstants propertiesFile) throws Exception {
        Properties propertiesQuerys = null;
        Object dto = null;
        try {
            propertiesQuerys = getProperties(propertiesFile);
            nameQuery = propertiesQuerys.getProperty(nameQuery);
            DataRowSet rs = oracleConnection.runOracleQuery(nameQuery, filters, datasourceOracle);
            if(rs.next()){
                dto = rs.getObject(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), nameQuery);            
            throw ex;
        }
        return dto;
    }    
    
    public Properties getProperties(TypePropertiesConstants propertiesFile) throws Exception {
        Properties propertiesQuerys = null;
        try {
            switch (propertiesFile) {
                case RAD:
                    propertiesQuerys = InitPropertiesQuerysRad.getInstance();
                    break;
                case LOGISTICA:
                    propertiesQuerys = InitPropertiesQuerysLogistica.getInstance();
                    break;
                case PRINCIPAL:
                    propertiesQuerys = InitPropertiesPrincipal.getInstance();
                    break;
                case ANOMALIAS:
                    propertiesQuerys = InitPropertiesQuerysAnmo.getInstance();
                    break;
                case EAD:
                    propertiesQuerys = InitPropertiesQuerysEad.getInstance();
                    break;
                case SEG:
                    propertiesQuerys = InitPropertiesQuerysSeg.getInstance();
                    break;
                case CENTINEL:
                    propertiesQuerys = InitPropertiesQuerysCentinel.getInstance();
                    break; 
                case COMERCIAL:
                    propertiesQuerys = InitPropertiesQuerysCommercial.getInstance();
                    break; 
                default:
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), propertiesFile);
            throw ex;
        }
        return propertiesQuerys;
    }

    public String getDataSource(TypeDataSourceConstants typeDataSourceConstants) throws Exception {
        String dataSource = null;
        try {
            switch (typeDataSourceConstants) {
                case ENGINEDATA:
                    dataSource = InitPropertiesApplication.getInstance().getProperty("datasourceOracle");
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), typeDataSourceConstants);
            throw ex;
        }
        return dataSource;
    }
    
    public ArrayList getAllItemsByFiltersWithConnGeo(String nameQuery, String[] parameters, Object dto, TypePropertiesConstants propertiesFile , Connection con , boolean close) throws Exception {
        ArrayList list = null;
        Properties propertiesQuerys = null;
        try {  
            propertiesQuerys = getProperties(propertiesFile);
            nameQuery = propertiesQuerys.getProperty(nameQuery); 
//            if (parameters.length>2){
//                System.out.println(parameters[2]);
//            }
            postgresConnection = new PostGresConnection();
            list = postgresConnection.runOracleQueryBeanList(nameQuery, parameters, new BeanListHandler(dto.getClass()), con, close);
 
        } catch (Exception ex) { 
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO Connection Geo", ex, TypeModule.GENERIC.getType(), nameQuery);            
            throw ex;
        }finally {
           if (close){
                con.close();
            }
        }
        return list;
    }    
    
/**
     * **********************************
     * Metodo para hacer querys genericos POSTGRES
     * **********************************
     * @param nameQuery : query directo que se va a ejectar
     * @param filters : Objeto donde vienen los parametros de la consulta
     * @param dto : Objeto DTO a llenar y regresar en la lista
     * @param propertiesFile : Archivo properties donde se busca la clave del query a consultar
     * @param con : Conexion donde se ejecutara la consulta
     * @return : Lista de DTOs obtenidos a partir de la consulta
     * @throws java.lang.Exception
     */    
    public ArrayList getAllItemsByFiltersQuery(String nameQuery, Object dto, TypePropertiesConstants propertiesFile,  Connection con) throws Exception 
    {
        ArrayList list = null;
        Object[] filters = null;
        try {
            postgresConnection = new PostGresConnection();
            list = postgresConnection.runOracleQueryBeanList(nameQuery, filters, new BeanListHandler(dto.getClass()), con, true);            
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO", ex, TypeModule.GENERIC.getType(), nameQuery);            
            throw ex;
        }
        
        return list;
    }       
 
    
/**
     * **********************************
     * Metodo para hacer querys genericos POSTGRES
     * **********************************
     * @param nameQueryA : Objeto donde se definen la clave del query a ejecutar
     * @param filters : Objeto donde vienen los parametros de la consulta
     * @param dto : Objeto DTO a llenar y regresar en la lista
     * @param propertiesFile : Archivo properties donde se busca la clave del query a consultar
     * @param con : Conexion donde se ejecutara la consulta
     * @return : Lista de DTOs obtenidos a partir de la consulta
     * @throws java.lang.Exception
     */    
    public ArrayList getAllItemsByFiltersPostgress(String nameQueryA, Object[] filters, Object dto, TypePropertiesConstants propertiesFile,  Connection con) throws Exception 
    {
        ArrayList list = null;
        Properties propertiesQuerys = null;
        try {
            propertiesQuerys = getProperties(propertiesFile);
            String nameQuery = propertiesQuerys.getProperty(nameQueryA);
            if (nameQuery.contains("@")) {
                if (filters != null) {
                    for (short x = 0; x < filters.length; x++) {
                        nameQuery = nameQuery.replace("@" + x, (String) filters[x]);
                    }
                    nameQuery = nameQuery.replaceAll("'null'", "null");
                    filters = null;
                }
            }
            postgresConnection = new PostGresConnection();
            list = postgresConnection.runOracleQueryBeanList(nameQuery, filters, new BeanListHandler(dto.getClass()), con, true);            
            //list = oracleConnection.runOracleQueryBeanList(nameQuery, filters, new BeanListHandler(dto.getClass()), ds);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error en GenericQueryDAO (getAllItemsByFiltersPostgress)", ex, TypeModule.GENERIC.getType(), nameQueryA);            
            throw ex;
        }
        
        return list;
    }       

    @Override
    public ArrayList getAllItemsByFilters(String nameQuery, Object[] filters, Object dto, mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants propertiesFile) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList getAllItemsByFiltersIn(String nameQuery, Object[] filters, Object dto, mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants propertiesFile) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getOnlyOneItemByFilters(String nameQuery, Object[] filters, Object dto, mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants propertiesFile) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getOnlyOneValue(String nameQuery, Object[] filters, mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants propertiesFile) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Properties getProperties(mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants propertiesFile) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList getAllItemsByFiltersWithConnGeo(String nameQuery, String[] parameters, Object dto, mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants propertiesFile, Connection con, boolean close) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList getAllItemsByFiltersPostgress(String nameQuery, Object[] filters, Object dto, mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants propertiesFile, Connection con) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList getAllItemsByFiltersQuery(String query, Object dto, mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants propertiesFile, Connection con) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}