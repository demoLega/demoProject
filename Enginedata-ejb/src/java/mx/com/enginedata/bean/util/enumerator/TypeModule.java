/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.bean.util.enumerator;

import java.io.Serializable;

/**
 *
 * @author jasanchez
 */
public enum TypeModule  implements Serializable{

    RAD("RAD"), ANMO("ANMO"), LOGISTIC("LOGISTIC"), GENERIC("GENERIC"), LGN("LGN"), COMMERCIAL("COMMERCIAL"), GENERAL("GENERAL"), EAD("EAD");
    private final String type;

    TypeModule(String clasificacion) {
        this.type = clasificacion;
    }

    public String getType() {
        return type;
    }

    public static TypeModule getEnum(String clasificador) {
        if (RAD.getType().equalsIgnoreCase(clasificador)) {
            return RAD;
        } else if (ANMO.getType().equalsIgnoreCase(clasificador)) {
            return ANMO;
        } else if (LOGISTIC.getType().equalsIgnoreCase(clasificador)) {
            return LOGISTIC;
        } else if (LGN.getType().equalsIgnoreCase(clasificador)) {
            return LGN;
        } else if (GENERIC.getType().equalsIgnoreCase(clasificador)) {
            return GENERIC;
        }else if (GENERAL.getType().equalsIgnoreCase(clasificador)) {
            return GENERAL;
        }else if (COMMERCIAL.getType().equalsIgnoreCase(clasificador)) {
            return COMMERCIAL;
        } else if (EAD.getType().equalsIgnoreCase(clasificador)) {
            return EAD;
        }
        return null;
    }
}
