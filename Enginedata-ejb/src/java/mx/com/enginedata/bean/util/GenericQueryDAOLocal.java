/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.enginedata.bean.util;

/**
 *
 * @author Edgar
 */
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Properties;
import javax.ejb.Local;
import mx.com.enginedata.bean.util.enumerator.TypePropertiesConstants;

/**
 *
 * @author evillegas
 */

@Local
public interface GenericQueryDAOLocal {

    ArrayList getAllItemsByFilters(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception;    

    ArrayList getAllItemsByFiltersIn(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception;    

    Object getOnlyOneItemByFilters(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile) throws Exception;
    
//    ArrayList getAllItemsByFiltersMsSql(String nameQuery, WhereCondition where, Object dto, TypePropertiesConstants propertiesFile, TypeDataSourceConstants typeDataSource) throws Exception;  
    public Object getOnlyOneValue(String nameQuery, Object[] filters, TypePropertiesConstants propertiesFile) throws Exception;
    
    Properties getProperties(TypePropertiesConstants propertiesFile) throws Exception;
    
    ArrayList getAllItemsByFiltersWithConnGeo(String nameQuery, String[] parameters, Object dto, TypePropertiesConstants propertiesFile , Connection con , boolean close) throws Exception;

    public ArrayList getAllItemsByFiltersPostgress(String nameQuery, Object[] filters, Object dto, TypePropertiesConstants propertiesFile,  Connection con) throws Exception ;
    
    ArrayList getAllItemsByFiltersQuery(String query, Object dto, TypePropertiesConstants propertiesFile , Connection con ) throws Exception;            
    
}